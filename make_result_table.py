import csv
import os

from app.utils import load_result_dir

result_dir = input('result dir > ')

metric_names, metric_keys, table_entries = load_result_dir(result_dir)

with open(os.path.join(result_dir, 'table.md'), 'w') as f:
    metric_header = ' | '.join(metric_names)
    header_border = ' | '.join(':-:' for _ in range(len(metric_names)))

    f.write('| Rank | Dataset | Label | Task | ' + metric_header + ' | Option |  TS  | DirName |\n')
    f.write('| :--: | :-----: | :---: | :--: | ' + header_border + ' | :----: | :--: | :-----: |\n')
    for table_entry in table_entries:
        f.write('| {rank} | {dataset} | {label} | {task} | {metrics} | {option} | {ts} | {dirname} |\n'.format(
            rank=table_entry.rank,
            dataset=table_entry.exp_config.dataset,
            label=table_entry.exp_config.label,
            task=table_entry.exp_config.task,
            metrics=' | '.join(
                '%.3lf (@%d)' % (table_entry.result[key].val,
                                 table_entry.result[key].epoch)
                for key in metric_keys
            ),
            option=table_entry.exp_config.option,
            ts=table_entry.exp_config.ts,
            dirname=table_entry.exp_config.dirname,
        ))

with open(os.path.join(result_dir, 'table.csv'), 'w', encoding='utf-8') as f:
    csvwriter = csv.DictWriter(
        f,
        quoting=csv.QUOTE_ALL,
        fieldnames=['Rank', 'Dataset', 'Label', 'Task', *metric_names, 'Option', 'TS', 'DirName']
    )

    csvwriter.writeheader()
    for table_entry in table_entries:
        csvwriter.writerow({
            'Rank': table_entry.rank,
            'Dataset': table_entry.exp_config.dataset,
            'Label': table_entry.exp_config.label,
            'Task': table_entry.exp_config.task,
            **{
                name: '%.3lf (@%d)' % (table_entry.result[key].val,
                                       table_entry.result[key].epoch)
                for name, key in zip(metric_names, metric_keys)
            },
            'Option': table_entry.exp_config.option,
            'TS': str(table_entry.exp_config.ts),
            'DirName': table_entry.exp_config.dirname,
        })
