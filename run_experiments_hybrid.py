from __future__ import annotations

import os
import queue
import random
import subprocess  # nosec
import time
from concurrent.futures import Future, ThreadPoolExecutor, as_completed

from sklearn.model_selection import ParameterGrid, ParameterSampler

POSSIBLE_INTERPRETERS: list[str] = [
    './venv/Scripts/python.exe',
    './venv/bin/python'
]

INTERPRETER: str | None = None
for pypath in POSSIBLE_INTERPRETERS:
    if os.path.exists(pypath):
        INTERPRETER = pypath
        break

if INTERPRETER is None:
    raise RuntimeError('Could not find python interpreter')
else:
    print(f'interpreter: {INTERPRETER}')

_SENSOR_MODEL_AROUSAL = (
    'db_v6/'
    'result_noaudio+small'
    '_arousal'
    '_c+nocbam+nonorm+gru+noise5+sdrop8+sfilter64+smfilter64+runit128+rdrop5+rlayer2+dunit128+dlayer1'
    '_366573/'
    'test_macro_f1.097-0.5191.hdf5'
)
_SENSOR_MODEL_VALENCE = (
    'db_v6/'
    'result_noaudio+small'
    '_valence'
    '_c+nocbam+nonorm+gru+noise5+sdrop8+sfilter64+smfilter64+runit128+rdrop5+rlayer2+dunit128+dlayer1'
    '_366290/'
    'test_macro_f1.070-0.5594.hdf5'
)
_SENTENCE_MODEL_AROUSAL = (
    'db_v6_emobank/'
    'result_fasttext300+emobank_arousal_gru+uni+runit32+rlayer2+dunit128+dlayer2_198030/'
    'test_MAPE.117-8.5133.hdf5'
)
_SENTENCE_MODEL_VALENCE = (
    'db_v6_emobank/'
    'result_fasttext300+emobank_valence_lstm+uni+runit32+rlayer2+dunit256+dlayer1_198143/'
    'test_MAPE.028-10.5115.hdf5'
)

_DATASET_GRID: dict[str, list[tuple[str, str | None]]] = {
    'SENSOR_MODEL_FILENAME': [('xarousal', _SENSOR_MODEL_AROUSAL), ('xvalence', _SENSOR_MODEL_VALENCE)],
    'SENTENCE_MODEL_FILENAME': [('yarousal', _SENTENCE_MODEL_AROUSAL), ('yvalence', _SENTENCE_MODEL_VALENCE)]
}

_PARAM_GRID: dict[str, list[tuple[str, str | None]]] = {
    'DO_REGRESSION': [('c', 'false'), ('r', 'true')],
    'DENSE_UNIT_CNT': [('dunit32', '32'), ('dunit64', '64'), ('dunit128', '128'), ('dunit256', '256')],
    'DENSE_NUM_LAYERS': [('dlayer1', '1'), ('dlayer2', '2'), ('dlayer3', '3'), ('dlayer3', '4')],
    'BATCH_SIZE': [('b32', '32'), ('b128', '128')],
}


RESULT_DIR_FORMAT = 'result_{dataset}_{option}_{ts}'


GPU_IDS = os.getenv('GPU_IDS', '0,1,2,3,4,5,6,7').split(',')
N_ITER = int(os.getenv('N_ITER', '1000'))


def _do_run(
    gpu_queue: queue.Queue[str],
    exp_param: dict[str, str],
    result_dir: str,
) -> dict[str, str]:
    assert INTERPRETER is not None  # nosec

    gpu_id = gpu_queue.get()
    try:
        subprocess.run(  # nosec
            [INTERPRETER, '-m', 'app.3_classifier_hybrid'],
            env={
                **os.environ,
                **exp_param,
                'CUDA_VISIBLE_DEVICES': gpu_id,
                'RESULT_DIR': result_dir,
                'NO_PLOT': 'true'
            }
        )
    finally:
        gpu_queue.task_done()
        gpu_queue.put(gpu_id)

    return exp_param


with ThreadPoolExecutor(len(GPU_IDS)) as executor:
    gpu_queue: queue.Queue[str] = queue.Queue()
    for gpu_id in GPU_IDS:
        gpu_queue.put(gpu_id)

    futs: list[Future] = []

    for dataset_sample in ParameterGrid(_DATASET_GRID):
        dataset_str = '+'.join(dataset_sample[key][0] for key in _DATASET_GRID.keys())

        for param_sample in ParameterSampler(_PARAM_GRID, n_iter=N_ITER):
            option_str = '+'.join(param_sample[key][0] for key in _PARAM_GRID.keys())

            exp_param: dict[str, str] = {
                **{
                    key: val[1]
                    for key, val in dataset_sample.items()
                    if val[1] is not None
                },
                **{
                    key: val[1]
                    for key, val in param_sample.items()
                    if val[1] is not None
                }
            }

            futs.append(
                executor.submit(
                    _do_run,
                    gpu_queue,
                    exp_param,
                    RESULT_DIR_FORMAT.format(
                        dataset=dataset_str,
                        option=option_str,
                        ts=int(time.monotonic() + random.randint(0, 1000))  # nosec
                    )
                )
            )

    try:
        for idx, fut in enumerate(as_completed(futs)):
            print('Done! [%d / %d]' % (idx + 1, len(futs)))
    except KeyboardInterrupt:
        print('Cancelled!')
        for fut in futs:
            if not fut.done():
                fut.cancel()
        executor.shutdown(False)
