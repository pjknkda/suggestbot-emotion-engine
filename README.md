## Requirements

- Python 3.8 + tensorflow-gpu 2.3
- Test environment
    - Ubuntu 18.04
    - Python 3.8.6 (default, Oct 10 2020, 02:28:03) [GCC 7.5.0] on linux
    - tensorflow-gpu 2.3.1
    - nvidia-driver 440.100 (supporting CUDA ~10.2)
    - CUDA 10.1
    - CuDNN 7.6.5.32
    - GraphViz 2.40.1

If the error `Could not load dynamic library 'libcublas.so.10'; dlerror: libcublas.so.10: cannot open shared object file: No such file or directory` occurs, you need to manually install `CUDA 10.0` library to get `libcublas.so` file and then create a symbolic link for missing library.

```sh
sudo apt-get install cuda-10-0
sudo ln -s /usr/local/cuda-10.0/lib64/libcublas.so.10.0 /usr/lib/x86_64-linux-gnu/libcublas.so.10
```

I think this is a mix of two bugs from `cuda` package in Ubuntu,
- `cuda-10-1` package misses `libcublas.so` file
- `cuda-10-0` pacakge does not correctly register `libcublas.so` to LDCONFIG path.


## Main Model

### Basics

You can run python modules using `python -m` syntax.

Examples:
- `python -m app.2_make_db`
- `USE_CBAM=false SENSOR_CNN_FILTER_CNT=32 python -m app.3_classifier_cnnrnn_cbam`


### 1. Convert various dataset formats to universal format

- `app/0_wav_to_wavf.py` : convert raw WAV file (which is float 32-bit PCM, mono-channel, 8,000HZ) to audio feature vector
- `app/1_raw_data_drm1.py` : build a dataset with Dr.M 1st Data
- `app/1_raw_data_drm2.py` : build a dataset with Dr.M 2nd Data
- `app/1_raw_data_debate_v2.py` : build a dataset with Debate V2 Data (with audio)
- The structure of dataset
    ```python
    {
        'labels': [
            'a1',
            'a2',
            ...
        ],
        'experiments': [
            # format: (subject_id, start_ts, end_ts, label_values)
            (1, 1000, 2000, (1, 2, ...)),
            (1, 3000, 4500, (2, 2, ...)),
            ...
        ],
        'sensors': [
            'e4_accel.x',
            'e4_accel.y',
            ...
        ],
        'sensor_values': [
            # format: `np.ndarray` with shape [sample_idx,]
            #         dtype: [('subject_id', 'int8'), ('ts', 'int64'), ('value', 'float64')]
            # e4_accel.x
            np.ndarray(...),
            # e4_accel.y
            np.ndarray(...),
            ...
        ]
    }
    ```


### 2. Build feature DB from universal dataset format

- `app/2_make_db.py` : build a database for model traning from dataset
- The structure of database
    ```python
    {
        'config': {
            'DATA': 'drm1.msgpack.gz',
            'MAX_DURATION': 25000,
            'WINDOW_SIZE': 1000,
            'WINDOW_INTERVAL': 1000,
            'DURATION_SPLIT': 5000,
            'VEC_DIM': 10
        },
        'labels': [
            'a1',
            'a2',
            ...
        ],
        'sensors': [
            'e4_accel.x',
            'e4_accel.y',
            ...
        ],
        'sensor_scales': {
            # format: (min_value, max_value, nan_value)
            # e4_accel.x
            (-250, 250, 15),
            # e4_accel.y
            (-250, 250, 26),
        },
        'exp_sensor_values': [
            # format[0]: (subject_id, start_ts, end_ts, label_values)
            # format[1]: `np.ndarray` with shape [window_idx, sensor_idx, vec_idx]
            #            dtype: float64
            # exp 1
            (
                (1, 1000, 2000, (1, 2, ...)),
                np.ndarray(...),
            ),
            # exp 2
            (
                (1, 3000, 4500, (2, 2, ...)),
                np.ndarray(...)
            )
            ...
        ]
    }
    ```
- The meaning of keys in `config`
    - `MAX_DURATION` : for each label, we discard the part of sensor values recorded longer than this duration (only used for traning) (unit: miliseconds)
    - `DURATION_SPLIT` : the maximum length of one session for traning/prediction (unit: miliseconds)
    - `WINDOW_SIZE` : the length of a sliding window (unit: miliseconds)
    - `WINDOW_INTERVAL` : the interval between sliding windows (unit: miliseconds)
    - `VEC_DIM` : the number of sampling points inside of a sliding window
    - Example
        ```
        MAX_DURATION = 15s
        DURATION_SPLIT = 5s
        WINDOW_SIZE = 3s
        WINDOW_INTERVAL = 1s
        VEC_DIM = 4

        |--------------------------------------------------| (raw data)
                                 17s

        |--------------------------------------------|  (truncated by MAX_DURATION)
                              15s

        |--------------|--------------|--------------|  5s * 3 (splitted by DURATION_SPLIT)
               5s             5s            5s

        |-------|      |-------|      |-------|         3 windows / split (splitted as
         1s |-------|   1s |-------|   1s |-------|     sliding window by WINDOW_SIZE
             1s|-------|    1s|-------|    1s|-------|  and WINDOW_INTERVAL)

        |-------|      |-------|      |-------|         4 points / window (by VEC_DIM)
        ^ ^ ^ ^        ^ ^ ^ ^        ^ ^ ^ ^
            |-------|      |-------|      |-------|
            ^ ^ ^ ^        ^ ^ ^ ^        ^ ^ ^ ^
               |-------|      |-------|      |-------|
               ^ ^ ^ ^        ^ ^ ^ ^        ^ ^ ^ ^
        ```


### 3. Train the model

Current models

- `app/3_classifier_cnnrnn_cbam.py` builds advanced CNN+RNN model
- `app/3_classifier_hybrid.py` builds CNN+RNN + pre-trained emobank model


Old models (deprecated)

- `app/old_classifiers/3_classifier_rf.py` builds baseline model (Random Forest)
- `app/old_classifiers/3_classifier_nn.py` builds simple nerual network model
- `app/old_classifiers/3_classifier_rnn.py` builds RNN model
- `app/old_classifiers/3_classifier_cnn_deprecated.py` build CNN model
- `app/old_classifiers/3_classifier_cnncnn_cnnrnn.py` builds CNN+CNN or CNN+RNN model


### 4. Use the model

- `app/4_server.py` serves a trainined model by `app/3_classifier_cnnrnn_cbam.py`

- The server will load models based on environments varialbes `MODEL_PATH` to find pre-trained models. Otherwise, the server will seek the default sample model from the location
    ```sh
    MODEL_PATH=sample/classification_arousal.hdf5 PORT=5050 python -m app.4_server
    ```

- If you want to serve multiple models, launch multiple servers per model and use different `PORT` for each.

- API specification

    - `GET /model_info`
        - Get the information (input shape) of the model.

        - Request

            ```sh
            curl -i -X GET \
            'http://127.0.0.1:5050/model_info'
            ```

        - Response

            ```js
            {
                "model_setup": {
                    "filename": "test_MD20000_WS1000_WI1000_DS5000_VD10.msgpack",
                    "config": {
                        "MAX_DURATION": 20000,
                        "WINDOW_SIZE": 1000,
                        "WINDOW_INTERVAL": 1000,
                        "DURATION_SPLIT": 5000,
                        "VEC_DIM": 10,
                        "DATA": "drm1.msgpack.gz"
                    },
                    "sensors": [
                        "e4_accel.x",
                        "e4_accel.y",
                        "e4_accel.z",
                        "e4_bvp",
                        ...
                    ],
                    "sensor_scales": [
                        [
                            -256.0,
                            254.0,
                            8.445171605593002
                        ],
                        [
                            -256.0,
                            254.0,
                            2.6360332823215873
                        ],
                        [
                            -256.0,
                            254.0,
                            -1.3786976348490012
                        ],
                        [
                            -19039.1953125,
                            12130.48046875,
                            0.0019406873771615548
                        ],
                        ...
                    ]
                }
            }
            ```

    - `POST /predict`
        - Get the predicted value of a specific context variable.

        - Parameters
            - sensor_values(str) -- Provided sensor values (refer to the sensor value format below)

        - (regression) Request

            ```sh
            curl -i -X POST \
            -H "Content-Type:application/x-www-form-urlencoded" \
            --data-urlencode "sensor_values=[...]" \
            'http://127.0.0.1:5050/predict'
            ```

        - (regression) Response
            ```js
            {
                "value": 3.1234
            }
            ```

        - (classification) Request

            ```sh
            curl -i -X POST \
            -H "Content-Type:application/x-www-form-urlencoded" \
            --data-urlencode "sensor_values=[...]" \
            'http://127.0.0.1:5050/predict'
            ```

        - (classification) Response

            ```js
            {
                "value": 2
            }
            ```

### How to call the API

1. By calling `/model_info` API, get model information for desired `target_result_type` and `target_vairable`.
2. According to the model information, prepare collected sensor data (from device) in a required format.
    - Let's assume that `/model_info` API returned the following model information:
        ```python
        {
            'MAX_DURATION': 600,
            'WINDOW_SIZE': 200,
            'WINDOW_INTERVAL': 100,
            'DURATION_SPLIT': 500,
            'VEC_DIM': 4,
            'DATA': 'drm1.msgpack.gz'
        }
        ```
    - To simplify the description, we assume that the model is trained with only single sensor.
    - Say, current timestamp is `now_ts = 1650` and you collected data from the sensor:
        ```python
        [(1000, 1.5), (1200, 2.0), (1300, 1.5), (1600, 3.0)]  # list of `(ts, value)`
        ```
    - Step 1, remove the sensor value older than `now_ts - MAX_DURATION = 1050`.
        ```python
        [(1200, 2.0), (1300, 1.5), (1600, 3.0)]  # list of `(ts, value)`
        ```
    - Step 2, take the last `DURATION_SPLIT = 500` miliseconds (actually, you do not need Step 1 due to this step)
        ```python
        [(1200, 2.0), (1300, 1.5), (1600, 3.0)]  # list of `(ts, value)`
        ```
    - Step 3, apply sliding windows with overlap. In the current model, there are total `(DURATION_SPLIT - WINDOW_SIZE) / WINDOW_INTERVAL + 1 = 4` sliding windows.
        ```python
        window_1 = (1150, 1350)  # `(start_ts, end_ts)`
        window_2 = (1250, 1450)
        ...
        window_4 = (1450, 1650)
        ```
    - Step 4, calculate `ts` of sampling point for the sliding windows by evenly split each window into `VEC_DIM = 4` segments
        ```python
        window_1_points = [1200, 1250, 1300, 1350]  # list of `ts`
        window_2_points = [1300, 1350, 1400, 1450]
        ...
        window_4_points = [1500, 1550, 1600, 1650]
        ```
    - Step 5, get "last observed" sensor value for each `ts` in the sliding windows (fill `NaN` if no observed value)
        ```python
        window_1_values = [2.0, 2.0, 1.5, 1.5]
        window_2_values = [1.5, 1.5, 1.5, 1.5]
        ...
        window_4_values = [1.5, 1.5, 3.0, 3.0]
        ```
    - Step 6, then, we get a numpy array with the shape `[window_idx, vec_idx]`
        ```python
        window_values = numpy.array([window_1_values, window_2_values, ..., window_4_values])
        ```
    - Step 7, apply normalization and missing value imputation. You can find `sensor_scale` from the response of `/model_info` API.
        ```python
        min_val, max_val, nan_val = sensor_scale
        window_values[numpy.isnan(window_values)] = nan_val  # replace NaN values to `nan_val`
        window_values = np.minimum(np.maximum(window_values, min_val), max_val)
        window_values = (window_values - min_val) / (max_val - min_val)
        ```
    - Step 8, Repeat the same process for other sensors and stack alongside the "second" axis. Here, the order of stacking should be the same to the order of `sensors` from the response of `/model_info` API.
    - Step 9, you finally get a tensor with the shape `[window_idx, sensor_idx, vec_idx]`.
3. Third, call `/predict` API with `target_result_type`, `target_vairable`, and the sensor values you got from above process. Here, the tensor with the shape `[window_idx, sensor_idx, vec_idx]` should be converted into 3-level nested list with JSON serialization. (see the example at `sample/(audio|noaudio)_(dense|small)_sample.json`)

#### Handling audio data

Audio data has a very high frequency compared to other sensor values, so we need a additional preprocessing before using it. You can find the python script corresponding to the description below at `app/0_wav_to_wavf.py`.

1. Prepare audio data in a WAVE format with 32-bit float PCM, mono-channal, and 8KHz sampling rate.
    ```java
    // Example for Android
    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                               8000,
                               AudioFormat.CHANNEL_CONFIGURATION_MONO,
                               AudioFormat.ENCODING_PCM_FLOAT,
                               bufferSize);
    ```
2. To simplify the description, assume that we collected audio data for 1 second. Then, we have total 8,000 samples.
3. We apply STFT (Short Time Fourier Transform) for 256 sampls in segment and 50% overlap. Each segment is around 32 miliseconds at 8KHz sampling rate.
4. We only use the real part of the FFT result, which means that we get singal power for 129 bins for each segment.
5. We regard each bin as an axis of audio data (like x, y, z axis of accelerometer).
    ```python
    # audio.f0 (bin 0)
    [(1000, 1.5), (1032, 2.0), (1064, 1.5), ..., (2000, 3.0)]  # list of `(ts, value)`

    # audio.f1 (bin 1)
    [(1000, 0.1), (1032, 1.1), (1064, 0.2), ..., (2000, 0.4)]  # list of `(ts, value)`
    
    ...
    ```



### Pre-trained Models

There are 6 types of datasets used to build pre-trained models:
- (Long) Audio Dense
- (Long) Audio Small
- (Long) Noaudio Dense
- (Long) Noaudio Small
- \* Short Audio Nobrain Dense
- \* Short Audio Nobrain Small

When using pre-trained models, you should be aware about the difference of each dataset:
- Noaudio : sensor `audio.f0` ~ `audio.f128` is not used
- Nobrain : sensor `brain_wave.high_alpha` ~ `brain_wave.delta` is not used
- Long : `MAX_DURATION = 5000` and `DURATION_SPLIT = 5000` and `WINDOW_SIZE = 1000`
    - Dense : use `WINDOW_INTERVAL = 500` and `VEC_DIM = 32` / Small : use `WINDOW_INTERVAL = 1000` and `VEC_DIM = 10`
- Short : `MAX_DURATION = 1000` and `DURATION_SPLIT = 1000` and `WINDOW_SIZE = 200`
    - Dense : use `WINDOW_INTERVAL = 100` and `VEC_DIM = 32` / Small : use `WINDOW_INTERVAL = 200` and `VEC_DIM = 10`

Currently, only one model is shared in `sample` directory, but you can download other ones from `https://momo.pjknkda.com/chasejung_pretrained`. Directory is configured according to the training environment, and in the directory, there are 3 pre-trained models according to (1) best validation loss (`valid_loss.x-y.hdf5`) (2) best testing accuracy (`test_acc.x-y.hdf5`) (3) best testing macro F1 score (`test_macro_f1.x-y.hdf5`). Choose one model and download it with `setup.json` file.

The performance overview of each traning environment is summrized at `https://momo.pjknkda.com/chasejung_pretrained/table.csv`.