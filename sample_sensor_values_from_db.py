import json
import os
import random

import msgpack
import msgpack_numpy

_DB_PATH = 'db_v6'


def _select_db_filename() -> str:
    available_db_filenames = []
    for filename in sorted(os.listdir(_DB_PATH)):
        if not filename.endswith('.msgpack'):
            continue
        available_db_filenames.append(filename)

    if not available_db_filenames:
        raise RuntimeError('No available DB')

    print('=== List of DB ===')
    for idx, db_filename in enumerate(available_db_filenames):
        print('[%d] %s' % (idx, db_filename))

    while True:
        chosen_db_idx = int(input('Target DB index > '))
        if not (0 <= chosen_db_idx < len(available_db_filenames)):
            continue
        break

    return available_db_filenames[chosen_db_idx]


db_filename = _select_db_filename()

with open(os.path.join(_DB_PATH, db_filename), 'rb') as db_f:
    db = msgpack.unpack(db_f, use_list=False, object_hook=msgpack_numpy.decode)

print('== Sensors ==')
print(db['sensors'])
print()

print('== One sensor values ==')
with open('sample.json', 'w') as f:
    json.dump(
        random.choice(db['exp_sensor_values'])[1].tolist(),  # nosec
        f,
        indent=2
    )
print('saved to `sample.json`')
