from os import path

from setuptools import setup

wdir = path.abspath(path.dirname(__file__))


with open(path.join(wdir, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

install_requires = [
    'aiohttp[speedups]==3.7.4.post0',
    'fasttext==0.9.2',
    'ffmpeg-python==0.2.0',
    'librosa==0.8.0',
    'lz4==3.1.3',
    'matplotlib==3.3.4',
    'msgpack-numpy==0.4.7.1',
    'msgpack==1.0.2',
    'nltk==3.5',
    'numpy==1.18.5',  # `tensorflow-gpu` requires < 1.19.0
    'pandas==1.2.3',
    'pydot==1.4.2',
    'scikit-learn==0.24.1',
    'scipy==1.6.1',
    'tensorflow-gpu==2.3.1',
    'tqdm==4.59.0',
]

dev_install_requires = [
    'autopep8==1.5.5',
    'bandit==1.7.0',
    'flake8==3.9.0',
    'mypy==0.812',
    'pip-tools==5.5.0',
]

extra_install_requires = [
    'mysql-connector-python==8.0.23',
]

setup(
    name='suggestbot-emotion-engine',

    version='0.0.1',

    description='Emotion recognition model for SuggestBot',
    long_description=long_description,
    url='https://gitlab.com/pjknkda/suggestbot-emotion-engine',

    author='Jungkook Park',
    author_email='pjknkda@gmail.com',

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X'
    ],

    py_modules=[
        '0_wav_to_wavf',
        '1_raw_to_data_debate_v2',
        '1_raw_to_data_drm1',
        '1_raw_to_data_drm2',
        '1x_data_merger',
        '2_make_db',
        '3_classifier_cnnrnn_cbam',
        '3_classifier_hybrid',
        '4_server',
    ],

    python_requires='>=3.8, <3.9',

    install_requires=install_requires,
    extras_require={'dev': dev_install_requires, 'extra': extra_install_requires},
)
