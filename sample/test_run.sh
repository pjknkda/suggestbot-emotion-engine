#!/bin/bash

echo "Get model info..."
curl -sS -i -X GET \
    'http://127.0.0.1:5050/model_info'
echo
echo

echo "Get prediction result..."
time curl -sS -i -X POST \
    -F "sensor_values=<audio_dense_sample.json" \
    'http://127.0.0.1:5050/predict'
