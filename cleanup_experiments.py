import os.path
import shutil
import sys

from app.utils import load_result_dir

result_dir = input('result dir > ')
max_rank = int(input('max rank (1...) > '))

_, _, table_entries = load_result_dir(result_dir)

target_dir_names = [
    table_entry.exp_config.dirname
    for table_entry in table_entries
    if int(table_entry.rank) > max_rank
]

print('Will remove %d experiments among %d' % (len(target_dir_names), len(table_entries)))
if input('enter "ok" to continue > ') != 'ok':
    sys.exit(0)

for target_dir_name in target_dir_names:
    print(f'Remove "{target_dir_name}"...')
    shutil.rmtree(os.path.join(result_dir, target_dir_name))
