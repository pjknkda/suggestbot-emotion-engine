from __future__ import annotations

import gzip
import os
from typing import Any, NamedTuple

import msgpack
import msgpack_numpy
import mysql.connector
import numpy as np
import tqdm

_DATA_PATH = 'data'
_SUBJECT_ID_OFFSET = 1000

# Output Structure
'''
{
    'labels': [
        'a1',
        'a2',
        ...
    ],
    'experiments': [
        # format: (subject_id, start_ts, end_ts, label_values)
        (1, 1000, 2000, (1, 2, ...)),
        (1, 3000, 4500, (2, 2, ...)),
        ...
    },
    'sensors': [
        'e4_accel.x',
        'e4_accel.y',
        ...
    ],
    'sensor_values': [
        # format: `np.ndarray` with shape [sample_idx,]
        #         dtype: [('subject_id', 'int32'), ('ts', 'int64'), ('value', 'float64')]
        # e4_accel.x
        np.ndarray(...),
        # e4_accel.y
        np.ndarray(...),
        ...
    ]
}
'''


class Experiment(NamedTuple):
    subject_id: int
    start_ts: int
    end_ts: int
    label_values: list[float]


LABELS = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8']
LABEL_RENAME_DICT = {
    'a1': 'interest_1',
    'a2': 'interest_2',
    'a3': 'interest_3',
    'a4': 'interest_4',
    'a5': 'interest_5',
    'a6': 'interest_6',
    'a7': 'valence',
    'a8': 'arousal',
}
SENSORS = [
    'e4_accel.x', 'e4_accel.y', 'e4_accel.z',
    'e4_bvp', 'e4_gsr', 'e4_ibi', 'e4_skin_temp',
    'smart_accel.x', 'smart_accel.y', 'smart_accel.z',
    'smart_gyro.x', 'smart_gyro.y', 'smart_gyro.z',
    # 'attention', 'meditation',
    'brain_wave.delta', 'brain_wave.theta',
    'brain_wave.high_alpha', 'brain_wave.low_alpha',
    'brain_wave.high_beta', 'brain_wave.low_beta',
    'brain_wave.middle_gamma', 'brain_wave.low_gamma',
]
SENSOR_SOURCES = {
    'e4_accel.x': ('E4_ACC', 'subject_phone', 'x'),
    'e4_accel.y': ('E4_ACC', 'subject_phone', 'y'),
    'e4_accel.z': ('E4_ACC', 'subject_phone', 'z'),
    'e4_bvp': ('E4_BVP', 'subject_phone', 'value'),
    'e4_gsr': ('E4_EDA', 'subject_phone', 'value'),
    'e4_ibi': ('E4_IBI', 'subject_phone', 'value'),
    'e4_skin_temp': ('E4_TEMP', 'subject_phone', 'value'),
    'smart_accel.x': ('SmartphoneAcc', 'subject_phoneNumber', 'x'),
    'smart_accel.y': ('SmartphoneAcc', 'subject_phoneNumber', 'y'),
    'smart_accel.z': ('SmartphoneAcc', 'subject_phoneNumber', 'z'),
    'smart_gyro.x': ('SmartphoneGyr', 'subject_phoneNumber', 'x'),
    'smart_gyro.y': ('SmartphoneGyr', 'subject_phoneNumber', 'y'),
    'smart_gyro.z': ('SmartphoneGyr', 'subject_phoneNumber', 'z'),
    'attention': ('Attention', 'subject_phoneNumber', 'value'),
    'meditation': ('Meditation', 'subject_phoneNumber', 'value'),
    'brain_wave.delta': ('BrainWave', 'subject_phoneNumber', 'delta'),
    'brain_wave.theta': ('BrainWave', 'subject_phoneNumber', 'theta'),
    'brain_wave.high_alpha': ('BrainWave', 'subject_phoneNumber', 'highAlpha'),
    'brain_wave.low_alpha': ('BrainWave', 'subject_phoneNumber', 'lowAlpha'),
    'brain_wave.high_beta': ('BrainWave', 'subject_phoneNumber', 'highBeta'),
    'brain_wave.low_beta': ('BrainWave', 'subject_phoneNumber', 'lowBeta'),
    'brain_wave.middle_gamma': ('BrainWave', 'subject_phoneNumber', 'middleGamma'),
    'brain_wave.low_gamma': ('BrainWave', 'subject_phoneNumber', 'lowGamma'),
}


db_conn = mysql.connector.connect(
    user='root',
    password='chasejung',
    host='anne.pjknkda.com',
    port=13306,
    database='drm2_data'
)


def read_table_rows(table_name: str, columns: list[str]) -> list[dict[str, Any]]:
    rows = []

    with db_conn.cursor(dictionary=True) as c:
        q = 'SELECT %s FROM %s' % (','.join(columns), table_name)
        if table_name != 'Subject':
            q += ' ORDER BY timestamp ASC'

        c.execute(q)

        for row in tqdm.tqdm(c, desc='rows'):
            rows.append({
                column: row[column]
                for column in columns
            })

    return rows


def main() -> None:
    subject_rows = read_table_rows('Subject', ['phoneNumber'])

    phone_number_dict = dict()
    for idx, subject_row in enumerate(subject_rows):
        phone_number_dict[subject_row['phoneNumber']] = _SUBJECT_ID_OFFSET + idx

    guide_rows = read_table_rows('Guide', ['phoneNumber',
                                           'guideNo',
                                           'startTime',
                                           'endTime',
                                           *LABELS])

    with gzip.open(os.path.join(_DATA_PATH, 'drm2.msgpack.gz'), mode='wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_map_header(4))

        f.write(packer.pack('labels'))
        f.write(packer.pack(
            [LABEL_RENAME_DICT.get(label, label) for label in LABELS]
        ))

        survey_dict = dict()
        for guide_row in tqdm.tqdm(guide_rows, desc='surveys'):
            survey_dict[(guide_row['phoneNumber'], guide_row['guideNo'])] = guide_row

        experiments = []
        for guide_row in tqdm.tqdm(guide_rows, desc='guides'):
            survey_row = survey_dict.get((guide_row['phoneNumber'], guide_row['guideNo']))
            if survey_row is None:
                continue

            label_values = []
            for label in LABELS:
                val = getattr(survey_row, label)
                label_values.append(val if val is not None else float('NaN'))

            experiments.append(Experiment(
                phone_number_dict[guide_row['phoneNumber']],
                int(guide_row['startTime']),
                int(guide_row['endTime']),
                label_values
            ))

        f.write(packer.pack('experiments'))
        f.write(packer.pack(experiments))

        f.write(packer.pack('sensors'))
        f.write(packer.pack(SENSORS))

        f.write(packer.pack('sensor_values'))
        f.write(packer.pack_array_header(len(SENSORS)))  # do stream write for memory-efficiency
        for sensor in tqdm.tqdm(SENSORS, desc='sensors'):
            table_name, key_name, column_name = SENSOR_SOURCES[sensor]
            rows = read_table_rows(table_name, ['timestamp', key_name, column_name])

            values = []
            for row in tqdm.tqdm(rows, desc='samples'):
                # NOTE : some phone numbers have no leading 0
                phone_number = getattr(row, key_name)
                if phone_number not in phone_number_dict:
                    phone_number = '0' + phone_number

                values.append(tuple([
                    phone_number_dict[phone_number],
                    int(row['timestamp']),
                    float(getattr(row, column_name))
                ]))

            values_np = np.array(values, dtype=[('subject_id', 'int32'), ('ts', 'int64'), ('value', 'float64')])

            f.write(packer.pack(values_np))


if __name__ == '__main__':
    main()
