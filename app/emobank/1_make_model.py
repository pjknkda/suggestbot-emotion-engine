from __future__ import annotations

import collections
import io
import json
import os.path
import shutil
from pprint import pprint
from typing import Iterator

import h5py
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.utils import plot_model

from app.models import EmobankExpSetup, EmobankSample, SampleSplit
from app.utils import load_emobank_dataset

_DB_PATH = 'db_v6_emobank'


def _build_model(exp_setup: EmobankExpSetup) -> keras.Model:
    if exp_setup.RNN_UNIT == 'GRU':
        RNN_UNIT = keras.layers.GRU
    elif exp_setup.RNN_UNIT == 'LSTM':
        RNN_UNIT = keras.layers.LSTM
    else:
        raise RuntimeError('Unexpected RNN_UNIT option')

    def _RNN_BIDIRECTIONAL_WRAPPER(cell):
        return keras.layers.Bidirectional(cell) if exp_setup.RNN_BIDIRECTIONAL else cell

    layers = [
        keras.layers.InputLayer(input_shape=(None, exp_setup.FASTTEXT_VECTOR_DIM))
    ]

    # RNN layers

    layers.extend([
        _RNN_BIDIRECTIONAL_WRAPPER(
            RNN_UNIT(exp_setup.RNN_UNIT_CNT,
                     activation='relu',
                     return_sequences=True),
        ),
        keras.layers.LayerNormalization(),
    ])

    for i in range(1, exp_setup.RNN_NUM_LAYERS - 1):
        layers.extend([
            _RNN_BIDIRECTIONAL_WRAPPER(
                RNN_UNIT(exp_setup.RNN_UNIT_CNT, activation='relu', return_sequences=True)
            ),
            keras.layers.LayerNormalization(),
        ])

    layers.extend([
        _RNN_BIDIRECTIONAL_WRAPPER(
            RNN_UNIT(exp_setup.RNN_UNIT_CNT, activation='relu')
        ),
        keras.layers.LayerNormalization(),
    ])

    # Dense layers

    for i in range(exp_setup.DENSE_NUM_LAYERS):
        layers.extend([
            keras.layers.Dense(exp_setup.DENSE_UNIT_CNT, activation='relu'),
            keras.layers.BatchNormalization(),
        ])

    # Output layers

    layers.append(keras.layers.Dense(1, activation='linear'))

    return keras.Sequential(layers)


def _make_tf_dataset(exp_setup: EmobankExpSetup,
                     dataset: list[EmobankSample],
                     epochs: int) -> tuple[int, tf.data.Dataset]:
    def _dataset_generator() -> Iterator[tuple[np.ndarray, float]]:
        for sample in dataset:
            if exp_setup.TARGET_EMOTION_ID == 0:
                yield sample.text, sample.A
            elif exp_setup.TARGET_EMOTION_ID == 1:
                yield sample.text, sample.V

    tf_dataset = tf.data.Dataset.from_generator(
        _dataset_generator,
        output_types=(tf.float32, tf.float32),
        output_shapes=(
            tf.TensorShape([None, exp_setup.FASTTEXT_VECTOR_DIM]),
            tf.TensorShape([])
        )
    )
    tf_dataset = tf_dataset.shuffle(50 * exp_setup.BATCH_SIZE, reshuffle_each_iteration=True)
    tf_dataset = tf_dataset.repeat(epochs)
    tf_dataset = tf_dataset.padded_batch(
        exp_setup.BATCH_SIZE,
        padded_shapes=(
            tf.TensorShape([None, exp_setup.FASTTEXT_VECTOR_DIM]),
            tf.TensorShape([])
        )
    )

    steps_per_epoch = len(dataset) // exp_setup.BATCH_SIZE

    return steps_per_epoch, tf_dataset


class _TestEvaluationCallback(keras.callbacks.Callback):
    def __init__(
        self,
        test_spe: int,
        test_tf_dataset: tf.data.Dataset
    ) -> None:
        super().__init__()

        self.test_spe = test_spe
        self.test_tf_dataset = test_tf_dataset

        self.train_history: dict[str, list[float]] = {
            'train_loss': [],
            'train_MAE': [],
            'train_MAPE': [],

            'valid_loss': [],
            'valid_MAE': [],
            'valid_MAPE': [],

            'test_MAE': [],
            'test_MAPE': []
        }

        self._model_checkpoint_dict: dict[str, tuple[float, int, h5py.File] | None] = {
            'valid_loss': None,
            'test_MAE': None,
            'test_MAPE': None,
        }

    def on_epoch_end(
        self,
        epoch: int,
        logs: dict[str, float] | None = None
    ) -> None:
        logs = logs or {}

        _, test_MAE, test_MAPE = self.model.evaluate(self.test_tf_dataset, steps=self.test_spe)

        try:
            self.train_history['train_loss'].append(float(logs['loss']))
            self.train_history['valid_loss'].append(float(logs['val_loss']))

            self.train_history['train_MAE'].append(float(logs['MAE']))
            self.train_history['valid_MAE'].append(float(logs['val_MAE']))

            self.train_history['train_MAPE'].append(float(logs['MAPE']))
            self.train_history['valid_MAPE'].append(float(logs['val_MAPE']))
        except KeyError:
            # does not reach to validation step
            return

        self.train_history['test_MAE'].append(float(test_MAE))
        self.train_history['test_MAPE'].append(float(test_MAPE))

        _saved_model = None
        for metric_name, direction in [('valid_loss', -1),
                                       ('test_MAE', -1),
                                       ('test_MAPE', -1)]:
            checkpoint = self._model_checkpoint_dict[metric_name]
            metric_val = self.train_history[metric_name][-1]

            if (checkpoint is not None
                    and checkpoint[0] * direction >= metric_val * direction):
                continue

            if _saved_model is None:
                _saved_model_io = io.BytesIO()
                _saved_model = h5py.File(_saved_model_io, mode='w')
                _saved_model._io = _saved_model_io
                self.model.save(_saved_model)

            self._model_checkpoint_dict[metric_name] = (metric_val, epoch, _saved_model)

        print('\n')
        print(('Test evaluation :\n'
               '  MAE %.4f / MAPE %.4f') % (
            test_MAE, test_MAPE))
        print(('Best scores :\n'
               '  val_loss %.4f (@%d) / test_MAE %.4f (@%d) / test_MAPE %.4f (@%d)\n') % (
            *(self._model_checkpoint_dict['valid_loss'] or [0.0, -1])[:2],
            *(self._model_checkpoint_dict['test_MAE'] or [0.0, -1])[:2],
            *(self._model_checkpoint_dict['test_MAPE'] or [0.0, -1])[:2],
        ))
        print('\n')

    def flush_checkpoints(
        self,
        result_dir: str,
    ) -> None:
        for metric_name in ['valid_loss', 'test_MAE', 'test_MAPE']:
            checkpoint = self._model_checkpoint_dict[metric_name]
            if checkpoint is None:
                continue

            metric_val, epoch, _saved_model = checkpoint

            filename = os.path.join(result_dir, f'{metric_name}.{epoch:03d}-{metric_val:.4f}.hdf5')
            with open(filename, 'wb') as f:
                _saved_model._io.seek(0)
                shutil.copyfileobj(_saved_model._io, f)


def _save_train_history(
    result_dir: str,
    train_history: dict[str, list[float]],
) -> None:
    with open(os.path.join(result_dir, 'history.json'), 'w') as f:
        json.dump(train_history, f, indent=2)

    # draw loss plot
    plt.subplot(3, 1, 1)
    plt.title('Loss')
    plt.plot(train_history['train_loss'], label='train')
    plt.plot(train_history['valid_loss'], label='valid')
    plt.legend()

    # draw accuracy plot
    plt.subplot(3, 1, 2)
    plt.title('Accuracy (MAE)')
    plt.plot(train_history['train_MAE'], label='train')
    plt.plot(train_history['valid_MAE'], label='valid')
    plt.plot(train_history['test_MAE'], label='test')
    plt.legend()

    plt.subplot(3, 1, 3)
    plt.title('Accuracy (MAPE')
    plt.plot(train_history['train_MAPE'], label='train')
    plt.plot(train_history['valid_MAPE'], label='valid')
    plt.plot(train_history['test_MAPE'], label='test')
    plt.legend()

    plt.tight_layout()

    plt.savefig(os.path.join(result_dir, 'history.pdf'))
    plt.savefig(os.path.join(result_dir, 'history.png'))

    if os.getenv('NO_PLOT', '').lower() != 'true':
        plt.show()


def train_mode(exp_setup: EmobankExpSetup) -> None:
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    dataset = load_emobank_dataset(exp_setup.FASTTEXT_VECTOR_DIM, exp_setup.MAX_SENTENCE_LENGTH)

    dataset_split_counter = collections.Counter([  # type: ignore
        sample.split for sample in dataset
    ])
    pprint(dataset_split_counter)

    model = _build_model(exp_setup)
    model.compile(
        optimizer=keras.optimizers.Adam(),
        # optimizer=keras.optimizers.SGD(momentum=0.9, nesterov=True),
        # optimizer=keras.optimizers.SGD(),
        # optimizer=keras.optimizers.RMSprop(),
        loss='mean_squared_error',
        metrics=['MAE', 'MAPE']
    )
    model.summary()

    result_dir = os.path.join(_DB_PATH, exp_setup.RESULT_DIR)
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'setup.json'), 'w') as f:
        json.dump(exp_setup._asdict(), f, indent=2)

    plot_model(model,
               show_shapes=True,
               expand_nested=True,
               to_file=os.path.join(result_dir, 'model.pdf'))

    train_spe, train_tf_dataset = _make_tf_dataset(
        exp_setup, [sample for sample in dataset if sample.split == SampleSplit.TRAIN], exp_setup.MAX_EPOCHS)
    valid_spe, valid_tf_dataset = _make_tf_dataset(
        exp_setup, [sample for sample in dataset if sample.split == SampleSplit.DEV], 1)
    test_spe, test_tf_dataset = _make_tf_dataset(
        exp_setup, [sample for sample in dataset if sample.split == SampleSplit.TEST], 1)

    test_evaluation_callback = _TestEvaluationCallback(test_spe, test_tf_dataset)

    try:
        model.fit(
            train_tf_dataset,
            verbose=1,
            epochs=exp_setup.MAX_EPOCHS,
            steps_per_epoch=train_spe,
            validation_data=valid_tf_dataset,
            validation_steps=valid_spe,
            callbacks=[
                test_evaluation_callback,
                keras.callbacks.ReduceLROnPlateau(
                    patience=10,
                    factor=0.5,
                    min_lr=1e-5,
                    verbose=1
                ),
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    patience=30
                )
            ]
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    test_evaluation_callback.flush_checkpoints(result_dir)
    _save_train_history(result_dir, test_evaluation_callback.train_history)


def main() -> None:
    exp_setup = EmobankExpSetup.init_from_env()
    train_mode(exp_setup)


if __name__ == '__main__':
    main()
