from __future__ import annotations

import asyncio
import json
import logging
import os
from concurrent.futures import ThreadPoolExecutor

import numpy as np
import tensorflow as tf
from aiohttp import web
from tensorflow import keras
from tensorflow.keras import backend as K

app_logger = logging.getLogger(__name__)
app_logger.propagate = False
app_logger.setLevel(logging.INFO)

app_logger_handler = logging.StreamHandler()
app_logger_handler.setFormatter(
    logging.Formatter('[%(levelname)1.1s %(asctime)s %(threadName)s %(module)s:%(lineno)d] '
                      '%(message)s')
)
app_logger.addHandler(app_logger_handler)


model_path = os.getenv('MODEL_PATH')
if model_path is None:
    raise RuntimeError('Environment variable `MODEL_PATH` is not set!')

for model_setup_path in [model_path.replace('.hdf5', '.json'),
                         os.path.dirname(model_path) + '/setup.json']:
    if not os.path.exists(model_setup_path):
        continue
    with open(model_setup_path, 'r') as f:
        model_setup = json.load(f)['db']
    break
else:
    raise RuntimeError('Cannot find `setup.json` file for model!')


logging.info('Initialize tensorflow session...')
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)


def _round_acc(y_true, y_pred):
    y_pred_round = K.minimum(4.0, K.maximum(0.0, K.round(y_pred)))
    return K.mean(K.equal(y_true, y_pred_round), axis=-1)


model = keras.models.load_model(
    model_path,
    custom_objects={
        'round_acc': _round_acc
    }
)


threaded_executor = ThreadPoolExecutor(1)

app = web.Application(client_max_size=100 * 2**20)
routes = web.RouteTableDef()


@routes.get('/model_info')
async def model_info(request):
    return web.json_response({
        'model_setup': model_setup
    })


@routes.post('/predict')
async def predict(request):
    post_data = await request.post()

    try:
        sensor_values_str = post_data['sensor_values']
    except KeyError:
        return web.HTTPBadRequest(text='missing required parameter')

    try:
        sensor_values = json.loads(sensor_values_str)
    except ValueError:
        return web.HTTPBadRequest(text='failed deserialize JSON sensor values')

    try:
        sensor_values_np = np.asarray(sensor_values)
    except Exception:
        return web.HTTPBadRequest(text='failed convert sensor values to numpy array')

    model_input_shape = model.layers[0].output_shape[0][1:-1]  # remove batch dim (0) and channel dim (-1)

    if sensor_values_np.shape != model_input_shape:
        return web.HTTPBadRequest(text='the shape of sensor values is wrong; you=%s, model=%s' % (
            sensor_values_np.shape, model_input_shape))

    if sensor_values_np.dtype != np.float64:
        return web.HTTPBadRequest(text='the dtype of sensor values is not float64')

    sensor_values_np = np.expand_dims(sensor_values_np, axis=0)  # add batch dim
    sensor_values_np = np.expand_dims(sensor_values_np, axis=-1)  # add channel dim

    try:
        result_value = await asyncio.get_running_loop().run_in_executor(
            threaded_executor,
            model.predict,
            sensor_values_np
        )
        result_value = result_value[0]  # unravel batch dim
    except Exception:
        app_logger.exception('failed predict the result value')
        return web.HTTPInternalServerError(text='failed predict the result value')

    if result_value.shape[0] == 1:
        # regression
        return web.json_response({
            'value': float(result_value[0])
        })

    else:
        # classification
        return web.json_response({
            'value': int(result_value.argmax()),
            'prob': result_value.tolist()
        })


app.add_routes(routes)

if __name__ == '__main__':
    web.run_app(
        app,
        port=int(os.getenv('PORT', '5050'))
    )
