from __future__ import annotations

import gzip
import os
from typing import Any, NamedTuple

import msgpack
import msgpack_numpy
import mysql.connector
import numpy as np
import pandas as pd
import tqdm

_DATA_PATH = 'data'
_XLSX_SELF_PATH = 'raw_data/Data/emotion_labels/emotion_self'
_XLSX_OTHER_PATH = 'raw_data/Data/emotion_labels/emotion_other'
_WAV_DATA_PATH = 'raw_data/Data/debate_audio/'

# Constants
NUM_NON_AUDIO_SENSORS = 16
FFT_DIM = 129


# Output Structure
'''
{
    'labels': [
        'a1',
        'a2',
        ...
    ],
    'experiments': [
        # format: (pid, start_ts, end_ts, label_values)
        (1, 1000, 2000, (1, 2, ...)),
        (1, 3000, 4500, (2, 2, ...)),
        ...
    },
    'sensors': [
        'e4_accel.x',
        'e4_accel.y',
        ...
    ],
    'sensor_values': [
        # format: `np.ndarray` with shape [sample_idx,]
        #         dtype: [('pid', 'int8'), ('ts', 'int64'), ('value', 'float64')]
        # e4_accel.x
        np.ndarray(...),
        # e4_accel.y
        np.ndarray(...),
        ...
    ]
}
'''


class Experiment(NamedTuple):
    pid: int
    start_ts: int
    end_ts: int
    label_values: list[float]


class SensorValue(NamedTuple):
    pid: int
    ts: int
    value: float


LABELS = ['arousal', 'valence', 'stress_self', 'stress_other']
SENSORS = [
    'e4_accel.x', 'e4_accel.y', 'e4_accel.z',
    'e4_bvp', 'e4_gsr', 'e4_ibi', 'e4_skin_temp',
    'heart_rate',
    'brain_wave.high_alpha', 'brain_wave.low_alpha',
    'brain_wave.high_beta', 'brain_wave.low_beta',
    'brain_wave.middle_gamma', 'brain_wave.low_gamma',
    'brain_wave.theta', 'brain_wave.delta',
    *['audio.f%d' % i for i in range(FFT_DIM)]
]


SENSOR_SOURCES = {
    'e4_accel.x': ('E4_ACC', 'x', 'pid', 'timestamp'),
    'e4_accel.y': ('E4_ACC', 'y', 'pid', 'timestamp'),
    'e4_accel.z': ('E4_ACC', 'z', 'pid', 'timestamp'),
    'e4_bvp': ('E4_BVP', 'value', 'pid', 'timestamp'),
    'e4_gsr': ('E4_EDA', 'value', 'pid', 'timestamp'),
    'e4_ibi': ('E4_IBI', 'value', 'pid', 'timestamp'),
    'e4_skin_temp': ('E4_TEMP', 'value', 'pid', 'timestamp'),
    'heart_rate': ('Polar_HR', 'value', 'pid', 'timestamp'),
    'brain_wave.high_alpha': ('BrainWave', 'highAlpha', 'pid', 'timestamp'),
    'brain_wave.low_alpha': ('BrainWave', 'lowAlpha', 'pid', 'timestamp'),
    'brain_wave.high_beta': ('BrainWave', 'highBeta', 'pid', 'timestamp'),
    'brain_wave.low_beta': ('BrainWave', 'lowBeta', 'pid', 'timestamp'),
    'brain_wave.middle_gamma': ('BrainWave', 'middleGamma', 'pid', 'timestamp'),
    'brain_wave.low_gamma': ('BrainWave', 'lowGamma', 'pid', 'timestamp'),
    'brain_wave.theta': ('BrainWave', 'theta', 'pid', 'timestamp'),
    'brain_wave.delta': ('BrainWave', 'delta', 'pid', 'timestamp'),
}

PID_METADATA = {
    # PID ->
    1: (854, 25619),
    2: (854, 25619),
    3: (688, 20639),
    4: (688, 20639),
    5: (629, 18869),
    6: (629, 18869),
    7: (613, 18389),
    8: (613, 18389),
    9: (616, 18479),
    10: (616, 18479),
    11: (612, 18359),
    12: (612, 18359),
    13: (611, 18329),
    14: (611, 18329),
    15: (615, 18449),
    16: (615, 18449),
    17: (732, 21959),
    18: (732, 21959),
    19: (621, 18629),
    20: (621, 18629),
    21: (606, 18179),
    22: (606, 18179),
    23: (608, 18239),
    24: (608, 18239),
    25: (623, 18689),
    26: (623, 18689),
    27: (647, 19409),
    28: (647, 19409),
    29: (642, 19259),
    30: (642, 19259),
    31: (658, 19739),
    32: (658, 19739),
}


db_conn = mysql.connector.connect(
    user='root',
    password='password',
    host='localhost',
    port=3306,
    database='debate_data_v2',
    auth_plugin='mysql_native_password'
)


def _emotion_num_to_cateogry(val: int) -> int:
    # convert 2~10 -> 0~4
    if np.isnan(val):
        return np.nan
    return round((val / 2) - 1)


def _stress_num_to_category(val: int) -> int:
    # convert 0~20 -> 0~4
    if val <= 7:
        return 0
    elif val <= 10:
        return 1
    elif val <= 14:
        return 2
    elif val <= 17:
        return 3
    else:
        return 4


def read_table_rows(table_name: str, columns: list[str]) -> list[dict[str, Any]]:
    rows = []

    with db_conn.cursor(dictionary=True) as c:
        q = 'SELECT %s FROM %s' % (','.join(columns), table_name)
        if table_name != 'Subject':
            q += ' ORDER BY timestamp ASC'

        c.execute(q)

        for row in tqdm.tqdm(c, desc='rows'):
            rows.append({
                column: row[column]
                for column in columns
            })

    return rows


def create_experiment(
    pid: int,
    start_time: int,
    pdframe_self: pd.DataFrame,
    pdframe_other: pd.DataFrame,
    row_num: int
) -> Experiment:
    if row_num == 0:
        start_ts = start_time
    else:
        start_ts = int(pdframe_self.iloc[row_num - 1, 0] + start_time)

    end_ts = int(pdframe_self.iloc[row_num, 0] + start_time)
    arousal = _emotion_num_to_cateogry(pdframe_self.iloc[row_num, 2] + pdframe_other.iloc[row_num, 2])
    valence = _emotion_num_to_cateogry(pdframe_self.iloc[row_num, 3] + pdframe_other.iloc[row_num, 3])
    stress_self = _stress_num_to_category(pdframe_self.iloc[row_num, 4:9].sum())
    stress_other = _stress_num_to_category(pdframe_other.iloc[row_num, 4:9].sum())

    return Experiment(
        pid,
        start_ts,
        end_ts,
        [
            arousal,
            valence,
            stress_self,
            stress_other
        ]
    )


# (idx, start_ts, end_ts, stress_self, stress_other)
# In the data, we threw out the first row for convenient processing.
def read_xlsx_rows(sbjt_info_rows: list[dict[str, Any]]) -> list[Experiment]:
    rows: list[Experiment] = []

    for sbjt_info in tqdm.tqdm(sbjt_info_rows, desc='xlsx_files'):
        pid = int(sbjt_info['pid'])
        startTime = int(sbjt_info['startTime'])

        pdframe_self = pd.read_csv(f'{_XLSX_SELF_PATH}/P{pid}.self.csv', usecols=list(range(1, 9)))
        pdframe_other = pd.read_csv(f'{_XLSX_OTHER_PATH}/P{pid}.other.csv', usecols=list(range(1, 9)))

        # Scale for timestamp
        pdframe_self.iloc[:, 0] *= 1000
        pdframe_other.iloc[:, 0] *= 1000

        # Adjust the score of positive emotions to sum them up with negative emotions.
        # (e.g.) "Cheerful" : 4 -> "Cheerful" : 1
        for i in [3, 4]:
            pdframe_self.iloc[:, i] = 5 - pdframe_self.iloc[:, i]
            pdframe_other.iloc[:, i] = 5 - pdframe_other.iloc[:, i]

        # Calculate the minimum number of rows between self and other file.
        min_num_rows = min(pdframe_self.shape[0], pdframe_other.shape[0])

        # Arousal, Valence : Sum up the values from self & other
        # Stress : Categorize the average value for self and other, respectively
        for row_num in range(min_num_rows):
            experiment_obj = create_experiment(pid, startTime, pdframe_self, pdframe_other, row_num)
            rows.append(experiment_obj)

    return rows


def read_audio_files_by_column(sbjt_info_rows: list[dict[str, Any]], column_idx: int) -> list[SensorValue]:
    values = []

    for sbjt_info in sbjt_info_rows:
        pid = int(sbjt_info['pid'])
        if pid % 2 == 1:
            continue

        startTime = sbjt_info['startTime']
        with open(_WAV_DATA_PATH + 'p%d_3rd_%ds.wav.msgpack' % (pid, PID_METADATA[pid][0]), 'rb') as f:
            audio_dict = msgpack.load(f, object_hook=msgpack_numpy.decode)
        timestamps = audio_dict['timestamps']

        it = np.nditer(audio_dict['fft_bins'][:, column_idx], flags=['f_index'])
        while not it.finished:
            intensity = it[0]
            row = it.index
            timestamp = startTime + timestamps[row]

            values.append(SensorValue(pid, timestamp, intensity))
            values.append(SensorValue(pid + 1, timestamp, intensity))
            it.iternext()

    return values


def main() -> None:
    subject_rows = read_table_rows('Subject', ['pid', 'initTime', 'startTime', 'endTime'])
    experiments = read_xlsx_rows(subject_rows)

    with gzip.open(os.path.join(_DATA_PATH, 'debate.msgpack.gz'), mode='wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_map_header(4))

        f.write(packer.pack('labels'))
        f.write(packer.pack(LABELS))

        f.write(packer.pack('experiments'))
        f.write(packer.pack(experiments))

        f.write(packer.pack('sensors'))
        f.write(packer.pack(SENSORS))

        f.write(packer.pack('sensor_values'))
        f.write(packer.pack_array_header(len(SENSORS)))

        # Non-audio Sensors
        for sensor in tqdm.tqdm(SENSORS[:NUM_NON_AUDIO_SENSORS], desc='sensors'):
            table_name, *column_name = SENSOR_SOURCES[sensor]
            rows = read_table_rows(table_name, column_name)

            values = []
            for row in tqdm.tqdm(rows, desc=sensor):
                values.append(
                    SensorValue(
                        int(row.pid),
                        int(row.timestamp),
                        float(getattr(row, column_name[0]))
                    )
                )

            values_np = np.array(values, dtype=[('pid', 'int32'), ('ts', 'int64'), ('value', 'float64')])
            f.write(packer.pack(values_np))

        # Audio Sensors
        for audio_sensor_idx in tqdm.trange(len(SENSORS[NUM_NON_AUDIO_SENSORS:]), desc='audio_sensors'):
            audio_values = read_audio_files_by_column(subject_rows, audio_sensor_idx)
            audio_values_np = np.array(audio_values, dtype=[('pid', 'int32'), ('ts', 'int64'), ('value', 'float64')])
            f.write(packer.pack(audio_values_np))


if __name__ == '__main__':
    main()
