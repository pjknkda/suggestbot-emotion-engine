from __future__ import annotations

import gzip
import os
from typing import Any, NamedTuple

import msgpack
import msgpack_numpy
import mysql.connector
import numpy as np
import tqdm

_DATA_PATH = 'data'
_SUBJECT_ID_OFFSET = 0

# Output Structure
'''
{
    'labels': [
        'a1',
        'a2',
        ...
    ],
    'experiments': [
        # format: (subject_id, start_ts, end_ts, label_values)
        (1, 1000, 2000, (1, 2, ...)),
        (1, 3000, 4500, (2, 2, ...)),
        ...
    },
    'sensors': [
        'e4_accel.x',
        'e4_accel.y',
        ...
    ],
    'sensor_values': [
        # format: `np.ndarray` with shape [sample_idx,]
        #         dtype: [('subject_id', 'int32'), ('ts', 'int64'), ('value', 'float64')]
        # e4_accel.x
        np.ndarray(...),
        # e4_accel.y
        np.ndarray(...),
        ...
    ]
}
'''


class Experiment(NamedTuple):
    subject_id: int
    start_ts: int
    end_ts: int
    label_values: list[float]


LABELS = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8']
LABEL_RENAME_DICT = {
    'a1': 'interest_1',
    'a2': 'interest_2',
    'a3': 'interest_3',
    'a4': 'interest_4',
    'a5': 'interest_5',
    'a6': 'interest_6',
    'a7': 'valence',
    'a8': 'arousal',
}
SENSORS = [
    'e4_accel.x', 'e4_accel.y', 'e4_accel.z',
    'e4_bvp', 'e4_gsr', 'e4_ibi', 'e4_skin_temp',
    'smart_accel.x', 'smart_accel.y', 'smart_accel.z',
    'smart_gyro.x', 'smart_gyro.y', 'smart_gyro.z',
    'tag_accel.x', 'tag_accel.y', 'tag_accel.z',
    'tag_gyro.x', 'tag_gyro.y', 'tag_gyro.z',
    'tag_mag.x', 'tag_mag.y', 'tag_mag.z',
]
SENSOR_SOURCES = {
    'e4_accel.x': ('E4Accelerometer', 'x'),
    'e4_accel.y': ('E4Accelerometer', 'y'),
    'e4_accel.z': ('E4Accelerometer', 'z'),
    'e4_bvp': ('E4BVP', 'value'),
    'e4_gsr': ('E4GSR', 'value'),
    'e4_ibi': ('E4IBI', 'value'),
    'e4_skin_temp': ('E4SkinTemperature', 'value'),
    'smart_accel.x': ('SmartphoneAccelerometer', 'x'),
    'smart_accel.y': ('SmartphoneAccelerometer', 'y'),
    'smart_accel.z': ('SmartphoneAccelerometer', 'z'),
    'smart_gyro.x': ('SmartphoneGyroscope', 'x'),
    'smart_gyro.y': ('SmartphoneGyroscope', 'y'),
    'smart_gyro.z': ('SmartphoneGyroscope', 'z'),
    'tag_accel.x': ('SensorTag', 'accelerometer_x'),
    'tag_accel.y': ('SensorTag', 'accelerometer_y'),
    'tag_accel.z': ('SensorTag', 'accelerometer_z'),
    'tag_gyro.x': ('SensorTag', 'gyroscope_x'),
    'tag_gyro.y': ('SensorTag', 'gyroscope_y'),
    'tag_gyro.z': ('SensorTag', 'gyroscope_z'),
    'tag_mag.x': ('SensorTag', 'magnitude_x'),
    'tag_mag.y': ('SensorTag', 'magnitude_y'),
    'tag_mag.z': ('SensorTag', 'magnitude_z'),
}


db_conn = mysql.connector.connect(
    user='root',
    password='chasejung',
    host='anne.pjknkda.com',
    port=13306,
    database='drm_data'
)


def read_table_rows(table_name: str, columns: list[str]) -> list[dict[str, Any]]:
    rows = []

    with db_conn.cursor(dictionary=True) as c:
        q = 'SELECT %s FROM %s' % (','.join(columns), table_name)
        if table_name != 'Subject':
            q += ' ORDER BY timestamp ASC'

        c.execute(q)

        for row in tqdm.tqdm(c, desc='rows'):
            rows.append({
                column: row[column]
                for column in columns
            })

    return rows


def main() -> None:
    subject_rows = read_table_rows('Subject', ['phoneNumber'])

    phone_number_dict = dict()
    for idx, subject_row in enumerate(subject_rows):
        phone_number_dict[subject_row['phoneNumber']] = _SUBJECT_ID_OFFSET + idx

    guide_rows = read_table_rows(
        'Guide',
        [
            'subject_phoneNumber',
            'productNo',
            'startTime',
            'endTime'
        ]
    )

    survey_rows = read_table_rows(
        '_Survey',
        [
            'subject_phoneNumber',
            'productNo',
            *LABELS
        ]
    )

    with gzip.open(os.path.join(_DATA_PATH, 'drm1.msgpack.gz'), mode='wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_map_header(4))

        f.write(packer.pack('labels'))
        f.write(packer.pack(
            [LABEL_RENAME_DICT.get(label, label) for label in LABELS]
        ))

        survey_dict = dict()
        for survey_row in tqdm.tqdm(survey_rows, desc='surveys'):
            survey_dict[(survey_row['subject_phoneNumber'], survey_row['productNo'])] = survey_row

        experiments = []
        for guide_row in tqdm.tqdm(guide_rows, desc='guides'):
            survey_row = survey_dict.get((guide_row['subject_phoneNumber'], guide_row['productNo']))
            if survey_row is None:
                continue

            label_values = []
            for label in LABELS:
                val = getattr(survey_row, label)
                label_values.append(val if val is not None else float('NaN'))

            experiments.append(Experiment(
                phone_number_dict[guide_row['subject_phoneNumber']],
                int(guide_row['startTime']),
                int(guide_row['endTime']),
                label_values
            ))

        f.write(packer.pack('experiments'))
        f.write(packer.pack(experiments))

        f.write(packer.pack('sensors'))
        f.write(packer.pack(SENSORS))

        f.write(packer.pack('sensor_values'))
        f.write(packer.pack_array_header(len(SENSORS)))  # do stream write for memory-efficiency
        for sensor in tqdm.tqdm(SENSORS, desc='sensors'):
            table_name, column_name = SENSOR_SOURCES[sensor]
            rows = read_table_rows(table_name, ['timestamp', 'subject_phoneNumber', column_name])

            values = []
            for row in tqdm.tqdm(rows, desc='samples'):
                values.append(tuple([
                    phone_number_dict[row['subject_phoneNumber']],
                    int(row['timestamp']),
                    float(getattr(row, column_name))
                ]))

            values_np = np.array(values, dtype=[('subject_id', 'int32'), ('ts', 'int64'), ('value', 'float64')])

            f.write(packer.pack(values_np))


if __name__ == '__main__':
    main()
