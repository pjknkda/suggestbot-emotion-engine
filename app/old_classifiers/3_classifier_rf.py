import os
import os.path
from pprint import pprint

import cloudpickle  # noqa:F401
import numpy as np
import pandas as pd
from sklearn import ensemble, svm  # noqa:F401
from sklearn.decomposition import PCA  # noqa:F401
from sklearn.impute import SimpleImputer
from sklearn.model_selection import GridSearchCV

SURVEY_COLS = ['a%d' % idx for idx in range(1, 9)]
CV_N_JOBS = 4

# Total 22 sensors
_SENSOR_COLS = [
    ('e4_accel', ['x', 'y', 'z']),
    ('e4_bvp', ['value']),
    ('e4_gsr', ['value']),
    ('e4_ibi', ['value']),
    ('e4_skin_temp', ['value']),
    ('smart_accel', ['x', 'y', 'z']),
    ('smart_gyro', ['x', 'y', 'z']),
    ('sensor_tag', ['accelerometer_x',
                    'accelerometer_y',
                    'accelerometer_z',
                    'gyroscope_x',
                    'gyroscope_y',
                    'gyroscope_z',
                    'magnitude_x',
                    'magnitude_y',
                    'magnitude_z'])
]
FEATURE_NAEMES = ['guide-direction']
for sensor_name, cols in _SENSOR_COLS:
    for col in cols:
        # Total 8 stats for each sensor
        for stat in ['avg', 'min', 'max', 'var', 'fb', 'f0', 'f1', 'f2']:
            FEATURE_NAEMES.append('%s.%s.%s' % (sensor_name, col, stat))


def main():
    data_dir = os.getenv('DATA_DIR', 'db_v1')

    X_df = pd.read_msgpack(os.path.join(data_dir, 'X.msgpack'))
    ys = {y_col: pd.read_msgpack(os.path.join(data_dir, 'y_%s.msgpack' % y_col))
          for y_col in SURVEY_COLS}

    impute_method = 'mean'  # from the expreiment, no score difference from `median`

    # Do missing value imputation on X
    imp = SimpleImputer(missing_values=np.nan, strategy=impute_method)
    imputed_X_df = imp.fit_transform(X_df)  # it becomes `np.array` (N, num_feature)

    for target_col in SURVEY_COLS:
        print('For survey %s :' % (target_col, ))

        XX_df = imputed_X_df
        y_df = ys[target_col].values.ravel()  # convert to `np.array` (N,)

        # Remove feature rows that `y` is NaN
        no_nan_pos = np.argwhere(~np.isnan(y_df)).ravel()  # `np.array` (N,)
        XX_df = XX_df[no_nan_pos, :]
        y_df = y_df[no_nan_pos]

        # Reduce feature dimension
        # pca = PCA(n_components=50)
        # pca.fit(XX_df)
        # print('PCA is applied:', pca.explained_variance_)
        # XX_df = pca.transform(XX_df)

        clf = ensemble.RandomForestClassifier
        param_grid = {
            'n_estimators': [100, 200, 500],
            'max_depth': [None, 10, 50, 100],
            'max_features': ['auto', 'log2', 2, 4]
        }

        # clf = svm.SVC
        # param_grid = {
        #     'C': [1, 10, 100, 1000],
        #     'gamma': ['auto', 'scale'],
        #     'kernel': ['linear', 'rbf']
        # }

        grid_cv = GridSearchCV(estimator=clf(), iid=False, param_grid=param_grid, cv=10, n_jobs=CV_N_JOBS)
        grid_cv.fit(XX_df, y_df)
        print('  Best score:', grid_cv.best_score_)
        print('  Best params:', grid_cv.best_params_)

        best_clf = grid_cv.best_estimator_
        if isinstance(best_clf, ensemble.RandomForestClassifier):
            feature_importances = list(zip(FEATURE_NAEMES, best_clf.feature_importances_))
            feature_importances.sort(key=lambda tu: tu[1], reverse=True)
            print('  Feature Importance =>')
            pprint(feature_importances[:20])


if __name__ == '__main__':
    main()
