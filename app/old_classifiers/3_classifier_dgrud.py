import json
import os
import os.path
import re
import time

import matplotlib.pyplot as plt
import numpy as np
import sklearn.utils
import tensorflow as tf
from sklearn import metrics as sklearn_metrics
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras import initializers, layers

_DATA_DIR = 'db_v4'

# Total 22 sensors
_SENSOR_PROBE_DICT = {
    'e4_accel': ['x', 'y', 'z'],
    'e4_bvp': ['value'],
    'e4_gsr': ['value'],
    'e4_ibi': ['value'],
    'e4_skin_temp': ['value'],
    'smart_accel': ['x', 'y', 'z'],
    'smart_gyro': ['x', 'y', 'z'],
    'sensor_tag': ['accelerometer_x',
                   'accelerometer_y',
                   'accelerometer_z',
                   'gyroscope_x',
                   'gyroscope_y',
                   'gyroscope_z',
                   'magnitude_x',
                   'magnitude_y',
                   'magnitude_z']
}


class DGRUD_tuned(layers.Layer):

    def __init__(self, units, time_window_size):
        self.units = units
        self.time_window_size = time_window_size

    def build(self, input_shape):
        input_dim = input_shape[-1]

        self.mean_kernal = self.add_weight(
            shape=[1, self.time_window_size, input_dim],
            name='mean_kernel',
            initializer=initializers.truncated_normal(0.0, 0.01)
        )

        self.W_x = self.add_weight(
            shape=[input_dim, input_dim],
            name='decay_W'
        )

        self.b_x = self.add_weight(
            shape=[input_dim],
            name='decay_b'
        )

        self.W_g = self.add_weight(
            shape=[input_dim + self.units + input_dim, 2 * self.units],
            name='gate_kernel'
        )
        self.b_g = self.add_weight(
            shape=[2 * self.units],
            name='gate_bias',
            initializer=initializers.constant(1.0)
        )

        self.W_h = self.add_weight(
            shape=[input_dim + self.units + input_dim, self.units],
            name='candidate_kernel'
        )
        self.b_h = self.add_weight(
            shape=[self.units],
            name='candidate_bias',
            initializer=initializers.constant(1.0)
        )

        self.built = True

    def call(self, inputs, state, scope=None):
        input_dim = inputs.shape[-1].value
        h, d, last_observation, prev_imputed_input, prev_stored_values = state

        # -- input_processing --

        # pre_processing
        m = tf.cast(~tf.is_nan(inputs), tf.float32)  # build mask
        inputs = tf.where(tf.is_nan(inputs), tf.zeros_like(inputs), inputs)

        # smoothing last obs
        prev_stored_values = tf.reshape(
            prev_stored_values,
            shape=[-1, self.time_window_size, input_dim]
        )

        # smoothing cur obs
        new_last_observation = (1.0 - m) * last_observation + m * inputs
        new_stored_values = tf.concat(
            (
                prev_stored_values[:, 1:, :],
                tf.reshape(new_last_observation, shape=[-1, 1, input_dim])
            ),
            axis=1
        )
        smoothed_cur_observation = tf.reduce_mean(self.mean_kernel * new_stored_values, axis=1)

        # decay rate for x
        gamma_x = tf.exp(-tf.nn.relu(tf.matmul(d, self.W_x) + self.b_x))

        # input for current timestep
        X = m * smoothed_cur_observation + (1.0 - m) * gamma_x * \
            smoothed_cur_observation  # zero state as a default

        # for next iter
        new_stored_values = tf.reshape(
            new_stored_values,
            shape=[-1, self.time_window_size * input_dim]
        )

        # -- gates --
        ihm = tf.concat((X, h, m), 1)

        concat = tf.sigmoid(tf.matmul(ihm, self.W_g) + self.b_g)
        r, z = tf.split(concat, num_or_size_splits=2, axis=1)

        # -- candidate --

        # candidate_h
        irhm = tf.concat((X, r * h, m), axis=1)

        cand_h = tf.tanh(tf.matmul(irhm, self.W_h) + self.b_h)

        # decay rate for h
        new_h = z * h + (1.0 - z) * cand_h

        # update delta: add one, then add previous d to only missing values
        new_d = tf.ones_like(d) + (1.0 - m) * d

        return new_h, (new_h, new_d, new_last_observation, X, new_stored_values)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.units)


def _build_sensor_cnn_model(exp_setup, exp_param, vector_dim):
    num_probes = sum(len(probes) for probes in _SENSOR_PROBE_DICT.values())
    input_layer = keras.layers.Input(shape=(num_probes, vector_dim, 1))
    # => (?, num_probes, vector_dim, 1)
    print('input', input_layer.shape)

    sensor_layers = []
    used_probe_idx = 0
    for sensor_name in _SENSOR_PROBE_DICT.keys():
        num_probe = len(_SENSOR_PROBE_DICT[sensor_name])

        sensor_layer = keras.layers.Cropping2D(
            ((used_probe_idx, num_probes - used_probe_idx - num_probe), (0, 0))
        )(input_layer)
        # => (?, num_probe, vector_dim, 1)
        used_probe_idx += num_probe

        if exp_setup['sensor_gaussian_noise'] is not None:
            sensor_layer = keras.layers.GaussianNoise(exp_setup['sensor_gaussian_noise'])(sensor_layer)

        sensor_layer = keras.layers.Conv2D(
            exp_param['SENSOR_CNN_FILTER_CNT'],
            (num_probe, exp_param['SENSOR_CNN_FILTER_WIDTH'][0]),
            activation='relu'
        )(sensor_layer)
        sensor_layer = keras.layers.Dropout(1 - exp_param['SENSOR_CNN_KEEP_PROB'])(sensor_layer)
        # => (?, 1, vector_dim - x1, SENSOR_CNN_FILTER_CNT)

        for i in range(1, exp_param['SENSOR_CNN_DEPTH']):
            sensor_layer = keras.layers.Conv2D(
                exp_param['SENSOR_CNN_FILTER_CNT'],
                (1, exp_param['SENSOR_CNN_FILTER_WIDTH'][i]),
                activation='relu'
            )(sensor_layer)
            sensor_layer = keras.layers.Dropout(1 - exp_param['SENSOR_CNN_KEEP_PROB'])(sensor_layer)
            # => (?, 1, vector_dim - xk, SENSOR_CNN_FILTER_CNT)

        sensor_layer = keras.layers.Flatten()(sensor_layer)
        # => (?, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT)

        sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=1))(sensor_layer)
        sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=-1))(sensor_layer)
        # => (?, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT, 1)

        sensor_layers.append(sensor_layer)

    num_sensors = len(_SENSOR_PROBE_DICT)

    last_layer = keras.layers.Concatenate(axis=1)(sensor_layers)
    # => (?, num_sensors, SENSOR_CNN_FILTER_CNT * (vector_dim - xk), 1)

    last_layer = keras.layers.Conv2D(
        exp_param['SENSOR_MERGE_CNN_FILTER_CNT'],
        (num_sensors, exp_param['SENSOR_MERGE_CNN_FILTER_WIDTH'][0]),
        activation='relu'
    )(last_layer)
    # => (?, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT - y1, SENSOR_MERGE_CNN_FILTER_CNT)

    for i in range(1, exp_param['SENSOR_MERGE_CNN_DEPTH']):
        last_layer = keras.layers.Conv2D(
            exp_param['SENSOR_MERGE_CNN_FILTER_CNT'],
            (1, exp_param['SENSOR_MERGE_CNN_FILTER_WIDTH'][i]),
            activation='relu'
        )(last_layer)
        # => (?, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT - yk, SENSOR_MERGE_CNN_FILTER_CNT)

    last_layer = keras.layers.Flatten()(last_layer)
    # => (?, ((vector_dim - xk) * SENSOR_CNN_FILTER_CNT - yk) * SENSOR_MERGE_CNN_FILTER_CNT)

    model = keras.models.Model(inputs=input_layer, outputs=last_layer)

    model.summary()

    return model


def _build_cnn_rnn_model(exp_setup, exp_param, window_cnt, vector_dim):
    num_probes = sum(len(probes) for probes in _SENSOR_PROBE_DICT.values())
    input_layer = keras.layers.Input(shape=(window_cnt, num_probes, vector_dim, 1))
    # => (?, window_cnt, num_probes, vector_dim, 1)

    sensor_cnn_model = _build_sensor_cnn_model(exp_setup, exp_param, vector_dim)
    # => (?, feature_dim)
    sensor_layer = keras.layers.TimeDistributed(sensor_cnn_model)(input_layer)
    # => (?, window_cnt, feature_dim)

    for i in range(0, exp_param['MAIN_RNN_NUM_LAYERS'] - 1):
        last_layer = keras.layers.CuDNNGRU(exp_param['MAIN_RNN_UNIT_CNT'], return_sequences=True)(sensor_layer)
        last_layer = keras.layers.Dropout(1 - exp_param['MAIN_RNN_KEEP_PROB'])(last_layer)
    # => (?, MAIN_RNN_UNIT_CNT)

    last_layer = keras.layers.CuDNNGRU(exp_param['MAIN_RNN_UNIT_CNT'])(last_layer)
    last_layer = keras.layers.Dropout(1 - exp_param['MAIN_RNN_KEEP_PROB'])(last_layer)
    # => (?, MAIN_RNN_UNIT_CNT)

    if exp_setup['do_regression']:
        # regression approach
        last_layer = keras.layers.Dense(1, activation='linear')(last_layer)
        # => (?, 1)
    else:
        # classification approach
        last_layer = keras.layers.Dense(5, activation='softmax')(last_layer)
        # # => (?, 5)

    model = keras.models.Model(inputs=input_layer, outputs=last_layer)

    model.summary()

    return model


def round_accuracy(y_true, y_pred):
    return K.mean(K.equal(y_true, K.round(y_pred)), axis=-1)


def do_train(exp_setup, exp_param, model, X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, stratify=y)
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, stratify=y_train)

    # => train : valid : test = 8.1 : 0.9 : 1
    if exp_setup['class_weight']:
        class_weight = sklearn.utils.class_weight.compute_class_weight(
            'balanced',
            classes=np.unique(y_train),
            y=y_train
        )
        print('Class weight: ', class_weight)
    else:
        class_weight = None

    input_config = {
        'x': X_train,
        'y': y_train,
        'class_weight': class_weight,
        'batch_size': exp_param['BATCH_SIZE']
    }

    result_dir = os.path.join(_DATA_DIR, 'result_%d' % time.time())
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'setup.json'), 'w') as f:
        json.dump(exp_setup, f, indent=2)
    with open(os.path.join(result_dir, 'param.json'), 'w') as f:
        json.dump(exp_param, f, indent=2)

    if exp_setup['do_regression']:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            loss='mean_squared_error',
            metrics=[round_accuracy]
        )
        acc_name = 'round_accuracy'
    else:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy']
        )
        acc_name = 'acc'

    train_loss_history = []
    val_loss_history = []

    train_acc_history = []
    val_acc_history = []
    test_acc_history = []

    class _ConfisionMatrixCallback(keras.callbacks.Callback):
        def on_epoch_end(self, epoch, logs=None):
            logs = logs or {}

            print('\n')
            print('Test confusion matrix : \n')

            if exp_setup['do_regression']:
                y_hat = self.model.predict(X_test).flatten().round()
            else:
                y_hat = self.model.predict(X_test).argmax(axis=1)

            cm = sklearn_metrics.confusion_matrix(y_test, y_hat)
            print('\n', cm)

            accuracy = sklearn_metrics.accuracy_score(y_test, y_hat)

            mac_precision = sklearn_metrics.precision_score(y_test, y_hat, average='macro')
            mac_recall = sklearn_metrics.recall_score(y_test, y_hat, average='macro')
            mac_f1_score = sklearn_metrics.f1_score(y_test, y_hat, average='macro')

            mic_precision = sklearn_metrics.precision_score(y_test, y_hat, average='micro')
            mic_recall = sklearn_metrics.recall_score(y_test, y_hat, average='micro')
            mic_f1_score = sklearn_metrics.f1_score(y_test, y_hat, average='micro')

            if not test_acc_history or np.max(test_acc_history) < accuracy:
                # save the best accuracy model
                self.model.save(
                    os.path.join(result_dir, 'test_acc.{epoch:04d}-{accuracy:.3f}.hdf5').format(
                        epoch=epoch,
                        accuracy=accuracy
                    ),
                    overwrite=True
                )

            train_loss_history.append(logs['loss'])
            val_loss_history.append(logs['val_loss'])

            train_acc_history.append(logs[acc_name])
            val_acc_history.append(logs['val_%s' % acc_name])
            test_acc_history.append(accuracy)

            print('\n')
            print(('Test evaluation : \n'
                   '  acc. %.4f\n'
                   '  (macro) prec. %.4f  recall. %.4f  f1. %.4f\n'
                   '  (micro) prec. %.4f  recall. %.4f  f1. %.4f\n\n') % (
                accuracy,
                mac_precision, mac_recall, mac_f1_score,
                mic_precision, mic_recall, mic_f1_score))

    try:
        model.fit(
            **input_config,
            verbose=1,
            callbacks=[
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    patience=10
                ),
                keras.callbacks.ModelCheckpoint(
                    os.path.join(result_dir, 'val_loss.{epoch:04d}-{val_loss:.3f}.hdf5'),
                    monitor='val_loss',
                    save_best_only=True,
                    mode='min'
                ),
                keras.callbacks.ModelCheckpoint(
                    os.path.join(result_dir, 'val_acc.{epoch:04d}-{val_%s:.3f}.hdf5' % acc_name),
                    monitor='val_%s' % acc_name,
                    save_best_only=True,
                    mode='max'
                ),
                _ConfisionMatrixCallback()
            ],
            epochs=1000,
            validation_data=(X_valid, y_valid)
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    # draw loss plot
    plt.subplot(2, 1, 1)
    plt.title('Loss')
    plt.plot(train_loss_history, label='train')
    plt.plot(val_loss_history, label='val')
    plt.legend()

    # draw accuracy plot
    plt.subplot(2, 1, 2)
    plt.title('Accuracy')
    plt.plot(train_acc_history, label='train')
    plt.plot(val_acc_history, label='val')
    plt.plot(test_acc_history, label='test')
    plt.legend()

    plt.show()


def train_mode():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    available_db = []
    for filename in os.listdir(_DATA_DIR):
        m = re.match(r'MD(?P<MAX_DURATION>\d+)'
                     r'_WS(?P<WINDOW_SIZE>\d+)'
                     r'_WI(?P<WINDOW_INTERVAL>\d+)'
                     r'(_DS(?P<DURATION_SPLIT>\d+))?'
                     r'_VD(?P<VEC_DIM>\d+)',
                     filename)
        if not m:
            continue

        available_db.append(filename)

    if not available_db:
        raise RuntimeError('No available dataset')

    for idx, dataset in enumerate(available_db):
        print('[%d] %s' % (idx, dataset))

    while True:
        chosen_dataset_idx = int(input('Target dataset index > '))
        if not (0 <= chosen_dataset_idx < len(available_db)):
            continue
        break

    selected_dataset = available_db[chosen_dataset_idx]
    print('dataset:', selected_dataset)

    exp_setup = {
        'classifier': '3_classifier_dgrud',
        'do_regression': True,
        'class_weight': True,
        'sensor_gaussian_noise': 0.001,
        'data_dir': _DATA_DIR,
        'dataset': selected_dataset
    }

    exp_param = {
        'MAIN_RNN_UNIT_CNT': 128,
        'MAIN_RNN_KEEP_PROB': 0.5,
        'MAIN_RNN_NUM_LAYERS': 2,

        'BATCH_SIZE': 256,

        'SURVEY_QUESTION_ID': 6
    }

    np.random.seed(457700)

    X = np.load(os.path.join(_DATA_DIR, exp_setup['dataset'], 'X.npy'))
    # => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim}

    X = np.expand_dims(X, axis=-1)
    # => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim} X {channel == 1}

    ys = np.load(os.path.join(_DATA_DIR, exp_setup['dataset'], 'ys.npy'))
    # => {exp_idx} X {survey_idx}

    if isinstance(exp_param['SURVEY_QUESTION_ID'], list):
        y = np.round(np.mean(ys[:, exp_param['SURVEY_QUESTION_ID']], axis=1))
    else:
        y = ys[:, exp_param['SURVEY_QUESTION_ID']]
    # => {exp_idx}

    # Remove feature rows that `y` is NaN
    no_nan_pos = np.argwhere(~np.isnan(y)).ravel()
    X = X[no_nan_pos]
    y = y[no_nan_pos]

    window_cnt = X.shape[1]
    vector_dim = X.shape[3]

    model = _build_cnn_rnn_model(exp_setup, exp_param, window_cnt, vector_dim)

    do_train(exp_setup, exp_param, model, X, y)


# def freeze_mode():
#     available_result = []
#     for filename in os.listdir(_DATA_DIR):
#         m = re.match(r'result_(?P<TIMESTAMP>\d+)', filename)
#         if not m:
#             continue

#         available_result.append(filename)

#     if not available_result:
#         raise RuntimeError('No available result')

#     result_dir_to_model_file = dict()
#     for idx, result_dir in enumerate(available_result):
#         print('[%d] %s' % (idx, result_dir))

#         for filename in sorted(os.listdir(os.path.join(_DATA_DIR, result_dir)), reverse=True):
#             m = re.match(r'test_acc_weights\.(?P<EPOCH>\d+)-(?P<ACC>[\.\d]+)\.hdf5', filename)
#             if not m:
#                 continue
#             print('  Best test accuracy: %s' % m.group('ACC'))
#             result_dir_to_model_file[result_dir] = filename
#             break

#         with open(os.path.join(_DATA_DIR, result_dir, 'setup.json'), 'r') as f:
#             setup_str = json.dumps(json.load(f), indent=2)
#             print('\n'.join('  ' + s for s in setup_str.splitlines()))

#     while True:
#         chosen_result_dir_idx = int(input('Target result dir index > '))
#         if not (0 <= chosen_result_dir_idx < len(available_result)):
#             continue
#         break

#     selected_result_dir = available_result[chosen_result_dir_idx]
#     print('selected result dir:', selected_result_dir)

#     with open(os.path.join(_DATA_DIR, selected_result_dir, 'setup.json')) as f:
#         exp_setup = json.load(f)

#     with open(os.path.join(_DATA_DIR, selected_result_dir, 'param.json')) as f:
#         exp_param = json.load(f)

#     X = np.load(os.path.join(_DATA_DIR, exp_setup['dataset'], 'X.npy'))
#     # => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim}

#     window_cnt = X.shape[1]
#     vector_dim = X.shape[3]

#     if exp_setup['main_model'] == 'cnn':
#         model = _build_cnn_cnn_model(exp_setup, exp_param, window_cnt, vector_dim)
#     elif exp_setup['main_model'] == 'rnn':
#         model = _build_cnn_rnn_model(exp_setup, exp_param, window_cnt, vector_dim)
#     else:
#         raise ValueError('Unexpected main_model: %s' % exp_setup['main_model'])

#     model.load_weights(
#         os.path.join(_DATA_DIR, selected_result_dir, result_dir_to_model_file[selected_result_dir])
#     )

#     model.save(os.path.join(_DATA_DIR, '%s_model.hdf5' % selected_result_dir))


if __name__ == '__main__':
    # while True:
    #     mode = input('Which mode? (train/freeze) > ').lower()
    #     if mode not in ['train', 'freeze']:
    #         continue
    #     break

    # if mode == 'train':
    #     train_mode()
    # elif mode == 'freeze':
    #     freeze_mode()

    train_mode()
