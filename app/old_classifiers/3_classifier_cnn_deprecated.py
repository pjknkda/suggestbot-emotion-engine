import os
import os.path

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.impute import SimpleImputer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize
from tensorflow import keras
from tensorflow.keras import layers

# Total 22 sensors
_SENSOR_COLS = [
    ('e4_accel', ['x', 'y', 'z']),
    ('e4_bvp', ['value']),
    ('e4_gsr', ['value']),
    ('e4_ibi', ['value']),
    ('e4_skin_temp', ['value']),
    ('smart_accel', ['x', 'y', 'z']),
    ('smart_gyro', ['x', 'y', 'z']),
    ('sensor_tag', ['accelerometer_x',
                    'accelerometer_y',
                    'accelerometer_z',
                    'gyroscope_x',
                    'gyroscope_y',
                    'gyroscope_z',
                    'magnitude_x',
                    'magnitude_y',
                    'magnitude_z'])
]

FEATURE_NAEMES = ['guide-direction']
for sensor_name, cols in _SENSOR_COLS:
    FEATURE_NAEMES.append('%s.onoff' % sensor_name)
    for col in cols:
        # Total 8 stats for each sensor
        for stat in ['avg', 'min', 'max', 'var', 'fb', 'f0', 'f1', 'f2']:
            FEATURE_NAEMES.append('%s.%s.%s' % (sensor_name, col, stat))

SURVEY_QUESTION_ID = 7

# e4_accel.x
# e4_accel.y
# e4_accel.z
# e4_bvp.value
# e4_gsr.value
# e4_ibi.value
# e4_skin_temp.value
# smart_accel.x
# smart_accel.y
# smart_accel.z
# smart_gyro.x
# smart_gyro.y
# smart_gyro.z
# sensor_tag.accelerometer_x
# sensor_tag.accelerometer_y
# sensor_tag.accelerometer_z
# sensor_tag.gyroscope_x
# sensor_tag.gyroscope_y
# sensor_tag.gyroscope_z
# sensor_tag.magnitude_x
# sensor_tag.magnitude_y
# sensor_tag.magnitude_z


class MyModel(tf.keras.Model):

    def __init__(self, num_classes=10):
        super(MyModel, self).__init__(name='my_model')
        self.num_classes = num_classes
        # Define your layers here.
        self.e4_accel = layers.Conv2D(16, kernel_size=(3, 3), strides=(1, 1),
                                      activation='relu',
                                      input_shape=(3, 16))
        self.pool1 = layers.MaxPool2D(pool_size=(3, 2), strides=(3, 2))
        # TODO: define for every sensor
        # self.e4_bvp =
        # self.e4_gsr =
        # self.e4_ibi =
        # self.e4_skin_temp =
        # self.smart_accel =
        # self.gyro =
        # self.sensor_tag_accel =
        # self.sensor_tag_gyro =
        # self.sensor_tag_mag =
        self.pred = layers.Dense(num_classes, activation='softmax')

    def call(self, inputs):
        # Define your forward pass here,
        # using layers you previously defined (in `__init__`).

        # X_train shape : (1439, 20, 22, 16)
        print(inputs.get_shape())
        print("something")
        print(inputs[:, 0, :3, :].get_shape())  # first sec among 20 sec, first 3 sensors among 22
        x = self.e4_accel(inputs[:, 0, :3, :])
        x = self.pool1(x)
        # TODO: apply this for every sensor...
        # TODO: repeat this every second and concatenate...
        return self.pred(x)

    def compute_output_shape(self, input_shape):
        # You need to override this function if you want to use the subclassed model
        # as part of a functional-style model.
        # Otherwise, this method is optional.
        shape = tf.TensorShape(input_shape).as_list()
        shape[-1] = self.num_classes
        return tf.TensorShape(shape)


def main():
    np.random.seed(930519)

    data_dir = os.getenv('DATA_DIR', 'db_v3')

    X = np.load(os.path.join(data_dir, 'X.npy'))
    ys = np.load(os.path.join(data_dir, 'ys.npy'))

    print('X', X.shape)
    print('ys', ys.shape)

    y = ys[:, SURVEY_QUESTION_ID]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

    print(X_train.shape)
    print(X_test.shape)

    # tf.keras.Input(shape=(?,))
    # x = layers.Conv2D(32, kernel_size=3, activation='relu', input_shape=)

    model = MyModel(num_classes=5)

    # The compile step specifies the training configuration.
    model.compile(optimizer=tf.train.AdamOptimizer(0.001),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    # Trains for 5 epochs.
    model.fit(X_train, y_train, epochs=10, batch_size=32)

    return

    inputs = tf.keras.Input(shape=(20, 22, 16,))  # Returns a placeholder tensor
    print(inputs.get_shape())
    print(inputs[:, :3, :, :].get_shape())
    # A layer instance is callable on a tensor, and returns a tensor.
    x_e4_accel = layers.Conv2D(32, kernel_size=(3, 3), strides=(1, 1),
                               activation='relu',
                               input_shape=(20, 3, 16,))(inputs[0, :3, :, :])

    x = layers.MaxPool2D(pool_size=(3, 3), strides=(3, 3))(x_e4_accel)
    predictions = layers.Dense(5, activation='softmax')(x)

    model = tf.keras.Model(inputs=inputs, outputs=predictions)

    return

    model = keras.Sequential([
        # keras.layers.CuDNNGRU(64, input_shape=(X.shape[1:]), return_sequences=True),
        # keras.layers.Dropout(0.5),
        # keras.layers.CuDNNGRU(64),
        keras.layers.CuDNNGRU(16, input_shape=(X.shape[1:])),
        keras.layers.Dropout(0.5),
        keras.layers.Dense(16, activation=tf.nn.relu),
        keras.layers.Dropout(0.5),
        keras.layers.Dense(5, activation=tf.nn.softmax)
    ])

    model.compile(optimizer=keras.optimizers.Adam(),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()

    class _ConfisionMatrixCallback(keras.callbacks.Callback):
        def on_epoch_end(self, batch, logs={}):
            y_hat = self.model.predict(X_test).argmax(axis=1)
            cm = confusion_matrix(y_test, y_hat)
            print('\n', cm)

    history = model.fit(
        X_train, y_train,
        verbose=1,
        callbacks=[
            keras.callbacks.EarlyStopping(
                monitor='val_loss',
                patience=100
            ),
            keras.callbacks.ModelCheckpoint(
                os.path.join(data_dir, 'best_model.hdf5'),
                monitor='val_loss',
                save_best_only=True,
                save_weights_only=False,
                mode='auto',
                period=1
            ),
            _ConfisionMatrixCallback()
        ],
        epochs=500,
        batch_size=128,
        validation_split=0.1
    )

    # draw loss plot
    plt.plot(history.history['loss'], label='train loss')
    plt.plot(history.history['val_loss'], label='val loss')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
