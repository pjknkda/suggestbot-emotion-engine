import json
import os
import os.path
import re
import time

import matplotlib.pyplot as plt
import msgpack
import msgpack_numpy
import numpy as np
import sklearn.utils
import tensorflow as tf
from sklearn import metrics as sklearn_metrics
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import backend as K

_DB_PATH = 'db_v4'

DEFAULT_EXP_PARAM = {
    'MAIN_MODEL': 'rnn',  # cnn | rnn
    'DO_REGRESSION': False,
    'CLASS_WEIGHT': True,

    'SENSOR_GAUSSIAN_NOISE': 0.001,

    'SENSOR_CNN_FILTER_CNT': 64,
    'SENSOR_CNN_FILTER_WIDTH': [3, 3, 3],
    'SENSOR_CNN_KEEP_PROB': 0.8,
    'SENSOR_CNN_DEPTH': 3,

    'SENSOR_MERGE_CNN_FILTER_CNT': 64,
    'SENSOR_MERGE_CNN_FILTER_WIDTH': [6, 4, 2],
    'SENSOR_MERGE_CNN_DEPTH': 3,

    'MAIN_CNN_FILTER_CNT': 64,
    'MAIN_CNN_DEPTH': 3,

    'MAIN_RNN_UNIT_CNT': 128,
    'MAIN_RNN_KEEP_PROB': 0.5,
    'MAIN_RNN_NUM_LAYERS': 2,

    'EPOCH': 1000,
    'BATCH_SIZE': 256,
    'RESULT_DIR': 'result_%d' % time.time()
}

EXP_PARAM_CONVERTER = {
    'DO_REGRESSION': lambda v: v.lower() == 'true',
    'CLASS_WEIGHT': lambda v: v.lower() == 'true',

    'SENSOR_GAUSSIAN_NOISE': float,

    'SENSOR_CNN_FILTER_CNT': int,
    'SENSOR_CNN_FILTER_WIDTH': lambda vs: [int(v) for v in vs.split(',')],
    'SENSOR_CNN_KEEP_PROB': float,
    'SENSOR_CNN_DEPTH': int,

    'SENSOR_MERGE_CNN_FILTER_CNT': int,
    'SENSOR_MERGE_CNN_FILTER_WIDTH': lambda vs: [int(v) for v in vs.split(',')],
    'SENSOR_MERGE_CNN_DEPTH': int,

    'MAIN_CNN_FILTER_CNT': int,
    'MAIN_CNN_DEPTH': int,

    'MAIN_RNN_UNIT_CNT': int,
    'MAIN_RNN_KEEP_PROB': float,
    'MAIN_RNN_NUM_LAYERS': int,

    'EPOCH': int,
    'BATCH_SIZE': int,
}


def _build_sensor_cnn_model(sensor_groups, exp_param, vector_dim):
    num_total_sensors = sum(len(sensor_group) for sensor_group in sensor_groups)
    input_layer = keras.layers.Input(shape=(num_total_sensors, vector_dim, 1))
    # => (?batch, num_total_sensors, vector_dim, 1)

    sensor_layers = []
    used_sensor_idx = 0
    for sensor_group in sensor_groups:
        num_sensors = len(sensor_group)

        sensor_layer = keras.layers.Cropping2D(
            ((used_sensor_idx, num_total_sensors - used_sensor_idx - num_sensors), (0, 0))
        )(input_layer)
        # => (?batch, num_sensors, vector_dim, 1)
        used_sensor_idx += num_sensors

        if exp_param['SENSOR_GAUSSIAN_NOISE'] is not None:
            sensor_layer = keras.layers.GaussianNoise(exp_param['SENSOR_GAUSSIAN_NOISE'])(sensor_layer)

        sensor_layer = keras.layers.Conv2D(
            exp_param['SENSOR_CNN_FILTER_CNT'],
            (num_sensors, exp_param['SENSOR_CNN_FILTER_WIDTH'][0]),
            activation='relu'
        )(sensor_layer)
        sensor_layer = keras.layers.Dropout(1 - exp_param['SENSOR_CNN_KEEP_PROB'])(sensor_layer)
        # => (?batch, 1, vector_dim - x1, SENSOR_CNN_FILTER_CNT)

        for i in range(1, exp_param['SENSOR_CNN_DEPTH']):
            sensor_layer = keras.layers.Conv2D(
                exp_param['SENSOR_CNN_FILTER_CNT'],
                (1, exp_param['SENSOR_CNN_FILTER_WIDTH'][i]),
                activation='relu'
            )(sensor_layer)
            sensor_layer = keras.layers.Dropout(1 - exp_param['SENSOR_CNN_KEEP_PROB'])(sensor_layer)
            # => (?batch, 1, vector_dim - xk, SENSOR_CNN_FILTER_CNT)

        sensor_layer = keras.layers.Flatten()(sensor_layer)
        # => (?batch, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT)

        sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=1))(sensor_layer)
        sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=-1))(sensor_layer)
        # => (?batch, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT, 1)

        sensor_layers.append(sensor_layer)

    num_sensor_groups = len(sensor_groups)

    last_layer = keras.layers.Concatenate(axis=1)(sensor_layers)
    # => (?batch, num_sensor_groups, SENSOR_CNN_FILTER_CNT * (vector_dim - xk), 1)

    last_layer = keras.layers.Conv2D(
        exp_param['SENSOR_MERGE_CNN_FILTER_CNT'],
        (num_sensor_groups, exp_param['SENSOR_MERGE_CNN_FILTER_WIDTH'][0]),
        activation='relu'
    )(last_layer)
    # => (?batch, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT - y1, SENSOR_MERGE_CNN_FILTER_CNT)

    for i in range(1, exp_param['SENSOR_MERGE_CNN_DEPTH']):
        last_layer = keras.layers.Conv2D(
            exp_param['SENSOR_MERGE_CNN_FILTER_CNT'],
            (1, exp_param['SENSOR_MERGE_CNN_FILTER_WIDTH'][i]),
            activation='relu'
        )(last_layer)
        # => (?batch, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT - yk, SENSOR_MERGE_CNN_FILTER_CNT)

    last_layer = keras.layers.Flatten()(last_layer)
    # => (?batch, ((vector_dim - xk) * SENSOR_CNN_FILTER_CNT - yk) * SENSOR_MERGE_CNN_FILTER_CNT)

    model = keras.models.Model(inputs=input_layer, outputs=last_layer)

    model.summary()

    return model


def _build_cnn_cnn_model(sensor_groups, exp_param, window_cnt, vector_dim):
    num_total_sensors = sum(len(sensor_group) for sensor_group in sensor_groups)
    input_layer = keras.layers.Input(shape=(window_cnt, num_total_sensors, vector_dim, 1))
    # => (?batch, window_cnt, num_total_sensors, vector_dim, 1)

    sensor_cnn_model = _build_sensor_cnn_model(sensor_groups, exp_param, vector_dim)
    sensor_layer = keras.layers.TimeDistributed(sensor_cnn_model)(input_layer)
    # => (?batch, window_cnt, feature_dim)
    sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=-1))(sensor_layer)
    # => (?batch, window_cnt, feature_dim, 1)

    feature_dim = int(sensor_layer.shape[2])
    last_layer = keras.layers.Conv2D(exp_param['MAIN_CNN_FILTER_CNT'],
                                     (3, feature_dim), activation='relu')(sensor_layer)
    # => (?batch, window_cnt - x1, 1, MAIN_CNN_FILTER_CNT)

    for i in range(1, exp_param['MAIN_CNN_DEPTH']):
        last_layer = keras.layers.Conv2D(exp_param['MAIN_CNN_FILTER_CNT'], (3, 1), activation='relu')(last_layer)
        # => (?batch, window_cnt - xk, 1, MAIN_CNN_FILTER_CNT)

    last_layer = keras.layers.Flatten()(last_layer)
    # => (?batch, (window_cnt - xk) * MAIN_CNN_FILTER_CNT)

    if exp_param['DO_REGRESSION']:
        # regression approach
        last_layer = keras.layers.Dense(1, activation='linear')(last_layer)
        # => (?batch, 1)
    else:
        # classification approach
        last_layer = keras.layers.Dense(5, activation='softmax')(last_layer)
        # # => (?batch, 5)

    model = keras.models.Model(inputs=input_layer, outputs=last_layer)

    model.summary()

    return model


def _build_cnn_rnn_model(sensor_groups, exp_param, window_cnt, vector_dim):
    num_total_sensors = sum(len(sensor_group) for sensor_group in sensor_groups)
    input_layer = keras.layers.Input(shape=(window_cnt, num_total_sensors, vector_dim, 1))
    # => (?batch, window_cnt, num_total_sensors, vector_dim, 1)

    sensor_cnn_model = _build_sensor_cnn_model(sensor_groups, exp_param, vector_dim)
    # => (?batch, feature_dim)
    sensor_layer = keras.layers.TimeDistributed(sensor_cnn_model)(input_layer)
    # => (?batch, window_cnt, feature_dim)

    for i in range(0, exp_param['MAIN_RNN_NUM_LAYERS'] - 1):
        last_layer = keras.layers.CuDNNGRU(exp_param['MAIN_RNN_UNIT_CNT'], return_sequences=True)(sensor_layer)
        last_layer = keras.layers.Dropout(1 - exp_param['MAIN_RNN_KEEP_PROB'])(last_layer)
    # => (?batch, MAIN_RNN_UNIT_CNT)

    last_layer = keras.layers.CuDNNGRU(exp_param['MAIN_RNN_UNIT_CNT'])(last_layer)
    last_layer = keras.layers.Dropout(1 - exp_param['MAIN_RNN_KEEP_PROB'])(last_layer)
    # => (?batch, MAIN_RNN_UNIT_CNT)

    if exp_param['DO_REGRESSION']:
        # regression approach
        last_layer = keras.layers.Dense(1, activation='linear')(last_layer)
        # => (?batch, 1)
    else:
        # classification approach
        last_layer = keras.layers.Dense(5, activation='softmax')(last_layer)
        # # => (?batch, 5)

    model = keras.models.Model(inputs=input_layer, outputs=last_layer)

    model.summary()

    return model


def r_square(y_true, y_pred):
    SS_res = K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return (1 - SS_res/(SS_tot + K.epsilon()))


def do_train(exp_setup, exp_param, model, X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, stratify=y)
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, stratify=y_train)
    # => train : valid : test = 8.1 : 0.9 : 1

    if exp_param['CLASS_WEIGHT']:
        classes = np.unique(y_train)
        class_weight = sklearn.utils.class_weight.compute_class_weight(
            'balanced',
            classes=classes,
            y=y_train
        )
        class_weight_dict = {
            int(k): v
            for k, v in zip(classes, class_weight)
        }
        print('Class weight: ', class_weight_dict)
    else:
        class_weight_dict = None

    input_config = {
        'x': X_train,
        'y': y_train,
        'class_weight': class_weight_dict,
        'batch_size': exp_param['BATCH_SIZE']
    }

    result_dir = os.path.join(_DB_PATH, exp_param['RESULT_DIR'])
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'setup.json'), 'w') as f:
        json.dump(exp_setup, f, indent=2)
    with open(os.path.join(result_dir, 'param.json'), 'w') as f:
        json.dump(exp_param, f, indent=2)

    if exp_param['DO_REGRESSION']:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            loss='mean_squared_error',
            metrics=[r_square]
        )
        acc_name = 'r_square'
    else:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy']
        )
        acc_name = 'acc'

    train_loss_history = []
    val_loss_history = []

    train_acc_history = []
    val_acc_history = []
    test_acc_history = []

    class _ConfisionMatrixCallback(keras.callbacks.Callback):
        def on_epoch_end(self, epoch, logs=None):
            logs = logs or {}
            if exp_param['DO_REGRESSION']:
                y_hat = self.model.predict(X_test).flatten().round()

                accuracy = sklearn_metrics.r2_score(y_test, y_hat)

                mac_precision = np.nan
                mac_recall = np.nan
                mac_f1_score = np.nan

                mic_precision = np.nan
                mic_recall = np.nan
                mic_f1_score = np.nan
            else:
                y_hat = self.model.predict(X_test).argmax(axis=1)

                print('\n')
                print('Test confusion matrix : ')
                cm = sklearn_metrics.confusion_matrix(y_test, y_hat)
                print(cm)

                accuracy = sklearn_metrics.accuracy_score(y_test, y_hat)

                mac_precision = sklearn_metrics.precision_score(y_test, y_hat, average='macro')
                mac_recall = sklearn_metrics.recall_score(y_test, y_hat, average='macro')
                mac_f1_score = sklearn_metrics.f1_score(y_test, y_hat, average='macro')

                mic_precision = sklearn_metrics.precision_score(y_test, y_hat, average='micro')
                mic_recall = sklearn_metrics.recall_score(y_test, y_hat, average='micro')
                mic_f1_score = sklearn_metrics.f1_score(y_test, y_hat, average='micro')

            if not test_acc_history or np.max(test_acc_history) < accuracy:
                # save the best accuracy model
                self.model.save(
                    os.path.join(result_dir, 'test_acc.{epoch:04d}-{accuracy:.3f}.hdf5').format(
                        epoch=epoch,
                        accuracy=accuracy
                    ),
                    overwrite=True
                )

            train_loss_history.append(float(logs['loss']))
            val_loss_history.append(float(logs['val_loss']))

            train_acc_history.append(float(logs[acc_name]))
            val_acc_history.append(float(logs['val_%s' % acc_name]))
            test_acc_history.append(float(accuracy))

            print('\n')
            print(('Test evaluation : \n'
                   '  %s. %.4f\n'
                   '  (macro) prec. %.4f  recall. %.4f  f1. %.4f\n'
                   '  (micro) prec. %.4f  recall. %.4f  f1. %.4f\n\n') % (
                acc_name, accuracy,
                mac_precision, mac_recall, mac_f1_score,
                mic_precision, mic_recall, mic_f1_score))

    class _PreserveLastNCheckpoint(keras.callbacks.Callback):
        def __init__(self, n):
            self.n = n

        def on_epoch_end(self, epoch, logs=None):
            targets = [
                ('val_loss', -1),
                ('val_acc', 1),
                ('test_loss', -1),
                ('test_acc', 1),
            ]

            model_files = os.listdir(result_dir)

            def _get_metric_val(model_file):
                m = re.match(
                    r'(?P<metric_name>.+)\.(?P<epoch>\d+)\-(?P<val>\-?\d+\.\d+)\.hdf5',
                    model_file
                )
                return float(m.group('val'))

            discarded_model_files = []
            for target_metric_name, target_order in targets:
                target_model_files = []
                for model_file in model_files:
                    if not model_file.endswith('.hdf5'):
                        continue
                    if not model_file.startswith(target_metric_name + '.'):
                        continue
                    target_model_files.append(model_file)

                target_model_files.sort(key=lambda model_file: target_order * _get_metric_val(model_file))
                discarded_model_files.extend(target_model_files[:-self.n])

            for model_file in discarded_model_files:
                os.unlink(os.path.join(result_dir, model_file))

    try:
        model.fit(
            **input_config,
            verbose=1,
            callbacks=[
                keras.callbacks.ModelCheckpoint(
                    os.path.join(result_dir, 'val_loss.{epoch:04d}-{val_loss:.3f}.hdf5'),
                    monitor='val_loss',
                    save_best_only=True,
                    mode='min'
                ),
                keras.callbacks.ModelCheckpoint(
                    os.path.join(result_dir, 'val_acc.{epoch:04d}-{val_%s:.3f}.hdf5' % acc_name),
                    monitor='val_%s' % acc_name,
                    save_best_only=True,
                    mode='max'
                ),
                _ConfisionMatrixCallback(),
                _PreserveLastNCheckpoint(10),
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    patience=5
                ),
            ],
            epochs=exp_param['EPOCH'],
            validation_data=(X_valid, y_valid)
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    with open(os.path.join(result_dir, 'history.json'), 'w') as f:
        json.dump({
            'train_loss_history': train_loss_history,
            'val_loss_history': val_loss_history,
            'train_acc_history': train_acc_history,
            'val_acc_history': val_acc_history,
            'test_acc_history': test_acc_history
        }, f, indent=2)

    if os.getenv('NO_PLOT', '').lower() != 'true':
        # draw loss plot
        plt.subplot(2, 1, 1)
        plt.title('Loss')
        plt.plot(train_loss_history, label='train')
        plt.plot(val_loss_history, label='val')
        plt.legend()

        # draw accuracy plot
        plt.subplot(2, 1, 2)
        plt.title('Accuracy')
        plt.plot(train_acc_history, label='train')
        plt.plot(val_acc_history, label='val')
        plt.plot(test_acc_history, label='test')
        plt.legend()

        plt.show()


def _select_db_filename():
    available_db_filenames = []
    for filename in os.listdir(_DB_PATH):
        if not filename.endswith('.msgpack'):
            continue
        available_db_filenames.append(filename)

    if not available_db_filenames:
        raise RuntimeError('No available DB')

    print('=== List of DB ===')
    for idx, db_filename in enumerate(available_db_filenames):
        print('[%d] %s' % (idx, db_filename))

    env_db_filename = os.getenv('DB_FILENAME')
    if (env_db_filename is not None
            and env_db_filename in available_db_filenames):
        return env_db_filename

    while True:
        chosen_db_idx = int(input('Target DB index > '))
        if not (0 <= chosen_db_idx < len(available_db_filenames)):
            continue
        break

    return available_db_filenames[chosen_db_idx]


def _select_label_ids(labels):
    print('=== List of labels ===')
    for idx, label in enumerate(labels):
        print('[%d] %s' % (idx, label))

    env_label_ids = os.getenv('LABEL_IDS')
    if env_label_ids is not None:
        env_label_ids = list(map(int, env_label_ids.split(',')))
        if 0 <= env_label_ids[0] <= env_label_ids[-1] < len(labels):
            return env_label_ids

    while True:
        try:
            chosen_label_ids = list(map(int, input('Target label ids (sep: ,) > ').split(',')))
        except ValueError:
            continue

        chosen_label_ids.sort()

        if not (0 <= chosen_label_ids[0] <= chosen_label_ids[-1] < len(labels)):
            continue
        break

    return chosen_label_ids if 1 < len(chosen_label_ids) else chosen_label_ids[0]


def train_mode(exp_param):
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    select_db_filename = _select_db_filename()
    print('DB:', select_db_filename)

    with open(os.path.join(_DB_PATH, select_db_filename), 'rb') as f:
        db = msgpack.unpack(f, use_list=False, max_bin_len=4*2**30, object_hook=msgpack_numpy.decode)

    selected_label_ids = _select_label_ids(db['labels'])
    print('Labels:', selected_label_ids)

    exp_setup = {
        'db': {
            'filename': select_db_filename,
            'config': db['config'],
            'sensors': db['sensors'],
            'sensor_scales': db['sensor_scales']
        },
        'label_ids': selected_label_ids,
    }

    np.random.seed(457700)

    X = np.array([
        sensor_value
        for _, sensor_value in db['exp_sensor_values']
    ])
    # (1) => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim}
    # (2) => [ {window_idx} X {sensor_idx} X {vector_dim}, ... ]

    print('Input X.shape', X.shape)

    if len(X.shape) == 4:
        # (1) when all exp has the same # of windows
        # => a single 4-dimensional tensor

        X = np.expand_dims(X, axis=-1)
        # => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim} X {channel == 1}

        window_cnt = X.shape[1]
        vector_dim = X.shape[3]
    elif len(X.shape) == 3:
        # (2) when each exp has variable # of windows
        # => a list of 3-dimensional tensor

        X = np.array([np.expand_dims(one_X, axis=-1) for one_X in X])
        # => [ {window_idx} X {sensor_idx} X {vector_dim} X {channel == 1}, ... ]

        window_cnt = None
        vector_dim = X[0].shape[2]
    else:
        raise RuntimeError('Unexpected `X` shape!', X.shape)

    ys = np.array([
        exp[3]  # (subject_id, start_ts, end_ts, label_values)
        for exp, _ in db['exp_sensor_values']
    ])
    # => {exp_idx} X {label_idx}

    print('Input ys.shape', ys.shape)

    if isinstance(exp_setup['label_ids'], list):
        y = np.round(np.mean(ys[:, exp_setup['label_ids']], axis=1))
    else:
        y = ys[:, exp_setup['label_ids']]
    # => {exp_idx}

    # Remove feature rows that `y` is NaN
    no_nan_pos = np.argwhere(~np.isnan(y)).ravel()
    X = X[no_nan_pos]
    y = y[no_nan_pos]

    print('Valid X.shape', X.shape)
    print('Valid ys.shape', ys.shape)

    sensor_groups = [['_X']]
    for sensor in db['sensors']:
        last_group = sensor_groups[-1]
        if last_group[-1].split('.')[0] == sensor.split('.')[0]:
            last_group.append(sensor)
        else:
            sensor_groups.append([sensor])
    sensor_groups = sensor_groups[1:]

    print('Sensor Groups', sensor_groups)

    if exp_param['MAIN_MODEL'] == 'cnn':
        if window_cnt is None:
            raise RuntimeError('CNN classifier is not usable to uneven duration data')
        model = _build_cnn_cnn_model(sensor_groups, exp_param, window_cnt, vector_dim)
    elif exp_param['MAIN_MODEL'] == 'rnn':
        model = _build_cnn_rnn_model(sensor_groups, exp_param, window_cnt, vector_dim)
    else:
        raise ValueError('Unexpected main_model: %s' % exp_param['MAIN_MODEL'])

    do_train(exp_setup, exp_param, model, X, y)


def main():
    exp_param = {**DEFAULT_EXP_PARAM}
    for key in DEFAULT_EXP_PARAM.keys():
        env_val = os.getenv(key)
        if env_val is None:
            continue
        exp_param[key] = EXP_PARAM_CONVERTER.get(key, lambda v: v)(env_val)

    train_mode(exp_param)


if __name__ == '__main__':
    main()
