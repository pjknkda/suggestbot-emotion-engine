import json
import os
import os.path
import time

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn import metrics as sklearn_metrics
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize
from tensorflow import keras
from tensorflow.keras import backend as K

MAIN_RNN_UNIT_CNT = 64
MAIN_RNN_KEEP_PROB = 0.5
MAIN_RNN_NUM_LAYERS = 2

BATCH_SIZE = 256

# Total 22 sensors
_SENSOR_COLS = [
    ('e4_accel', ['x', 'y', 'z']),
    ('e4_bvp', ['value']),
    ('e4_gsr', ['value']),
    ('e4_ibi', ['value']),
    ('e4_skin_temp', ['value']),
    ('smart_accel', ['x', 'y', 'z']),
    ('smart_gyro', ['x', 'y', 'z']),
    ('sensor_tag', ['accelerometer_x',
                    'accelerometer_y',
                    'accelerometer_z',
                    'gyroscope_x',
                    'gyroscope_y',
                    'gyroscope_z',
                    'magnitude_x',
                    'magnitude_y',
                    'magnitude_z'])
]
FEATURE_NAEMES = ['guide-direction']
for sensor_name, cols in _SENSOR_COLS:
    FEATURE_NAEMES.append('%s.onoff' % sensor_name)
    for col in cols:
        # Total 8 stats for each sensor
        for stat in ['avg', 'min', 'max', 'var', 'fb', 'f0', 'f1', 'f2']:
            FEATURE_NAEMES.append('%s.%s.%s' % (sensor_name, col, stat))

SURVEY_QUESTION_ID = 7


def main():
    np.random.seed(930519)

    data_dir = os.getenv('DATA_DIR', 'db_v2')

    X = np.load(os.path.join(data_dir, 'X.npy'))
    ys = np.load(os.path.join(data_dir, 'ys.npy'))

    print('X', X.shape)
    print('ys', ys.shape)

    # reverse time domain for better RNN training
    X = np.flip(X, 1)

    flatten_X = (X
                 # to (sensor, time, session)
                 .transpose(2, 1, 0)
                 # to (sensor, time * session)
                 .reshape(X.shape[2], -1)
                 # to (time * session, sensor)
                 .transpose(1, 0))

    def _nan_percent(M, axis=None):
        num_nan = np.count_nonzero(np.isnan(M), axis=axis)
        num_non_nan = np.count_nonzero(~np.isnan(M), axis=axis)
        return num_nan * 100 / (num_nan + num_non_nan)

    # calculate the portion of NaN
    print('Total NaN = %.2lf%%' % _nan_percent(flatten_X))

    # calculater the portion of NaN depending on sensor
    # print('Column NaN => ')
    # print(list(zip(FEATURE_NAEMES, _nan_percent(flatten_X, axis=0))))

    # print('Before remove too-NaN features', flatten_X.shape)
    # no_nan_features = _nan_percent(flatten_X, axis=0) < 50
    # flatten_X = flatten_X[:, no_nan_features]
    # print('After remove too-NaN features', flatten_X.shape)

    # impute NaN values in each sensor domain
    imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
    flatten_X = imputer.fit_transform(flatten_X)

    # normalize each sensor domain
    flatten_X = normalize(flatten_X, norm='max', axis=0)

    # print(flatten_X.max(axis=0))
    # print(flatten_X.min(axis=0))
    # print(flatten_X.mean(axis=0))

    X = (flatten_X
         # to (reduced_sensor, time * session)
         .transpose(1, 0)
         # to (reduced_sensor, time, session)
         .reshape(-1, X.shape[1], X.shape[0])
         # to (session, time, reduced_sensor)
         .transpose(2, 1, 0))

    y = ys[:, SURVEY_QUESTION_ID]

    print('Final X', X.shape)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, stratify=y)
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, stratify=y_train)
    # => train : valid : test = 8.1 : 0.9 : 1

    model_layers = []

    for i in range(0, MAIN_RNN_NUM_LAYERS - 1):
        model_layers.append(keras.layers.CuDNNGRU(MAIN_RNN_UNIT_CNT, input_shape=(X.shape[1:]), return_sequences=True))
        model_layers.append(keras.layers.Dropout(1 - MAIN_RNN_KEEP_PROB))
    # => (?, MAIN_RNN_UNIT_CNT)

    model_layers.append(keras.layers.CuDNNGRU(MAIN_RNN_UNIT_CNT, input_shape=(X.shape[1:])))
    model_layers.append(keras.layers.Dropout(1 - MAIN_RNN_KEEP_PROB))
    # => (?, MAIN_RNN_UNIT_CNT)

    model_layers.append(keras.layers.Dense(5, activation=tf.nn.softmax))
    # => (?, 5)

    model = keras.Sequential(model_layers)

    model.compile(optimizer=keras.optimizers.Adam(),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()

    result_dir = os.path.join(data_dir, 'result_%d' % time.time())
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'config.json'), 'w') as f:
        json.dump(
            {
                'MAIN_RNN_UNIT_CNT': MAIN_RNN_UNIT_CNT,
                'MAIN_RNN_KEEP_PROB': MAIN_RNN_KEEP_PROB,
                'MAIN_RNN_NUM_LAYERS': MAIN_RNN_NUM_LAYERS,
                'BATCH_SIZE': BATCH_SIZE
            }, f, indent=2
        )

    train_loss_history = []
    val_loss_history = []

    train_acc_history = []
    val_acc_history = []
    test_acc_history = []

    class _ConfisionMatrixCallback(keras.callbacks.Callback):
        def on_epoch_end(self, epoch, logs=None):
            logs = logs or {}

            print('\n')
            print('Test confusion matrix : \n')

            y_hat = self.model.predict(X_test).argmax(axis=1)

            cm = sklearn_metrics.confusion_matrix(y_test, y_hat)
            print('\n', cm)

            accuracy = sklearn_metrics.accuracy_score(y_test, y_hat)

            mac_precision = sklearn_metrics.precision_score(y_test, y_hat, average='macro')
            mac_recall = sklearn_metrics.recall_score(y_test, y_hat, average='macro')
            mac_f1_score = sklearn_metrics.f1_score(y_test, y_hat, average='macro')

            mic_precision = sklearn_metrics.precision_score(y_test, y_hat, average='micro')
            mic_recall = sklearn_metrics.recall_score(y_test, y_hat, average='micro')
            mic_f1_score = sklearn_metrics.f1_score(y_test, y_hat, average='micro')

            if not test_acc_history or np.max(test_acc_history) < accuracy:
                # save the best accuracy model
                self.model.save(
                    os.path.join(result_dir, 'test_acc.{epoch:04d}-{accuracy:.3f}.hdf5').format(
                        epoch=epoch,
                        accuracy=accuracy
                    ),
                    overwrite=True
                )

            train_loss_history.append(logs['loss'])
            val_loss_history.append(logs['val_loss'])

            train_acc_history.append(logs['acc'])
            val_acc_history.append(logs['val_acc'])
            test_acc_history.append(accuracy)

            print('\n')
            print(('Test evaluation : \n'
                   '  acc. %.4f\n'
                   '  (macro) prec. %.4f  recall. %.4f  f1. %.4f\n'
                   '  (micro) prec. %.4f  recall. %.4f  f1. %.4f\n\n') % (
                accuracy,
                mac_precision, mac_recall, mac_f1_score,
                mic_precision, mic_recall, mic_f1_score))

    try:
        model.fit(
            X_train, y_train,
            verbose=1,
            callbacks=[
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    patience=10
                ),
                keras.callbacks.ModelCheckpoint(
                    os.path.join(result_dir, 'val_loss.{epoch:04d}-{val_loss:.3f}.hdf5'),
                    monitor='val_loss',
                    save_best_only=True,
                    mode='min'
                ),
                keras.callbacks.ModelCheckpoint(
                    os.path.join(result_dir, 'val_acc.{epoch:04d}-{val_acc:.3f}.hdf5'),
                    monitor='val_acc',
                    save_best_only=True,
                    mode='max'
                ),
                _ConfisionMatrixCallback()
            ],
            epochs=500,
            batch_size=BATCH_SIZE,
            validation_data=(X_valid, y_valid)
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    # draw loss plot
    plt.subplot(2, 1, 1)
    plt.title('Loss')
    plt.plot(train_loss_history, label='train')
    plt.plot(val_loss_history, label='val')
    plt.legend()

    # draw accuracy plot
    plt.subplot(2, 1, 2)
    plt.title('Accuracy')
    plt.plot(train_acc_history, label='train')
    plt.plot(val_acc_history, label='val')
    plt.plot(test_acc_history, label='test')
    plt.legend()

    plt.show()


if __name__ == '__main__':
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    main()
