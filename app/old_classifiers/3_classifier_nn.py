import numpy as np
import pandas as pd
import torch
from sklearn import ensemble, svm
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import Imputer

SURVEY_COLS = ['a%d' % idx for idx in range(1, 9)]

X_df = pd.read_msgpack('db_v1/X.msgpack')
ys = {y_col: pd.read_msgpack('db_v1/y_%s.msgpack' % y_col)
      for y_col in SURVEY_COLS}

for impute_method in ['mean', 'median']:
    # Do missing value imputation on X
    imp = Imputer(missing_values=np.nan, strategy=impute_method, axis=0)
    imputed_X_df = imp.fit_transform(X_df)  # it becomes `np.array` (N, num_feature)

    for target_col in SURVEY_COLS:
        XX_df = imputed_X_df
        y_df = ys[target_col].values.ravel()  # convert to `np.array` (N,)

        # Remove feature rows that `y` is NaN
        no_nan_pos = np.argwhere(~np.isnan(y_df)).ravel()  # `np.array` (N,)
        XX_df = XX_df[no_nan_pos, :]
        y_df = y_df[no_nan_pos]

        # split train/test set
        N = XX_df.shape[0]
        cut = int(N/10*9)
        X_train = XX_df[:cut]
        X_test = XX_df[cut:]
        Y_train = y_df[:cut]
        Y_test = y_df[cut:]

        N, D_in, H, D_out = X_train.shape[0], X_train.shape[1], 50, 5

        x = torch.from_numpy(X_train).float()
        y = torch.from_numpy(Y_train).long()

        model = torch.nn.Sequential(
            torch.nn.Linear(D_in, H),
            torch.nn.ReLU(),
            #torch.nn.Linear(H, H),
            # torch.nn.ReLU(),
            torch.nn.Linear(H, D_out)
        )

        # train
        loss_fn = torch.nn.CrossEntropyLoss()
        learning_rate = 1e-2
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
        for epoch in range(1000):
            y_pred = model(x)
            loss = loss_fn(y_pred, y)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # test
        correct = 0
        total = 0
        with torch.no_grad():
            x = torch.from_numpy(X_test).float()
            y = torch.from_numpy(Y_test).long()
            outputs = model(x)
            _, predicted = torch.max(outputs, 1)
            # print(predicted, predicted.shape)
            # print(y, y.shape)
            total += y.size(0)
            correct += (predicted == y).sum().item()
        print('%s, accuracy: %f' % (target_col, correct / total))

'''

        for kernel_name in ['linear', 'rbf', 'sigmoid']:
            print('impute-%s, svm_regression+kernal-%s, survey-%s :'
                  % (impute_method, kernel_name, target_col))

            clf = svm.SVR(kernel=kernel_name)
            scores = cross_val_score(clf, XX_df, y_df, cv=10)
            print('  Accuracy: %0.2f (+/- %0.2f)' % (scores.mean(), scores.std() * 2))

        for n_estimators in [100, 200]:
            print('impute-%s, rf_regression+n-%s, survey-%s :'
                  % (impute_method, n_estimators, target_col))

            clf = ensemble.RandomForestRegressor(n_estimators=n_estimators)
            scores = cross_val_score(clf, XX_df, y_df, cv=10)
            print('  Accuracy: %0.2f (+/- %0.2f)' % (scores.mean(), scores.std() * 2))
'''
