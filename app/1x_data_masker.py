import gzip
import os

import msgpack
import msgpack_numpy
import tqdm

_DATA_PATH = 'data'

# Output Structure
'''
{
    'labels': [
        'a1',
        'a2',
        ...
    ],
    'experiments': [
        # format: (subject_id, start_ts, end_ts, label_values)
        (1, 1000, 2000, (1, 2, ...)),
        (1, 3000, 4500, (2, 2, ...)),
        ...
    },
    'sensors': [
        'e4_accel.x',
        'e4_accel.y',
        ...
    ],
    'sensor_values': [
        # format: `np.ndarray` with shape [sample_idx,]
        #         dtype: [('subject_id', 'int32'), ('ts', 'int64'), ('value', 'float64')]
        # e4_accel.x
        np.ndarray(...),
        # e4_accel.y
        np.ndarray(...),
        ...
    ]
}
'''


def main() -> None:
    while True:
        data_filename = input('data name > ') + '.msgpack.gz'
        if not os.path.exists(os.path.join(_DATA_PATH, data_filename)):
            print('"%s" does not exists' % data_filename)
            continue
        break

    while True:
        output_filename = input('output name > ') + '.msgpack.gz'
        if os.path.exists(os.path.join(_DATA_PATH, output_filename)):
            print('"%s" already exists' % output_filename)
            continue
        break

    print('Load Data...')
    with gzip.open(os.path.join(_DATA_PATH, data_filename)) as f:
        data = msgpack.unpack(f, use_list=False, max_bin_len=4 * 2**30, object_hook=msgpack_numpy.decode)

    alive_sensors = data['sensors'][:]
    while True:
        print('Sensors :', alive_sensors)

        filter_out_sensor_prefix = input('Sensor prefix to filter out (empty to exit) > ')
        if not filter_out_sensor_prefix:
            break

        alive_sensors = [
            sensor for sensor in alive_sensors if not sensor.startswith(filter_out_sensor_prefix)
        ]

    if not alive_sensors:
        raise RuntimeError('No sensors!!')

    with gzip.open(os.path.join(_DATA_PATH, output_filename), mode='wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_map_header(4))

        f.write(packer.pack('labels'))
        f.write(packer.pack(data['labels']))

        f.write(packer.pack('experiments'))
        f.write(packer.pack(data['experiments']))

        f.write(packer.pack('sensors'))
        f.write(packer.pack(alive_sensors))

        f.write(packer.pack('sensor_values'))
        f.write(packer.pack_array_header(len(alive_sensors)))  # do stream write for memory-efficiency
        for sensor in tqdm.tqdm(alive_sensors, desc='sensors'):
            data_values_np = data['sensor_values'][data['sensors'].index(sensor)]
            f.write(packer.pack(data_values_np))


if __name__ == '__main__':
    main()
