from __future__ import annotations

from tensorflow import keras


def _build_model_basic_block(
    input_layer: keras.layers.Layer,
    filter_num: int,
    stride: int = 1,
) -> keras.layers.Layer:
    conv_1_layer = keras.layers.Conv2D(
        filters=filter_num,
        kernel_size=(3, 3),
        strides=stride,
        padding='same',
        use_bias=False,
    )(input_layer)

    bn_1_layer = keras.layers.BatchNormalization()(conv_1_layer)
    bn_1_layer = keras.layers.ReLU()(bn_1_layer)

    conv_2_layer = keras.layers.Conv2D(
        filters=filter_num,
        kernel_size=(3, 3),
        strides=1,
        padding='same',
        use_bias=False,
    )(bn_1_layer)

    bn_2_layer = keras.layers.BatchNormalization()(conv_2_layer)

    if stride == 1:
        residual_layer = input_layer
    else:
        residual_layer = keras.layers.Conv2D(
            filters=filter_num,
            kernel_size=(1, 1),
            strides=stride,
            use_bias=False,
        )(input_layer)
        residual_layer = keras.layers.BatchNormalization()(residual_layer)

    output_layer = keras.layers.add([residual_layer, bn_2_layer])
    output_layer = keras.layers.ReLU()(output_layer)

    return output_layer


def _build_model_basic_block_layer(
    input_layer: keras.layers.Layer,
    filter_num: int,
    blocks: int,
    stride: int = 1,
) -> keras.layers.Layer:
    output_layer = _build_model_basic_block(input_layer, filter_num, stride)
    for _ in range(1, blocks):
        output_layer = _build_model_basic_block(output_layer, filter_num)
    return output_layer


def _resnet_type_1(
    layer_filters: list[int],
    layer_blocks: list[int],
) -> tuple[keras.layers.Input, keras.layers.Layer]:
    input_layer = keras.layers.Input(shape=(None, None, 1))
    # => (?batch, window_idx, n_feature, 1)

    conv_1_layer = keras.layers.Conv2D(
        filters=layer_filters[0],
        kernel_size=(7, 7),
        strides=2,
        padding='same',
        use_bias=False,
    )(input_layer)

    bn_1_layer = keras.layers.BatchNormalization()(conv_1_layer)
    bn_1_layer = keras.layers.ReLU()(bn_1_layer)

    pool_1_layer = keras.layers.MaxPool2D(
        pool_size=(3, 3),
        strides=2,
        padding='same',
    )(bn_1_layer)

    block_1_layer = _build_model_basic_block_layer(pool_1_layer, layer_filters[0], layer_blocks[0])
    block_2_layer = _build_model_basic_block_layer(block_1_layer, layer_filters[1], layer_blocks[1], stride=2)
    block_3_layer = _build_model_basic_block_layer(block_2_layer, layer_filters[2], layer_blocks[2], stride=2)
    block_4_layer = _build_model_basic_block_layer(block_3_layer, layer_filters[3], layer_blocks[3], stride=2)

    avgpool_layer = keras.layers.GlobalAveragePooling2D()(block_4_layer)
    # => (?batch, 512)

    return (input_layer, avgpool_layer)


def resnet_18() -> tuple[keras.layers.Input, keras.layers.Layer]:
    return _resnet_type_1(
        layer_filters=[64, 128, 256, 512],
        layer_blocks=[2, 2, 2, 2],
    )


def resnet_34() -> tuple[keras.layers.Input, keras.layers.Layer]:
    return _resnet_type_1(
        layer_filters=[64, 128, 256, 512],
        layer_blocks=[3, 4, 6, 3],
    )


def my_resnet_18() -> tuple[keras.layers.Input, keras.layers.Layer]:
    return _resnet_type_1(
        layer_filters=[32, 64, 128, 256],
        layer_blocks=[2, 2, 2, 2],
    )


def my_resnet_34() -> tuple[keras.layers.Input, keras.layers.Layer]:
    return _resnet_type_1(
        layer_filters=[32, 64, 128, 256],
        layer_blocks=[3, 4, 6, 3],
    )
