from __future__ import annotations

import concurrent.futures
import logging
import os
import pathlib
import tempfile
from typing import Any

import ffmpeg
import msgpack
import msgpack_numpy
import numpy as np
import scipy
import scipy.io.wavfile
import scipy.signal
import tqdm

logger = logging.getLogger(__name__)

_DATA_PATH = 'raw_data/vox_celeb_full'
_DB_PATH = 'db_v6_voxceleb'

_WORKER_CHUNK_SIZE = 256

SAMPLE_RATE = 8_000
N_PER_SEGMENT = 256  # produces `{N_PER_SEGMENT} // 2 + 1` frequency bins
MAX_NUM_SPEAKERS = 100

# Output Structure
'''
[
    {
        'voice_ident': 0,
        'sample_ident': '5r0dWxy17C8+9',
        'path': 'id10270/5r0dWxy17C8/00009.wav',
        'timestamps': np.ndarray(
            [
                1000,
                2000,
                3000,
                ...
            ],
            dtype=np.int64
        ),
        'fft_bins': np.ndarray(
            [
                [0.0, 0.2, 0.3, 0.2, ...],
                [0.0, 0.6, 0.4, 0.0, ...],
                [0.0, 0.1, 0.1, 0.4, ...],
                ...
            ],
            dtype=np.float32
        )
    },
    ...
]
'''


def _read_wave_format(wave_path: str) -> np.ndarray:
    sampling_rate, wave_np = scipy.io.wavfile.read(str(wave_path))

    if sampling_rate != SAMPLE_RATE:
        raise RuntimeError(f'The wave `{wave_path}` should have {SAMPLE_RATE}-Hz smapling rate')

    if len(wave_np.shape) != 1:
        raise RuntimeError(f'The wave `{wave_path}` should be mono-channel.')

    if wave_np.dtype != np.float32:
        raise RuntimeError(f'The wave `{wave_path}` should have 32-bit float PCM.')

    return wave_np


def _do_load(
    data_dir_path: pathlib.Path,
    wave_path: pathlib.Path,
    duration: int,
) -> np.ndarray:
    logger.info('Processing `%s`...', wave_path.name)

    try:
        wave_np = _read_wave_format(str(data_dir_path / wave_path))
    except RuntimeError:
        with tempfile.NamedTemporaryFile(suffix='.wav') as tmp_f:
            ffmpeg \
                .input(str(data_dir_path / wave_path)) \
                .output(tmp_f.name, acodec='pcm_f32le', ar=SAMPLE_RATE) \
                .overwrite_output() \
                .run(quiet=True)
            wave_np = _read_wave_format(tmp_f.name)

    if wave_np.shape[0] * 1000 < duration * SAMPLE_RATE:
        raise RuntimeError('Too short audio file')

    # Consider the realistic use-case, assume that the input is multiple 1-second chunk of the wave file
    t_list = []
    stft_np_list = []
    for st_frame_idx in range(0, wave_np.shape[0] - SAMPLE_RATE + 1, SAMPLE_RATE):
        wave_chunk_np = wave_np[st_frame_idx:st_frame_idx + SAMPLE_RATE]

        wave_chunk_np = wave_chunk_np / (np.abs(wave_chunk_np).max() + 1e-6)  # normalize volume

        # Apply STFT with 256 samples (~ 0.032 seconds at 8,000 Hz) per segment
        _, t, stft_np = scipy.signal.stft(
            wave_chunk_np,
            fs=SAMPLE_RATE,
            nperseg=N_PER_SEGMENT,
            noverlap=N_PER_SEGMENT // 2,
            boundary=None  # use `None` boundary to prevent overlap between adjacent wave chunks
        )

        t = ((t + st_frame_idx / SAMPLE_RATE) * 1000).astype(np.int64)  # => {seg_idx}
        if np.max(t) >= duration:
            break

        stft_np = np.abs(stft_np).transpose().astype(np.float32)  # => {seg_idx} x {freq_bins}

        t_list.append(t)
        stft_np_list.append(stft_np)

    timestamps = np.concatenate(t_list, axis=0)
    fft_bins = np.concatenate(stft_np_list, axis=0)

    return timestamps, fft_bins


def _worker(
    voice_ident_to_voice_id: dict[str, int],
    data_dir_path: pathlib.Path,
    wave_path: pathlib.Path,
    duration: int,
) -> dict[str, Any] | None:
    voice_ident, sample_ident, sample_idx_str = str(wave_path).split(os.sep)
    sample_idx = int(sample_idx_str.replace('.wav', ''))

    try:
        timestamps, fft_bins = _do_load(data_dir_path, wave_path, duration)
    except RuntimeError:
        return None

    return {
        'voice_ident': voice_ident_to_voice_id[voice_ident],
        'sample_ident': f'{sample_ident}+{sample_idx}',
        'path': str(wave_path),
        'timestamps': timestamps,
        'fft_bins': fft_bins
    }


def main() -> None:
    while True:
        # DURATION : preserve only the first part of this duration (unit: miliseconds)
        # Note that, the audio shorter than this value will be discarded
        try:
            duration = int(input('DURATION (default: 5,000, max: 25,000) > ') or 5_000)
        except ValueError:
            continue
        break

    data_dir_path = pathlib.Path(_DATA_PATH)
    db_dir_path = pathlib.Path(_DB_PATH)

    wave_path_list = [
        wave_path.relative_to(data_dir_path)
        for wave_path in data_dir_path.rglob("*")
        if wave_path.is_file() and wave_path.suffix in {'.wav', '.wave'}
    ]

    truncated_wave_path_list = []

    voice_ident_to_voice_id: dict[str, int] = {}
    for wave_path in wave_path_list:
        voice_ident, _, _ = str(wave_path).split(os.sep)

        if voice_ident not in voice_ident_to_voice_id:
            if MAX_NUM_SPEAKERS <= len(voice_ident_to_voice_id):
                break
            voice_ident_to_voice_id[voice_ident] = len(voice_ident_to_voice_id)

        truncated_wave_path_list.append(wave_path)

    with open(db_dir_path / f'db_{MAX_NUM_SPEAKERS}_{duration}.msgpack', mode='wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_array_header(len(truncated_wave_path_list)))

        with concurrent.futures.ProcessPoolExecutor() as executor:
            for chunk_idx in tqdm.tqdm(range(0, len(truncated_wave_path_list), _WORKER_CHUNK_SIZE)):
                chunk_wave_path_list = truncated_wave_path_list[chunk_idx:chunk_idx + _WORKER_CHUNK_SIZE]

                futs = [
                    executor.submit(
                        _worker,
                        voice_ident_to_voice_id,
                        data_dir_path,
                        wave_path,
                        duration,
                    )
                    for wave_path in chunk_wave_path_list
                ]

                for fut in tqdm.tqdm(concurrent.futures.as_completed(futs), total=len(futs)):
                    f.write(packer.pack(fut.result()))


if __name__ == '__main__':
    main()
