from __future__ import annotations

import tensorflow as tf

from .residual_block import make_basic_block_layer, make_bottleneck_layer


class PlottableModel(tf.keras.Model):
    '''https://stackoverflow.com/questions/61427583/how-do-i-plot-a-keras-tensorflow-subclassing-api-model
    '''

    def build(self, input_shape: tuple[int | None, ...]) -> None:
        self._input_shape = input_shape
        super().build(input_shape)

    def build_graph(self) -> tf.keras.Model:
        x = tf.keras.layers.Input(shape=self._input_shape[1:])  # remove the first batch axis
        return tf.keras.Model(inputs=[x], outputs=self.call(x))


class ResNetTypeI(PlottableModel):
    def __init__(
        self,
        dense_size: int,
        num_classes: int,
        layer_params: tuple[int, int, int, int],
    ) -> None:
        super(ResNetTypeI, self).__init__()

        self.conv1 = tf.keras.layers.Conv2D(
            filters=64,
            kernel_size=(7, 7),
            strides=2,
            padding='same',
            use_bias=False
        )
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(
            pool_size=(3, 3),
            strides=2,
            padding='same'
        )

        self.layer1 = make_basic_block_layer(filter_num=64, blocks=layer_params[0])
        self.layer2 = make_basic_block_layer(filter_num=128, blocks=layer_params[1], stride=2)
        self.layer3 = make_basic_block_layer(filter_num=256, blocks=layer_params[2], stride=2)
        self.layer4 = make_basic_block_layer(filter_num=512, blocks=layer_params[3], stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        self.fc1 = tf.keras.layers.Dense(units=dense_size, activation=tf.keras.activations.relu)
        self.fc2 = tf.keras.layers.Dense(units=num_classes, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        x = self.fc1(x)
        output = self.fc2(x)

        return output


class ResNetTypeII(PlottableModel):
    def __init__(
        self,
        dense_size: int,
        num_classes: int,
        layer_params: tuple[int, int, int, int],
    ) -> None:
        super(ResNetTypeII, self).__init__()
        self.conv1 = tf.keras.layers.Conv2D(
            filters=64,
            kernel_size=(7, 7),
            strides=2,
            padding='same',
            use_bias=False
        )
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(
            pool_size=(3, 3),
            strides=2,
            padding='same'
        )

        self.layer1 = make_bottleneck_layer(filter_num=64, blocks=layer_params[0])
        self.layer2 = make_bottleneck_layer(filter_num=128, blocks=layer_params[1], stride=2)
        self.layer3 = make_bottleneck_layer(filter_num=256, blocks=layer_params[2], stride=2)
        self.layer4 = make_bottleneck_layer(filter_num=512, blocks=layer_params[3], stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        self.fc1 = tf.keras.layers.Dense(units=dense_size, activation=tf.keras.activations.relu)
        self.fc2 = tf.keras.layers.Dense(units=num_classes, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        x = self.fc1(x)
        output = self.fc2(x)

        return output


def resnet_18(dense_size: int, num_classes: int) -> ResNetTypeI:
    return ResNetTypeI(dense_size, num_classes, (2, 2, 2, 2))


def resnet_34(dense_size: int, num_classes: int) -> ResNetTypeI:
    return ResNetTypeI(dense_size, num_classes, (3, 4, 6, 3))


def resnet_50(dense_size: int, num_classes: int) -> ResNetTypeII:
    return ResNetTypeII(dense_size, num_classes, (3, 4, 6, 3))


def resnet_101(dense_size: int, num_classes: int) -> ResNetTypeII:
    return ResNetTypeII(dense_size, num_classes, (3, 4, 23, 3))


def resnet_152(dense_size: int, num_classes: int) -> ResNetTypeII:
    return ResNetTypeII(dense_size, num_classes, (3, 8, 36, 3))
