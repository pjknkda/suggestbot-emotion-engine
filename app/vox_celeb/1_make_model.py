from __future__ import annotations

import io
import json
import os.path
import shutil

import h5py
import matplotlib
import matplotlib.pyplot as plt
import msgpack
import msgpack_numpy
import numpy as np
import tensorflow as tf
from sklearn import metrics as sklearn_metrics
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras.utils import plot_model

from app.models import VoxCelebExpSetup, VoxCelebSample
from app.utils import mel_spectogram

from . import renset

_DB_PATH = 'db_v6_voxceleb'


def _load_dataset(exp_setup: VoxCelebExpSetup) -> tuple[int, np.ndarray, np.ndarray]:
    with open(os.path.join(_DB_PATH, exp_setup.DATASET_NAME), 'rb') as f:
        dataset: list[VoxCelebSample | None] = msgpack.load(f, object_hook=msgpack_numpy.decode)

    X = np.array([sample['fft_bins'] for sample in dataset if sample])
    # => {sample_idx} x {window_idx} x {fft_bin_idx}

    X = mel_spectogram(X, n_mels=128)
    # => {sample_idx} x {window_idx} x {n_mels}

    X = np.expand_dims(X, axis=-1)
    # => {sample_idx} x {window_idx} x {n_mels} x 1

    y = np.array([sample['voice_ident'] for sample in dataset if sample])
    # => {sample_idx}

    num_classes = np.max(y) + 1

    print('num_classes :', num_classes)

    return num_classes, X, y


class _TestEvaluationCallback(keras.callbacks.Callback):
    _ACC_NAME = 'accuracy'
    # _ACC_NAME = 'sparse_top_k_categorical_accuracy'

    def __init__(
        self,
        X_test: np.ndarray,
        y_test: np.ndarray,
    ) -> None:
        super().__init__()

        self.X_test = X_test
        self.y_test = y_test

        self.train_history: dict[str, list[float]] = {
            'train_loss': [],
            'train_acc': [],

            'valid_loss': [],
            'valid_acc': [],

            'test_acc': [],
            'test_macro_precision': [],
            'test_macro_recall': [],
            'test_macro_f1': []
        }

        self._model_checkpoint_dict: dict[str, tuple[float, int, h5py.File] | None] = {
            'valid_loss': None,
            'test_acc': None,
            'test_macro_f1': None,
        }

    def on_epoch_end(
        self,
        epoch: int,
        logs: dict[str, float] | None = None
    ) -> None:
        logs = logs or {}

        y_pred = self.model.predict(self.X_test)
        y_hat = y_pred.argmax(axis=1)

        test_accuracy = sklearn_metrics.accuracy_score(self.y_test, y_hat)
        # test_accuracy = sklearn_metrics.top_k_accuracy_score(self.y_test, y_pred, k=5)

        test_mac_precision, test_mac_recall, test_mac_f1_score, _ = sklearn_metrics.precision_recall_fscore_support(
            self.y_test,
            y_hat,
            average='macro',
            zero_division=0,
        )

        try:
            self.train_history['train_loss'].append(float(logs['loss']))
            self.train_history['valid_loss'].append(float(logs['val_loss']))

            self.train_history['train_acc'].append(float(logs[self._ACC_NAME]))
            self.train_history['valid_acc'].append(float(logs[f'val_{self._ACC_NAME}']))
        except KeyError:
            # does not reach to validation step
            return

        self.train_history['test_acc'].append(float(test_accuracy))
        self.train_history['test_macro_precision'].append(float(test_mac_precision))
        self.train_history['test_macro_recall'].append(float(test_mac_recall))
        self.train_history['test_macro_f1'].append(float(test_mac_f1_score))

        _saved_model = None
        for metric_name, direction in [('valid_loss', -1),
                                       ('test_acc', 1),
                                       ('test_macro_f1', 1)]:
            checkpoint = self._model_checkpoint_dict[metric_name]
            metric_val = self.train_history[metric_name][-1]

            if (checkpoint is not None
                    and checkpoint[0] * direction >= metric_val * direction):
                continue

            if _saved_model is None:
                _saved_model_io = io.BytesIO()
                _saved_model = h5py.File(_saved_model_io, mode='w')
                _saved_model._io = _saved_model_io
                self.model.save(_saved_model)

            self._model_checkpoint_dict[metric_name] = (metric_val, epoch, _saved_model)

        print('\n')
        print(
            'Test evaluation :\n'
            f'  accuracy {test_accuracy:.4f}\n'
            f'  (macro) prec {test_mac_precision:.4f} / recall {test_mac_recall:.4f} / f1 {test_mac_f1_score:.4f}'
        )
        print(('Best scores :\n'
               '  val_loss %.4f (@%d) / test_acc %.4f (@%d) / test_macro_f1 %.4f (@%d)\n') % (
            *(self._model_checkpoint_dict['valid_loss'] or [0.0, -1])[:2],
            *(self._model_checkpoint_dict['test_acc'] or [0.0, -1])[:2],
            *(self._model_checkpoint_dict['test_macro_f1'] or [0.0, -1])[:2],
        ))
        print('\n')

    def flush_checkpoints(
        self,
        result_dir: str,
    ) -> None:
        for metric_name in ['valid_loss', 'test_acc', 'test_macro_f1']:
            checkpoint = self._model_checkpoint_dict[metric_name]
            if checkpoint is None:
                continue

            metric_val, epoch, _saved_model = checkpoint

            filename = os.path.join(result_dir, f'{metric_name}.{epoch:03d}-{metric_val:.4f}.hdf5')
            with open(filename, 'wb') as f:
                _saved_model._io.seek(0)
                shutil.copyfileobj(_saved_model._io, f)


def _save_train_history(
    result_dir: str,
    train_history: dict[str, list[float]],
) -> None:
    with open(os.path.join(result_dir, 'history.json'), 'w') as f:
        json.dump(train_history, f, indent=2)

    if os.getenv('SHOW_POLT', '').lower() != 'true':
        matplotlib.use('Agg')

    # draw loss plot
    plt.subplot(2, 1, 1)
    plt.title('Loss')
    plt.plot(train_history['train_loss'], label='train')
    plt.plot(train_history['valid_loss'], label='valid')
    plt.legend()

    # draw accuracy plot
    plt.subplot(2, 1, 2)
    plt.title('Accuracy')
    plt.plot(train_history['train_acc'], label='train')
    plt.plot(train_history['valid_acc'], label='valid')
    plt.plot(train_history['test_acc'], label='test')
    plt.plot(train_history['test_macro_precision'], label='test_m_prec')
    plt.plot(train_history['test_macro_recall'], label='test_m_rec')
    plt.plot(train_history['test_macro_f1'], label='test_m_f1')
    plt.legend()

    plt.tight_layout()

    plt.savefig(os.path.join(result_dir, 'history.pdf'))
    plt.savefig(os.path.join(result_dir, 'history.png'))

    if os.getenv('SHOW_POLT', '').lower() == 'true':
        plt.show()


def _build_model(
    ident_vector_dim: list[int],
    num_classes: int,
) -> keras.models.Model:
    # input_layer, avgpool_layer = renset.my_resnet_18()
    input_layer, avgpool_layer = renset.resnet_18()

    fc_layer = avgpool_layer
    for i in range(len(ident_vector_dim)):
        fc_layer = tf.keras.layers.Dense(units=ident_vector_dim[i])(fc_layer)
        fc_layer = keras.layers.BatchNormalization()(fc_layer)
        fc_layer = tf.keras.layers.ReLU()(fc_layer)
        fc_layer = tf.keras.layers.Dropout(0.5)(fc_layer)

    output_layer = tf.keras.layers.Dense(
        units=num_classes,
        activation=tf.keras.activations.softmax,
    )(fc_layer)

    return keras.models.Model(name='ResNet-Model', inputs=input_layer, outputs=output_layer)


def train_mode(exp_setup: VoxCelebExpSetup) -> None:
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    num_classes, X, y = _load_dataset(exp_setup)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, stratify=y)
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, stratify=y_train)
    # => train : valid : test = 8.1 : 0.9 : 1

    for name, X_iter, y_iter in [('train', X_train, y_train), ('valid', X_valid, y_valid), ('test', X_test, y_test)]:
        print(f'[{name}] X : {X_iter.shape}, y : {y_iter.shape}')

    model = _build_model(exp_setup.IDENT_VECTOR_DIM, num_classes)
    model.compile(
        # optimizer=keras.optimizers.SGD(),
        optimizer=keras.optimizers.Adam(
        ),
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy'],
        # metrics=['sparse_top_k_categorical_accuracy'],
    )
    model.summary()

    result_dir = os.path.join(_DB_PATH, exp_setup.RESULT_DIR)
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'setup.json'), 'w') as f:
        json.dump(exp_setup._asdict(), f, indent=2)

    plot_model(
        model,
        show_shapes=True,
        expand_nested=True,
        to_file=os.path.join(result_dir, 'model.pdf'),
    )

    test_evaluation_callback = _TestEvaluationCallback(X_test, y_test)

    try:
        model.fit(
            x=X_train,
            y=y_train,
            batch_size=exp_setup.BATCH_SIZE,

            verbose=1,
            callbacks=[
                test_evaluation_callback,
                # keras.callbacks.ReduceLROnPlateau(
                #     patience=5,
                #     factor=0.5,
                #     min_lr=1e-5,
                #     verbose=1
                # ),
                # keras.callbacks.ReduceLROnPlateau(
                #     patience=3,
                #     factor=0.5,
                #     min_lr=1e-5,
                #     verbose=1
                # ),
                keras.callbacks.LearningRateScheduler(
                    lambda epoch: max(0.001 * 0.7 ** (epoch // 10), 1e-6)
                    # lambda epoch: max(0.001 * 0.95 ** (epoch // 10), 1e-6)
                ),
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    # patience=30
                    patience=50
                ),
            ],
            epochs=exp_setup.MAX_EPOCHS,
            validation_data=(X_valid, y_valid)
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    test_evaluation_callback.flush_checkpoints(result_dir)
    _save_train_history(result_dir, test_evaluation_callback.train_history)


def main() -> None:
    exp_setup = VoxCelebExpSetup.init_from_env()
    np.random.seed(457700)
    train_mode(exp_setup)


if __name__ == '__main__':
    main()
