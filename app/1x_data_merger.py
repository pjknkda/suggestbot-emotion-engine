import gzip
import os

import msgpack
import msgpack_numpy
import numpy as np
import tqdm

_DATA_PATH = 'data'

# Output Structure
'''
{
    'labels': [
        'a1',
        'a2',
        ...
    ],
    'experiments': [
        # format: (subject_id, start_ts, end_ts, label_values)
        (1, 1000, 2000, (1, 2, ...)),
        (1, 3000, 4500, (2, 2, ...)),
        ...
    },
    'sensors': [
        'e4_accel.x',
        'e4_accel.y',
        ...
    ],
    'sensor_values': [
        # format: `np.ndarray` with shape [sample_idx,]
        #         dtype: [('subject_id', 'int32'), ('ts', 'int64'), ('value', 'float64')]
        # e4_accel.x
        np.ndarray(...),
        # e4_accel.y
        np.ndarray(...),
        ...
    ]
}
'''


def main() -> None:
    while True:
        data_1_filename = input('data 1 name > ') + '.msgpack.gz'
        if not os.path.exists(os.path.join(_DATA_PATH, data_1_filename)):
            print('"%s" does not exists' % data_1_filename)
            continue
        break

    while True:
        data_2_filename = input('data 2 name > ') + '.msgpack.gz'
        if not os.path.exists(os.path.join(_DATA_PATH, data_2_filename)):
            print('"%s" does not exists' % data_2_filename)
            continue
        break

    while True:
        output_filename = input('output name > ') + '.msgpack.gz'
        if os.path.exists(os.path.join(_DATA_PATH, output_filename)):
            print('"%s" already exists' % output_filename)
            continue
        break

    print('Load Data 1...')
    with gzip.open(os.path.join(_DATA_PATH, data_1_filename)) as f:
        data1 = msgpack.unpack(f, use_list=False, max_bin_len=4 * 2**30, object_hook=msgpack_numpy.decode)

    print('Load Data 2...')
    with gzip.open(os.path.join(_DATA_PATH, data_2_filename)) as f:
        data2 = msgpack.unpack(f, use_list=False, max_bin_len=4 * 2**30, object_hook=msgpack_numpy.decode)

    common_labels = list(set(data1['labels']) & set(data2['labels']))
    common_labels.sort()
    print('Common Labels :', common_labels)

    common_sensors = list(set(data1['sensors']) & set(data2['sensors']))
    common_sensors.sort()
    print('Common Sensors :', common_sensors)

    data_1_subject_ids = set(experiment[0] for experiment in data1['experiments'])
    data_2_subject_ids = set(experiment[0] for experiment in data2['experiments'])
    if data_1_subject_ids & data_2_subject_ids:
        raise RuntimeError('Conflicting Sbuject Ids are found!')

    data_1_label_indexes = [data1['labels'].index(label) for label in common_labels]
    data_2_label_indexes = [data2['labels'].index(label) for label in common_labels]

    with gzip.open(os.path.join(_DATA_PATH, output_filename), mode='wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_map_header(4))

        f.write(packer.pack('labels'))
        f.write(packer.pack(common_labels))

        f.write(packer.pack('experiments'))
        f.write(packer.pack(
            [
                (
                    experiment[0],  # subject_id
                    experiment[1],  # start_ts
                    experiment[2],  # end_ts
                    [experiment[3][idx] for idx in data_1_label_indexes]  # label_values
                )
                for experiment in data1['experiments']
            ] + [
                (
                    experiment[0],  # subject_id
                    experiment[1],  # start_ts
                    experiment[2],  # end_ts
                    [experiment[3][idx] for idx in data_2_label_indexes]  # label_values
                )
                for experiment in data2['experiments']
            ]
        ))

        f.write(packer.pack('sensors'))
        f.write(packer.pack(common_sensors))

        f.write(packer.pack('sensor_values'))
        f.write(packer.pack_array_header(len(common_sensors)))  # do stream write for memory-efficiency
        for sensor in tqdm.tqdm(common_sensors, desc='sensors'):
            data_1_values_np = data1['sensor_values'][data1['sensors'].index(sensor)]
            data_2_values_np = data2['sensor_values'][data2['sensors'].index(sensor)]

            values_np = np.concatenate((data_1_values_np, data_2_values_np))

            f.write(packer.pack(values_np))


if __name__ == '__main__':
    main()
