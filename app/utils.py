from __future__ import annotations

import csv
import dataclasses
import glob
import os
import os.path
import pickle  # nosec
import re
from collections import Counter
from typing import Any, NamedTuple

import fasttext
import librosa.filters
import nltk
import numpy as np
import tqdm

from .models import EmobankSample, SampleSplit

_EMOBANK_CSV_FILE_FORMAT = 'data/emobank.csv'
_EMOBANK_FASTTEXT_FILE_FORMAT = 'data/fasttext.en.{vector_dim}.bin'
_EMOBANK_CACHE_FILE_FORMAT = 'data/emobank_fasttext.en.{vector_dim}.pickle'

_RESULT_DIR_REGEX = re.compile(r'result'
                               r'_(?P<dataset>[\w_+]+)'
                               r'_(?P<label>\w+)'
                               r'_(?P<option>[\w_+]+)'
                               r'_(?P<ts>\d+)')
_MODEL_SNAPSHOT_FILE_REGEX = re.compile(r'(?P<metric_name>[\w_]+)'
                                        r'\.(?P<epoch>\d+)'
                                        r'\-(?P<metric_val>\d+\.\d+)'
                                        r'\.hdf5')


def load_sensor_dataset(
    db: dict[str, Any],
    label_ids: int | list[int],
) -> tuple[np.ndarray, dict[str, np.ndarray], np.ndarray, list[list[str]]]:
    X = np.array([
        sensor_value
        for _, sensor_value, _ in db['exp_sensor_values']
    ])
    # => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim}

    X = np.expand_dims(X, axis=-1)
    # => {exp_idx} X {window_idx} X {sensor_idx} X {vector_dim} X {channel == 1}

    # window_cnt := X.shape[1], vector_dim := X.shape[3]

    print('Input X.shape', X.shape)

    one_extra = db['exp_sensor_values'][0][2]

    X_extra = {}

    if 'raw_audio' in one_extra:
        X_audio = np.array([
            extra['raw_audio']
            for _, _, extra in db['exp_sensor_values']
        ])
        # => {exp_idx} X {window_idx} X {chunk_idx} X {n_fft_bin}

        X_audio = mel_spectogram(X_audio, n_mels=128)
        # => {exp_idx} X {window_idx} X {chunk_idx} X {n_mels}

        X_audio = np.expand_dims(X_audio, axis=-1)
        # => {exp_idx} X {window_idx} X {chunk_idx} X {n_mels} x 1

        print('Input X_audio.shape', X_audio.shape)

        X_extra['audio'] = X_audio

    if 'prev_label_values' in one_extra:
        X_prev_label_values = np.array([
            extra['prev_label_values']
            for _, _, extra in db['exp_sensor_values']
        ])
        # => {exp_idx} X {label_idx}

        print('Input X_prev_label_values.shape', X_prev_label_values.shape)

        if isinstance(label_ids, list):
            X_prev_label_value = np.round(np.mean(X_prev_label_values[:, label_ids], axis=1))
        else:
            X_prev_label_value = X_prev_label_values[:, label_ids]
        # => {exp_idx}

        print('Input X_prev_label_value.shape', X_prev_label_value.shape)

        X_extra['prev_label_value'] = X_prev_label_value

    ys = np.array([
        exp[3]  # exp := (subject_id, start_ts, end_ts, label_values)
        for exp, _, _ in db['exp_sensor_values']
    ])
    # => {exp_idx} X {label_idx}

    print('Input ys.shape', ys.shape)

    if isinstance(label_ids, list):
        y = np.round(np.mean(ys[:, label_ids], axis=1))
    else:
        y = ys[:, label_ids]
    # => {exp_idx}

    # Remove feature rows that `y` is NaN
    no_nan_pos = np.argwhere(~np.isnan(y)).ravel()
    X = X[no_nan_pos]
    y = y[no_nan_pos]

    print('Valid X.shape', X.shape)
    print('Valid ys.shape', ys.shape)

    sensor_groups = [['_X']]  # `['_X']` is a dummy group
    for sensor in db['sensors']:
        last_group = sensor_groups[-1]
        if last_group[-1].split('.')[0] == sensor.split('.')[0]:
            last_group.append(sensor)
        else:
            sensor_groups.append([sensor])
    sensor_groups = sensor_groups[1:]

    print('Sensor Groups', sensor_groups)

    return X, X_extra, y, sensor_groups


def load_emobank_dataset(
    vector_dim: int,
    max_sentence_length: int,
) -> list[EmobankSample]:
    cache_file = _EMOBANK_CACHE_FILE_FORMAT.format(vector_dim=vector_dim)

    if os.path.exists(cache_file):
        try:
            with open(cache_file, 'rb') as fb:
                return pickle.load(fb)  # nosec
        except Exception:
            print('Failed to load cached EmoBank dataset')

    ft_model = fasttext.load_model(_EMOBANK_FASTTEXT_FILE_FORMAT.format(vector_dim=vector_dim))

    eng_stopwords = nltk.corpus.stopwords.words('english')

    dataset: list[EmobankSample] = []
    with open(_EMOBANK_CSV_FILE_FORMAT, 'r', encoding='utf-8') as ft:
        reader = csv.DictReader(ft, fieldnames=['id', 'split', 'V', 'A', 'D', 'text'], doublequote=True)
        next(reader)  # skip header

        for row in tqdm.tqdm(reader, desc='read emobank dataset'):
            dataset.append(
                EmobankSample(
                    id=row['id'],
                    split=SampleSplit.from_str(row['split']),
                    V=float(row['V']) - 1,
                    A=float(row['A']) - 1,
                    D=float(row['D']) - 1,
                    text=np.array([
                        ft_model.get_word_vector(word.lower())
                        for word in nltk.word_tokenize(row['text'])[:max_sentence_length]
                        if word not in eng_stopwords
                    ])
                )
            )

    with open(cache_file, 'wb') as fb:
        pickle.dump(dataset, fb)

    return dataset


class TableEntryExpConfig(NamedTuple):
    dirname: str
    dataset: str
    label: str
    option: str
    ts: int

    @property
    def task(self) -> str:
        if self.option.startswith('r+'):
            return 'regression'
        elif self.option.startswith('c+'):
            return 'classification'
        raise NotImplementedError()


class TableEntryResult(NamedTuple):
    filename: str
    val: float
    epoch: int


@dataclasses.dataclass
class TableEntry:
    exp_config: TableEntryExpConfig
    result: dict[str, TableEntryResult]
    rank: str = ''

    @property
    def ident(self) -> str:
        ec = self.exp_config
        return f'{ec.dataset}.{ec.label}.{ec.task}'


def load_result_dir(
    result_dir: str,
) -> tuple[list[str], list[str], list[TableEntry]]:
    table_entries: list[TableEntry] = []

    for result_dir in glob.glob(os.path.join(result_dir, '*')):
        if not os.path.isdir(result_dir):
            continue

        dirname = os.path.basename(result_dir)
        m_result_dir = _RESULT_DIR_REGEX.match(dirname)
        if m_result_dir is None:
            continue

        if not os.path.exists(os.path.join(result_dir, 'setup.json')):
            continue

        model_shapshot_dict: dict[str, TableEntryResult] = dict()
        for hdf5_file in glob.glob(os.path.join(result_dir, '*.hdf5')):
            model_filename = os.path.basename(hdf5_file)

            m_test_hdf5 = _MODEL_SNAPSHOT_FILE_REGEX.match(model_filename)
            if m_test_hdf5 is None:
                continue

            model_shapshot_dict[m_test_hdf5.group('metric_name')] = TableEntryResult(
                filename=model_filename,
                val=float(m_test_hdf5.group('metric_val')),
                epoch=int(m_test_hdf5.group('epoch'))
            )

        if not model_shapshot_dict:
            continue

        table_entries.append(
            TableEntry(
                exp_config=TableEntryExpConfig(
                    dirname=dirname,
                    dataset=m_result_dir.group('dataset'),
                    label=m_result_dir.group('label'),
                    option=m_result_dir.group('option'),
                    ts=int(m_result_dir.group('ts'))
                ),
                result=model_shapshot_dict
            )
        )

    if set(table_entries[0].result.keys()) == {'valid_loss', 'test_acc', 'test_macro_f1'}:
        metric_names = ['ValLoss', 'TestAcc', 'TestF1']
        metric_keys = ['valid_loss', 'test_acc', 'test_macro_f1']
    elif set(table_entries[0].result.keys()) == {'valid_loss', 'test_MAE', 'test_MAPE'}:
        metric_names = ['ValLoss', 'TestMAE', 'TestMAPE']
        metric_keys = ['valid_loss', 'test_MAE', 'test_MAPE']
    else:
        raise RuntimeError('Unknown checkpoint metric type')

    table_entries.sort(key=lambda entry: entry.result[metric_keys[-1]].val, reverse=True)

    rank_counter = Counter()  # type: ignore
    for table_entry in table_entries:
        rank_counter[table_entry.ident] += 1
        table_entry.rank = str(rank_counter[table_entry.ident])

    return (metric_names, metric_keys, table_entries)


def mel_spectogram(
    spectogram: np.ndarray,
    n_mels: int,
    sampling_rate: int = 8_000,
) -> np.ndarray:
    n_fft = (spectogram.shape[-1] - 1) * 2

    # matrix : {fft_bin_idx = 1 + n_fft/2} x {n_mels}
    fft_to_mel = librosa.filters.mel(sampling_rate, n_fft, n_mels).T

    print('spectogram shape : ', spectogram.shape)
    print('mel matrix shape : ', fft_to_mel.shape)

    # {sample_idx} x {window_idx} x {fft_bin_idx} => {sample_idx} x {window_idx} x {mel_index}
    return np.log(np.matmul(spectogram, fft_to_mel) + 1e-6)
