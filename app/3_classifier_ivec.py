from __future__ import annotations

import dataclasses
import io
import json
import os
import os.path
import shutil

import h5py
import lz4.frame
import matplotlib
import matplotlib.pyplot as plt
import msgpack
import msgpack_numpy
import numpy as np
import sklearn.utils
import tensorflow as tf
from sklearn import metrics as sklearn_metrics
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras.utils import plot_model

from .models import IvecExpParam
from .utils import load_sensor_dataset

_DB_PATH = 'db_v6_ivec'


@dataclasses.dataclass
class _ExpSetupTypeDBConfig:
    MAX_DURATION: int
    WINDOW_SIZE: int
    WINDOW_INTERVAL: int
    DURATION_SPLIT: int
    VEC_DIM: int
    DATA: str


@dataclasses.dataclass
class _ExpSetupTypeDB:
    filename: str
    config: _ExpSetupTypeDBConfig
    sensors: list[str]
    sensor_scales: list[tuple[float, float, float]]


@dataclasses.dataclass
class _ExpSetupType:
    db: _ExpSetupTypeDB
    label_ids: int | list[int]


def _cbam(
    input_layer: keras.layers.Layer,
    channel_raio: int = 16,
    spatial_kernel_size: int = 7,
    name: str | None = None,
) -> keras.layers.Layer:
    attention_input_layer = keras.layers.Input(shape=input_layer.shape[1:])
    # => (?batch, w, h, channel)

    def _channel_attention(
        layer: keras.layers.Layer,
    ) -> keras.layers.Layer:
        channel = layer.shape[-1]

        shared_layer_one = keras.layers.Dense(
            channel // channel_raio,
            activation='relu',
            kernel_initializer='he_normal'
        )
        shared_layer_two = keras.layers.Dense(
            channel,
            kernel_initializer='he_normal'
        )

        avg_pool_layer = keras.layers.GlobalAveragePooling2D()(layer)
        avg_pool_layer = shared_layer_one(avg_pool_layer)
        avg_pool_layer = shared_layer_two(avg_pool_layer)

        max_pool_layer = keras.layers.GlobalMaxPooling2D()(layer)
        max_pool_layer = shared_layer_one(max_pool_layer)
        max_pool_layer = shared_layer_two(max_pool_layer)

        cbam_layer = keras.layers.Add()([avg_pool_layer, max_pool_layer])
        cbam_layer = keras.layers.Activation('sigmoid')(cbam_layer)
        # => (?batch, channel)

        cbam_layer = keras.layers.Reshape((1, 1, channel))(cbam_layer)
        # => (?batch, 1, 1, channel)

        return keras.layers.Multiply()([layer, cbam_layer])

    def _spatial_attention(
        layer: keras.layers.Layer
    ) -> keras.layers.Layer:
        avg_pool_layer = keras.layers.Lambda(lambda x: K.mean(x, axis=3, keepdims=True))(layer)
        # => (?batch, w, h, 1)
        max_pool_layer = keras.layers.Lambda(lambda x: K.max(x, axis=3, keepdims=True))(layer)
        # => (?batch, w, h, 1)

        concat_layer = keras.layers.Concatenate(axis=3)([avg_pool_layer, max_pool_layer])
        # => (?batch, w, h, 2)

        cbam_layer = keras.layers.Conv2D(
            1,
            (1, spatial_kernel_size),
            padding='same',
            kernel_initializer='he_normal',
            use_bias=False
        )(concat_layer)
        cbam_layer = keras.layers.BatchNormalization()(cbam_layer)
        cbam_layer = keras.layers.Activation('sigmoid')(cbam_layer)

        return keras.layers.Multiply()([layer, cbam_layer])

    attention_layer = _channel_attention(attention_input_layer)
    attention_layer = _spatial_attention(attention_layer)

    output_layer = keras.layers.Add()([attention_input_layer, attention_layer])

    return keras.models.Model(name=name, inputs=attention_input_layer, outputs=output_layer)(input_layer)


def _build_sensor_cnn_model(
    sensor_groups: list[list[str]],
    exp_param: IvecExpParam,
    vector_dim: int,
) -> keras.models.Model:
    num_total_sensors = sum(len(sensor_group) for sensor_group in sensor_groups)
    # NOTE: `channel_last` format
    input_layer = keras.layers.Input(shape=(num_total_sensors, vector_dim, 1))
    # => (?batch, num_total_sensors, vector_dim, 1)

    sensor_layers = []
    used_sensor_idx = 0
    for sensor_group in sensor_groups:
        num_sensors = len(sensor_group)

        sensor_layer = keras.layers.Cropping2D(
            ((used_sensor_idx, num_total_sensors - used_sensor_idx - num_sensors), (0, 0))
        )(input_layer)
        # => (?batch, num_sensors, vector_dim, 1)
        used_sensor_idx += num_sensors

        if exp_param.SENSOR_GAUSSIAN_NOISE is not None:
            sensor_layer = keras.layers.GaussianNoise(exp_param.SENSOR_GAUSSIAN_NOISE)(sensor_layer)

        # if num_sensors > 1:
        #     # self-attention : attention b/w sensor axis
        #     sensor_layer = keras.layers.Permute((3, 2, 1))(sensor_layer)  # make `num_sensors` be the channel
        #     sensor_layer = _cbam(sensor_layer, channel_raio=1, spatial_kernel_size=3)
        #     sensor_layer = keras.layers.Permute((3, 2, 1))(sensor_layer)

        sensor_layer = keras.layers.Conv2D(
            exp_param.SENSOR_CNN_FILTER_CNT,
            (num_sensors, exp_param.SENSOR_CNN_FILTER_WIDTH[0]),  # sesnor channels (e.g. x, y, z) are merged here
            activation='relu'
        )(sensor_layer)
        # => (?batch, 1, vector_dim - x1, SENSOR_CNN_FILTER_CNT)

        if exp_param.SENSOR_CNN_KEEP_PROB is not None:
            sensor_layer = keras.layers.Dropout(1 - exp_param.SENSOR_CNN_KEEP_PROB)(sensor_layer)

        if exp_param.USE_NORM:
            sensor_layer = keras.layers.BatchNormalization()(sensor_layer)

        for i in range(1, exp_param.SENSOR_CNN_DEPTH):
            sensor_layer = keras.layers.Conv2D(
                exp_param.SENSOR_CNN_FILTER_CNT,
                (1, exp_param.SENSOR_CNN_FILTER_WIDTH[i]),
                activation='relu'
            )(sensor_layer)
            # => (?batch, 1, vector_dim - xk, SENSOR_CNN_FILTER_CNT)

            if exp_param.SENSOR_CNN_KEEP_PROB is not None:
                sensor_layer = keras.layers.Dropout(1 - exp_param.SENSOR_CNN_KEEP_PROB)(sensor_layer)

            if exp_param.USE_NORM:
                sensor_layer = keras.layers.BatchNormalization()(sensor_layer)

        sensor_layer = keras.layers.Flatten()(sensor_layer)
        # => (?batch, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT)

        # sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=1))(sensor_layer)
        # sensor_layer = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, axis=-1))(sensor_layer)
        sensor_layer = keras.layers.Reshape((1, -1, 1))(sensor_layer)
        # => (?batch, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT, 1)

        sensor_layers.append(sensor_layer)

    num_sensor_groups = len(sensor_groups)

    last_layer = keras.layers.Concatenate(axis=1)(sensor_layers)
    # => (?batch, num_sensor_groups, SENSOR_CNN_FILTER_CNT * (vector_dim - xk), 1)

    # self-attention : attention b/w sensors
    if exp_param.USE_CBAM:
        last_layer = keras.layers.Permute((3, 2, 1))(last_layer)  # make `num_sensor_groups` be the channel
        last_layer = _cbam(last_layer, channel_raio=1, name='CBAM-Sensor-Fusion')
        last_layer = keras.layers.Permute((3, 2, 1))(last_layer)

    last_layer = keras.layers.Conv2D(
        exp_param.SENSOR_MERGE_CNN_FILTER_CNT,
        (num_sensor_groups, exp_param.SENSOR_MERGE_CNN_FILTER_WIDTH[0]),
        activation='relu'
    )(last_layer)
    # => (?batch, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT - y1, SENSOR_MERGE_CNN_FILTER_CNT)

    for i in range(1, exp_param.SENSOR_MERGE_CNN_DEPTH):
        last_layer = keras.layers.Conv2D(
            exp_param.SENSOR_MERGE_CNN_FILTER_CNT,
            (1, exp_param.SENSOR_MERGE_CNN_FILTER_WIDTH[i]),
            activation='relu'
        )(last_layer)
        # => (?batch, 1, (vector_dim - xk) * SENSOR_CNN_FILTER_CNT - yk, SENSOR_MERGE_CNN_FILTER_CNT)

    # self-attention : attention b/w sensor-merged filters
    if exp_param.USE_CBAM:
        last_layer = _cbam(last_layer, name='CBAM-Filter-Fusion')  # make `filters` be the channel

    # last_layer = keras.layers.Flatten()(last_layer)
    # # => (?batch, ((vector_dim - xk) * SENSOR_CNN_FILTER_CNT - yk) * SENSOR_MERGE_CNN_FILTER_CNT)

    last_layer_avg = keras.layers.GlobalAveragePooling2D()(last_layer)
    last_layer_max = keras.layers.GlobalMaxPooling2D()(last_layer)
    last_layer = keras.layers.Concatenate(axis=1)([last_layer_avg, last_layer_max])
    # # => (?batch, SENSOR_MERGE_CNN_FILTER_CNT)

    return keras.models.Model(name='Sensor-CNN', inputs=input_layer, outputs=last_layer)


def _build_cnn_rnn_model(
    sensor_groups: list[list[str]],
    exp_param: IvecExpParam,
    window_cnt: int,
    vector_dim: int,
) -> keras.models.Model:
    if exp_param.RNN_UNIT == 'GRU':
        RNN_UNIT = keras.layers.GRU
    elif exp_param.RNN_UNIT == 'LSTM':
        RNN_UNIT = keras.layers.LSTM
    else:
        raise RuntimeError('Unexpected RNN_UNIT option')

    pretrained_voxceleb_model: keras.models.Model = \
        keras.models.load_model(h5py.File(exp_param.VOXCELEB_MODEL_FILENAME, 'r'), compile=False)
    pretrained_voxceleb_model.trainable = False

    # for layer in pretrained_voxceleb_model.layers:
    #     layer._name = layer.name + '_transferred'
    #     print(layer.name)

    # for weight in pretrained_voxceleb_model.weights:
    #     weight._handle_name = weight.name + '_transferred'
    #     print(weight.name)

    rawaudio_input_layer = keras.layers.Input(
        shape=(window_cnt, None, None, 1),
        name='audio_input'
    )
    # => (?batch, window_cnt, chunk_idx, n_mels, 1)

    transferred_voxceleb_model = keras.models.Model(
        inputs=pretrained_voxceleb_model.inputs,
        outputs=pretrained_voxceleb_model.layers[-2].output,  # exclude the last softmax layer
        name='transferred_voxceleb_model'
    )
    ivec_output = keras.layers.TimeDistributed(transferred_voxceleb_model)(rawaudio_input_layer)
    # => (?batch, window_cnt, ivec_dim)

    num_total_sensors = sum(len(sensor_group) for sensor_group in sensor_groups)
    sensor_input_layer = keras.layers.Input(
        shape=(window_cnt, num_total_sensors, vector_dim, 1),
        name='sensor_input'
    )
    # => (?batch, window_cnt, num_total_sensors, vector_dim, 1)

    sensor_cnn_model = _build_sensor_cnn_model(sensor_groups, exp_param, vector_dim)
    # => (?batch, feature_dim)
    sensor_layer = keras.layers.TimeDistributed(sensor_cnn_model)(sensor_input_layer)
    # => (?batch, window_cnt, feature_dim)

    sensor_n_ivec_layer = keras.layers.Concatenate(axis=-1)([ivec_output, sensor_layer])

    last_layer = sensor_n_ivec_layer
    for i in range(0, exp_param.MAIN_RNN_NUM_LAYERS - 1):
        if exp_param.MAIN_RNN_BIDIRECTIONAL:
            last_layer = keras.layers.Bidirectional(
                RNN_UNIT(exp_param.MAIN_RNN_UNIT_CNT, activation='relu', return_sequences=True)
            )(last_layer)
        else:
            last_layer = RNN_UNIT(exp_param.MAIN_RNN_UNIT_CNT, activation='relu', return_sequences=True)(last_layer)

        if exp_param.USE_NORM:
            last_layer = keras.layers.LayerNormalization()(last_layer)

        if exp_param.MAIN_RNN_KEEP_PROB is not None:
            last_layer = keras.layers.Dropout(1 - exp_param.MAIN_RNN_KEEP_PROB)(last_layer)
    # => (?batch, MAIN_RNN_UNIT_CNT)

    if exp_param.MAIN_RNN_BIDIRECTIONAL:
        last_layer = keras.layers.Bidirectional(
            RNN_UNIT(exp_param.MAIN_RNN_UNIT_CNT, activation='relu')
        )(last_layer)
    else:
        last_layer = RNN_UNIT(exp_param.MAIN_RNN_UNIT_CNT, activation='relu')(last_layer)

    if exp_param.USE_NORM:
        last_layer = keras.layers.LayerNormalization()(last_layer)

    if exp_param.MAIN_RNN_KEEP_PROB is not None:
        last_layer = keras.layers.Dropout(1 - exp_param.MAIN_RNN_KEEP_PROB)(last_layer)
    # => (?batch, MAIN_RNN_UNIT_CNT)

    for i in range(exp_param.MAIN_DENSE_NUM_LAYERS):
        last_layer = keras.layers.Dense(
            exp_param.MAIN_DENSE_UNIT_CNT,
        )(last_layer)

        if exp_param.USE_NORM:
            last_layer = keras.layers.LayerNormalization()(last_layer)

        last_layer = keras.layers.ReLU()(last_layer)

    if exp_param.DO_REGRESSION:
        # regression approach
        last_layer = keras.layers.Dense(1, activation='linear')(last_layer)
        # => (?batch, 1)
    else:
        # classification approach
        last_layer = keras.layers.Dense(5, activation='softmax')(last_layer)
        # # => (?batch, 5)

    model = keras.models.Model(
        inputs=[rawaudio_input_layer, sensor_input_layer],
        outputs=last_layer
    )

    model.summary()

    return model


class _TestEvaluationCallback(keras.callbacks.Callback):
    def __init__(
        self,
        X_test: np.ndarray,
        X_audio_test: np.ndarray,
        y_test: np.ndarray,
        round_predict_for_acc: bool
    ) -> None:
        super().__init__()

        self.X_test = X_test
        self.X_audio_test = X_audio_test
        self.y_test = y_test
        self.round_predict_for_acc = round_predict_for_acc

        self.train_history: dict[str, list[float]] = {
            'train_loss': [],
            'train_acc': [],

            'valid_loss': [],
            'valid_acc': [],

            'test_acc': [],
            'test_macro_precision': [],
            'test_macro_recall': [],
            'test_macro_f1': []
        }

        self._model_checkpoint_dict: dict[str, tuple[float, int, h5py.File] | None] = {
            'valid_loss': None,
            'test_acc': None,
            'test_macro_f1': None,
        }

    def on_epoch_end(
        self,
        epoch: int,
        logs: dict[str, float] | None = None
    ) -> None:
        logs = logs or {}

        y_pred = self.model.predict({
            'sensor_input': self.X_test,
            'audio_input': self.X_audio_test,
        })
        if self.round_predict_for_acc:
            y_hat = np.maximum(0, np.minimum(4, np.round(y_pred)))
            acc_name = 'round_acc'
        else:
            y_hat = y_pred.argmax(axis=1)
            acc_name = 'accuracy'

        print('\n')
        print('Test confusion matrix : ')
        cm = sklearn_metrics.confusion_matrix(self.y_test, y_hat)
        print(cm)

        test_accuracy = sklearn_metrics.accuracy_score(self.y_test, y_hat)

        test_mac_precision, test_mac_recall, test_mac_f1_score, _ = sklearn_metrics.precision_recall_fscore_support(
            self.y_test,
            y_hat,
            average='macro',
            zero_division=0,
        )

        try:
            self.train_history['train_loss'].append(float(logs['loss']))
            self.train_history['valid_loss'].append(float(logs['val_loss']))

            self.train_history['train_acc'].append(float(logs[acc_name]))
            self.train_history['valid_acc'].append(float(logs['val_%s' % acc_name]))
        except KeyError:
            # does not reach to validation step
            return

        self.train_history['test_acc'].append(float(test_accuracy))
        self.train_history['test_macro_precision'].append(float(test_mac_precision))
        self.train_history['test_macro_recall'].append(float(test_mac_recall))
        self.train_history['test_macro_f1'].append(float(test_mac_f1_score))

        _saved_model = None
        for metric_name, direction in [('valid_loss', -1),
                                       ('test_acc', 1),
                                       ('test_macro_f1', 1)]:
            checkpoint = self._model_checkpoint_dict[metric_name]
            metric_val = self.train_history[metric_name][-1]

            if (checkpoint is not None
                    and checkpoint[0] * direction >= metric_val * direction):
                continue

            if _saved_model is None:
                _saved_model_io = io.BytesIO()
                _saved_model = h5py.File(_saved_model_io, mode='w')
                _saved_model._io = _saved_model_io
                self.model.save(_saved_model)

            self._model_checkpoint_dict[metric_name] = (metric_val, epoch, _saved_model)

        print('\n')
        print(('Test evaluation :\n'
               '  %s %.4f\n'
               '  (macro) prec %.4f / recall %.4f / f1 %.4f') % (
            acc_name, test_accuracy,
            test_mac_precision, test_mac_recall, test_mac_f1_score))
        print(('Best scores :\n'
               '  val_loss %.4f (@%d) / test_acc %.4f (@%d) / test_macro_f1 %.4f (@%d)\n') % (
            *(self._model_checkpoint_dict['valid_loss'] or [0.0, -1])[:2],
            *(self._model_checkpoint_dict['test_acc'] or [0.0, -1])[:2],
            *(self._model_checkpoint_dict['test_macro_f1'] or [0.0, -1])[:2],
        ))
        print('\n')

    def flush_checkpoints(
        self,
        result_dir: str,
    ) -> None:
        for metric_name in ['valid_loss', 'test_acc', 'test_macro_f1']:
            checkpoint = self._model_checkpoint_dict[metric_name]
            if checkpoint is None:
                continue

            metric_val, epoch, _saved_model = checkpoint

            filename = os.path.join(result_dir, f'{metric_name}.{epoch:03d}-{metric_val:.4f}.hdf5')
            with open(filename, 'wb') as f:
                _saved_model._io.seek(0)
                shutil.copyfileobj(_saved_model._io, f)


def _save_train_history(
    result_dir: str,
    train_history: dict[str, list[float]],
) -> None:
    with open(os.path.join(result_dir, 'history.json'), 'w') as f:
        json.dump(train_history, f, indent=2)

    if os.getenv('SHOW_POLT', '').lower() != 'true':
        matplotlib.use('Agg')

    # draw loss plot
    plt.subplot(2, 1, 1)
    plt.title('Loss')
    plt.plot(train_history['train_loss'], label='train')
    plt.plot(train_history['valid_loss'], label='valid')
    plt.legend()

    # draw accuracy plot
    plt.subplot(2, 1, 2)
    plt.title('Accuracy')
    plt.plot(train_history['train_acc'], label='train')
    plt.plot(train_history['valid_acc'], label='valid')
    plt.plot(train_history['test_acc'], label='test')
    plt.plot(train_history['test_macro_precision'], label='test_m_prec')
    plt.plot(train_history['test_macro_recall'], label='test_m_rec')
    plt.plot(train_history['test_macro_f1'], label='test_m_f1')
    plt.legend()

    plt.tight_layout()

    plt.savefig(os.path.join(result_dir, 'history.pdf'))
    plt.savefig(os.path.join(result_dir, 'history.png'))

    if os.getenv('SHOW_POLT', '').lower() == 'true':
        plt.show()


def round_acc(y_true, y_pred) -> keras.Ten:
    y_pred_round = K.minimum(4.0, K.maximum(0.0, K.round(y_pred)))
    return K.mean(K.equal(y_true, y_pred_round), axis=-1)


def do_train(
    exp_setup: _ExpSetupType,
    exp_param: IvecExpParam,
    model: keras.models.Model,
    X: np.ndarray,
    X_audio: np.ndarray,
    y: np.ndarray,
) -> None:
    # FIXME: need to preserve train/valid/test split for transfer learning
    X_train, X_test, X_audio_train, X_audio_test, y_train, y_test = train_test_split(
        X, X_audio, y, test_size=0.1, stratify=y
    )
    X_train, X_valid, X_audio_train, X_audio_valid, y_train, y_valid = train_test_split(
        X_train, X_audio_train, y_train, test_size=0.1, stratify=y_train
    )
    # => train : valid : test = 8.1 : 0.9 : 1

    class_weight_dict = None
    if exp_param.CLASS_WEIGHT:
        classes = np.unique(y_train)
        class_weight = sklearn.utils.class_weight.compute_class_weight(
            'balanced',
            classes=classes,
            y=y_train
        )
        class_weight_dict = {
            int(k): v
            for k, v in zip(classes, class_weight)
        }
        print('Class weight: ', class_weight_dict)

    result_dir = os.path.join(_DB_PATH, exp_param.RESULT_DIR)
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'setup.json'), 'w') as f:
        json.dump(dataclasses.asdict(exp_setup), f, indent=2)
    with open(os.path.join(result_dir, 'param.json'), 'w') as f:
        json.dump(dataclasses.asdict(exp_param), f, indent=2)

    plot_model(
        model,
        show_shapes=True,
        expand_nested=True,
        # rankdir='LR',
        to_file=os.path.join(result_dir, 'model.pdf'),
    )

    if exp_param.DO_REGRESSION:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            # optimizer=keras.optimizers.SGD(momentum=0.9, nesterov=True),
            loss='mean_squared_error',
            metrics=[round_acc]
        )
    else:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            # optimizer=keras.optimizers.SGD(momentum=0.9, nesterov=True),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy']
        )

    test_evaluation_callback = _TestEvaluationCallback(
        X_test,
        X_audio_test,
        y_test,
        exp_param.DO_REGRESSION
    )

    try:
        model.fit(
            x={
                'sensor_input': X_train,
                'audio_input': X_audio_train,
            },
            y=y_train,
            class_weight=class_weight_dict,
            batch_size=exp_param.BATCH_SIZE,

            verbose=1,
            callbacks=[
                test_evaluation_callback,
                *([
                    keras.callbacks.ReduceLROnPlateau(
                        patience=10,
                        factor=0.5,
                        min_lr=1e-5,
                        verbose=1
                    )] if exp_param.LR_SCHEDULE_TYPE == 0 else []
                  ),
                *([
                    keras.callbacks.LearningRateScheduler(
                        lambda epoch: max(0.001 * 0.7 ** (epoch // 10), 1e-6)
                        # lambda epoch: max(0.001 * 0.95 ** (epoch // 10), 1e-6)
                    )
                ] if exp_param.LR_SCHEDULE_TYPE == 1 else []
                ),
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    mode='min',
                    patience=40
                )
            ],
            epochs=exp_param.EPOCH,
            validation_data=(
                {
                    'sensor_input': X_valid,
                    'audio_input': X_audio_valid,
                },
                y_valid
            )
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    test_evaluation_callback.flush_checkpoints(result_dir)
    _save_train_history(result_dir, test_evaluation_callback.train_history)


def _select_db_filename() -> str:
    available_db_filenames = []
    for filename in sorted(os.listdir(_DB_PATH)):
        if not filename.endswith('.msgpack.lz4'):
            continue
        available_db_filenames.append(filename)

    if not available_db_filenames:
        raise RuntimeError('No available DB')

    print('=== List of DB ===')
    for idx, db_filename in enumerate(available_db_filenames):
        print('[%d] %s' % (idx, db_filename))

    env_db_filename = os.getenv('DB_FILENAME')
    if (env_db_filename is not None
            and env_db_filename in available_db_filenames):
        return env_db_filename

    while True:
        chosen_db_idx = int(input('Target DB index > '))
        if not (0 <= chosen_db_idx < len(available_db_filenames)):
            continue
        break

    return available_db_filenames[chosen_db_idx]


def _select_label_ids(
    labels: list[str]
) -> int | list[int]:
    print('=== List of labels ===')
    for idx, label in enumerate(labels):
        print('[%d] %s' % (idx, label))

    env_label_ids_str = os.getenv('LABEL_IDS')
    if env_label_ids_str is not None:
        env_label_ids = list(map(int, env_label_ids_str.split(',')))
        if 0 <= env_label_ids[0] <= env_label_ids[-1] < len(labels):
            return env_label_ids

    while True:
        try:
            chosen_label_ids = list(map(int, input('Target label ids (sep: ,) > ').split(',')))
        except ValueError:
            continue

        chosen_label_ids.sort()

        if not (0 <= chosen_label_ids[0] <= chosen_label_ids[-1] < len(labels)):
            continue
        break

    return chosen_label_ids if 1 < len(chosen_label_ids) else chosen_label_ids[0]


def main():
    tf.compat.v1.disable_eager_execution()

    exp_param = IvecExpParam.init_from_env()
    np.random.seed(457700)

    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    select_db_filename = _select_db_filename()
    print('DB:', select_db_filename)

    with lz4.frame.open(os.path.join(_DB_PATH, select_db_filename), 'rb') as f:
        db = msgpack.unpack(f, use_list=False, object_hook=msgpack_numpy.decode)

    selected_label_ids = _select_label_ids(db['labels'])
    print('Labels:', selected_label_ids)

    exp_setup = _ExpSetupType(
        db=_ExpSetupTypeDB(
            filename=select_db_filename,
            config=_ExpSetupTypeDBConfig(**db['config']),
            sensors=db['sensors'],
            sensor_scales=db['sensor_scales']
        ),
        label_ids=selected_label_ids
    )

    X, X_extra, y, sensor_groups = load_sensor_dataset(db, selected_label_ids)
    X_audio = X_extra['audio']

    model = _build_cnn_rnn_model(sensor_groups, exp_param, X.shape[1], X.shape[3])
    do_train(exp_setup, exp_param, model, X, X_audio, y)


if __name__ == '__main__':
    main()
