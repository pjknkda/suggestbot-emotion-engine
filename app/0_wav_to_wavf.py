import logging
import pathlib

import msgpack
import msgpack_numpy
import numpy as np
import scipy
import scipy.io.wavfile
import scipy.signal
import tqdm

logger = logging.getLogger(__name__)

# Required format: 32-bit (float) PCM, mono-channel, 8,000 Hz
# Example 1: `ffmpeg -i input.mp3 -ar 8000 -ac 1 -codec pcm_f32le output.wav`
# Example 2: `ffmpeg -i input.wav -ar 8000 -ac 1 -codec pcm_f32le output.wav`
_DATA_PATH = 'raw_data/토론_v2/figshare/Recordings/recordings/debate_audio'

SAMPLE_RATE = 8_000
N_PER_SEGMENT = 256  # produces `{N_PER_SEGMENT} // 2 + 1` frequency bins

# Output Structure
'''
{
    'timestamps': np.ndarray(
        [
            1000,
            2000,
            3000,
            ...
        ],
        dtype=int64
    ),
    'fft_bins': np.ndarray(
        [
            [0.0, 0.2, 0.3, 0.2, ...],
            [0.0, 0.6, 0.4, 0.0, ...],
            [0.0, 0.1, 0.1, 0.4, ...],
            ...
        ],
        dtype=float32
    )
}
'''


def main() -> None:
    data_dir_path = pathlib.Path(_DATA_PATH)

    wave_path_list = [
        wave_path.relative_to(data_dir_path)
        for wave_path in data_dir_path.rglob("*")
        if wave_path.is_file() and wave_path.suffix in {'.wav', '.wave'}
    ]

    def _do_load(wave_path: pathlib.Path) -> np.ndarray:
        logger.info('Processing `%s`...', wave_path.name)
        sampling_rate, wave_np = scipy.io.wavfile.read(str(data_dir_path / wave_path))

        if sampling_rate != SAMPLE_RATE:
            raise RuntimeError(f'The wave `{wave_path}` should have {SAMPLE_RATE}-Hz smapling rate')

        if len(wave_np.shape) != 1:
            raise RuntimeError(f'The wave `{wave_path}` should be mono-channel.')

        if wave_np.dtype != np.float32:
            raise RuntimeError(f'The wave `{wave_path}` should have 32-bit float PCM.')

        # Consider the realistic use-case, assume that the input is multiple 1-second chunk of the wave file
        t_list = []
        stft_np_list = []
        for st_frame_idx in range(0, wave_np.shape[0] - SAMPLE_RATE + 1, SAMPLE_RATE):
            wave_chunk_np = wave_np[st_frame_idx:st_frame_idx + SAMPLE_RATE]

            wave_chunk_np = wave_chunk_np / np.abs(wave_chunk_np).max()  # normalize volume

            # Apply STFT with 256 samples (~ 0.032 seconds at 8,000 Hz) per segment
            _, t, stft_np = scipy.signal.stft(
                wave_chunk_np,
                fs=SAMPLE_RATE,
                nperseg=N_PER_SEGMENT,
                noverlap=N_PER_SEGMENT // 2,
                boundary=None  # use `None` boundary to prevent overlap between adjacent wave chunks
            )

            t = ((t + st_frame_idx / SAMPLE_RATE) * 1000).astype(np.int64)  # => {seg_idx}
            stft_np = np.abs(stft_np).transpose().astype(np.float32)  # => {seg_idx} x {freq_bins}

            t_list.append(t)
            stft_np_list.append(stft_np)

        timestamps = np.concatenate(t_list, axis=0)
        fft_bins = np.concatenate(stft_np_list, axis=0)

        return timestamps, fft_bins

    # Validate input files
    for wave_path in tqdm.tqdm(wave_path_list, desc='wave files'):
        timestamps, fft_bins = _do_load(wave_path)

        with open(data_dir_path / (str(wave_path) + '.msgpack'), mode='wb') as f:
            packer = msgpack.Packer(default=msgpack_numpy.encode)
            f.write(
                packer.pack({
                    'timestamps': timestamps,
                    'fft_bins': fft_bins
                })
            )


if __name__ == '__main__':
    main()
