from __future__ import annotations

import gzip
import os
from collections import defaultdict
from concurrent.futures import Future, ThreadPoolExecutor
from pprint import pprint
from typing import Any, NamedTuple

import lz4.frame
import msgpack
import msgpack_numpy
import numpy as np
import tqdm

_DATA_PATH = 'data'
_DB_PATH = 'db_v6_other'

LZ4_ENABLED = True


# NOTE : pid `2x+1` and `2x+2` are the same session
# NOTE : output : my-past-emo -> other-between-sensor -> my-current-emo

# Output Structure
'''
{
    'config': {
        'DATA': 'drm1.msgpack.gz',
        'MAX_DURATION': 25000,
        'WINDOW_SIZE': 1000,
        'WINDOW_INTERVAL': 1000,
        'DURATION_SPLIT': 5000,
        'VEC_DIM': 10
    },
    'labels': [
        'a1',
        'a2',
        ...
    ],
    'sensors': [
        'e4_accel.x',
        'e4_accel.y',
        ...
    ],
    'sensor_scales': {
        # format: (min_value, max_value, nan_value)
        # e4_accel.x
        (-250, 250, 15),
        # e4_accel.y
        (-250, 250, 26),
    },
    'exp_sensor_values': [
        # format[0]: (subject_id, start_ts, end_ts, label_values)
        # format[1]: `np.ndarray` with shape [window_idx, sensor_idx, vec_idx]
        #            dtype: float64
        # format[2]: extra (dict[str, Any])
        # exp 1
        (
            (1, 1000, 2000, (1, 2, ...)),
            np.ndarray(...),
            {
                'prev_label_values': (1, 2, ...),
            },
        ),
        # exp 2
        (
            (1, 3000, 4500, (2, 2, ...)),
            np.ndarray(...),
            {
                'prev_label_values': (1, 2, ...),
            },
        )
        ...
    ]
}
'''

NUM_THREAD = 4


class Experiment(NamedTuple):
    subject_id: int
    start_ts: int
    end_ts: int
    label_values: list[float]


def _select_data_filename() -> str:
    available_data_filenames = []
    for filename in sorted(os.listdir(_DATA_PATH)):
        if not filename.endswith('.msgpack.gz'):
            continue
        available_data_filenames.append(filename)

    if not available_data_filenames:
        raise RuntimeError('No available data')

    print('=== List of Data ===')
    for idx, data_filename in enumerate(available_data_filenames):
        print('[%d] %s' % (idx, data_filename))

    while True:
        chosen_data_idx = int(input('Target data index > '))
        if not (0 <= chosen_data_idx < len(available_data_filenames)):
            continue
        break

    return available_data_filenames[chosen_data_idx]


def _input_db_config() -> dict[str, Any]:
    while True:
        # MAX_DURATION : preserve only the last part of experiment (unit: miliseconds)
        try:
            max_duration = int(input('MAX_DURATION (default: 5,000, max: 25,000) > ') or 5_000)
        except ValueError:
            continue
        break

    while True:
        # WINDOW_SIZE : the size of sliding window (unit: miliseconds)
        try:
            window_size = int(input('WINDOW_SIZE (default: 1,000) > ') or 1_000)
        except ValueError:
            continue

        if max_duration < window_size:
            print('WINDOW_SIZE should be less than or equal to MAX_DURATION')
            continue
        break

    while True:
        # WINDOW_INTERVAL : the interval between sliding window (unit: miliseconds)
        try:
            window_interval = int(input('WINDOW_INTERVAL (default: 1,000) > ') or 1_000)
        except ValueError:
            continue

        if (max_duration - window_size) % window_interval != 0:
            print('(MAX_DURATION - WINDOW_SIZE) % WINDOW_INTERVAL should be 0')
            continue
        break

    while True:
        # DURATION_SPLIT : split MAX_DURATION for data augmentation (unit: miliseconds)
        try:
            duration_split = int(input('DURATION_SPLIT (default: 5,000) > ') or 5_000)
        except ValueError:
            continue

        if max_duration % duration_split != 0 or (duration_split - window_size) % window_interval != 0:
            print('Both MAX_DURATION % DURATION_SPLIT and (DURATION_SPLIT - WINDOW_SIZE) % WINDOW_INTERVAL should be 0')
            continue
        break

    while True:
        # VEC_DIM : sampling rate inside of the window
        try:
            vec_dim = int(input('VEC_DIM (default: 10) > ') or 10)
        except ValueError:
            continue
        break

    return {
        'MAX_DURATION': max_duration,
        'WINDOW_SIZE': window_size,
        'WINDOW_INTERVAL': window_interval,
        'DURATION_SPLIT': duration_split,
        'VEC_DIM': vec_dim
    }


def _input_db_name(db_config: dict[str, Any]) -> str:
    while True:
        db_name = input('DB_NAME > ').strip()
        if not db_name:
            print('DB_NAME should be an empty string')
            continue
        if '_' in db_name:
            print('DB_NAME should not contain "_" character')
            continue

        db_filename = (
            f'{db_name}'
            f"_MD{db_config['MAX_DURATION']}"
            f"_WS{db_config['WINDOW_SIZE']}"
            f"_WI{db_config['WINDOW_INTERVAL']}"
            f"_DS{db_config['DURATION_SPLIT']}"
            f"_VD{db_config['VEC_DIM']}"
            '.msgpack' + ('.lz4' if LZ4_ENABLED else '')
        )

        if os.path.exists(os.path.join(_DB_PATH, db_filename)):
            print(f'The DB file {db_filename} already exists')
            continue
        break

    return db_filename


def _get_exp_sensor_values(
    db_config: dict[str, Any],
    subject_id: int,
    experiments: list[Experiment],
    all_sensor_values: list[np.ndarray],
    sensor_scales: list[tuple[float, float, float]],
) -> list[tuple[Experiment, np.ndarray, dict[str, Any]]]:
    other_subject_id = subject_id + 1 if subject_id % 2 else subject_id - 1

    # filter out different subject_id
    other_sensor_values = [
        sensor_value[sensor_value['pid'] == other_subject_id]
        for sensor_value in all_sensor_values
    ]

    sensor_data_list = []
    for sensor_value, sensor_scale in zip(other_sensor_values, sensor_scales):
        sensor_min, sensor_max, sensor_nan = sensor_scale

        # sort sensor value by timestamps
        target_sensor_value = np.sort(sensor_value, order='ts')

        timestamps = target_sensor_value['ts']
        values = target_sensor_value['value']

        # augment index 0 for fallback
        timestamps = np.insert(timestamps, 0, 0)
        values = np.insert(values, 0, sensor_nan)

        # normalize
        values = np.minimum(np.maximum(values, sensor_min), sensor_max)
        values = (values - sensor_min) / (sensor_max - sensor_min)

        sensor_data_list.append({
            'timestamps': timestamps,
            'values': values
        })

    def _get_resampled_sensor_values(start_ts: int, end_ts: int) -> np.ndarray:
        window_values = []
        for win_start_ts in range(start_ts,
                                  end_ts + 1,
                                  db_config['WINDOW_INTERVAL']):
            win_end_ts = win_start_ts + db_config['WINDOW_SIZE']
            target_timestamps = np.linspace(win_start_ts, win_end_ts, db_config['VEC_DIM'])

            resampled_sensor_values = []
            for sensor_data in sensor_data_list:
                value_indexes = np.searchsorted(
                    sensor_data['timestamps'],
                    target_timestamps,
                    side='right'  # to catch last-bounded element
                )
                value_indexes -= 1  # because 'right' side returns the index of the element + 1
                resampled_sensor_values.append(sensor_data['values'][value_indexes])

            window_values.append(resampled_sensor_values)

        return np.array(window_values)

    exp_sensor_values = []
    for k in range(1, len(experiments)):
        prev_experiment = experiments[k - 1]
        experiment = experiments[k]

        for i in range(db_config['MAX_DURATION'] // db_config['DURATION_SPLIT']):
            new_start_ts = experiment.end_ts \
                - db_config['MAX_DURATION'] \
                + i * db_config['DURATION_SPLIT']

            if new_start_ts < experiment.start_ts:
                raise RuntimeError('MAX_DURATION exceeds the length of expierment session!')

            new_end_ts = experiment.end_ts \
                - db_config['MAX_DURATION'] \
                + (i + 1) * db_config['DURATION_SPLIT']  \
                - db_config['WINDOW_SIZE']

            exp_sensor_values.append((
                Experiment(
                    subject_id,
                    new_start_ts,
                    new_end_ts,
                    experiment.label_values,
                ),
                _get_resampled_sensor_values(new_start_ts, new_end_ts),
                {
                    'prev_label_values': prev_experiment.label_values,
                },
            ))

    return exp_sensor_values


def main() -> None:
    selected_data_filename = _select_data_filename()
    print('data:', selected_data_filename)

    os.makedirs(_DB_PATH, exist_ok=True)

    db_config = _input_db_config()
    db_config['DATA'] = selected_data_filename

    pprint(db_config)

    db_name = _input_db_name(db_config)

    with gzip.open(os.path.join(_DATA_PATH, db_config['DATA'])) as data_f:
        data = msgpack.unpack(data_f, use_list=False, object_hook=msgpack_numpy.decode)

    if LZ4_ENABLED:
        file_opener = lz4.frame.open
    else:
        file_opener = open

    with file_opener(os.path.join(_DB_PATH, db_name), 'wb') as f:
        packer = msgpack.Packer(default=msgpack_numpy.encode)
        f.write(packer.pack_map_header(5))

        f.write(packer.pack('config'))
        f.write(packer.pack(db_config))

        f.write(packer.pack('labels'))
        f.write(packer.pack(data['labels']))

        f.write(packer.pack('sensors'))
        f.write(packer.pack(data['sensors']))

        # Calculate `sensor_scales`
        sensor_scales = []
        for sensor_value in data['sensor_values']:
            min_val = np.percentile(sensor_value['value'], 2)  # remove 2% low extream
            max_val = np.percentile(sensor_value['value'], 98)  # remove 2% high extream
            nan_val = np.percentile(sensor_value['value'], 50)  # impute NaN as global median
            sensor_scales.append((min_val, max_val, nan_val))

        f.write(packer.pack('sensor_scales'))
        f.write(packer.pack(sensor_scales))

        experiments_by_subject_id: defaultdict[int, list[Experiment]] = defaultdict(list)
        for experiment in data['experiments']:
            experiment = Experiment(*experiment)
            experiments_by_subject_id[experiment.subject_id].append(experiment)

        for experiments in experiments_by_subject_id.values():
            experiments.sort(key=lambda experiment: experiment.start_ts)

        f.write(packer.pack('exp_sensor_values'))

        num_exp_sensor_values = sum(
            max(len(experiments) - 1, 0)
            for experiments in experiments_by_subject_id.values()
        )
        f.write(packer.pack_array_header(num_exp_sensor_values))  # do stream write for memory-efficiency

        with ThreadPoolExecutor(NUM_THREAD) as executor:
            futs: list[Future[list[tuple[Experiment, np.ndarray, dict[str, Any]]]]] = []

            def _flush() -> None:
                for fut in futs:
                    exp_sensor_values = fut.result()
                    for experiment, sensor_values, extra in exp_sensor_values:
                        f.write(packer.pack_array_header(3))
                        f.write(packer.pack(experiment))
                        f.write(packer.pack(sensor_values))
                        f.write(packer.pack(extra))
                futs.clear()

            for subject_id, experiments in tqdm.tqdm(experiments_by_subject_id.items(), desc='subjects'):
                fut = executor.submit(
                    _get_exp_sensor_values,
                    db_config, subject_id, experiments, data['sensor_values'], sensor_scales
                )
                futs.append(fut)

                if NUM_THREAD <= len(futs):
                    _flush()

            _flush()


if __name__ == '__main__':
    main()
