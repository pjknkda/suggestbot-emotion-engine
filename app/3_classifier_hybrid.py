import io
import json
import os.path
import shutil
from typing import Dict, Iterator, List, Tuple

import h5py
import matplotlib.pyplot as plt
import msgpack
import msgpack_numpy
import numpy as np
import tensorflow as tf
from sklearn import metrics as sklearn_metrics
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import backend as K
from tensorflow.keras.utils import plot_model

from .models import EmobankSample, HybridExpSetup, SampleSplit
from .utils import load_emobank_dataset, load_sensor_dataset

_DB_PATH = 'db_v6_hybrid'

_LABEL_VAL_MIN = 0.0
_LABEL_VAL_MAX = 4.0


def _make_sentence_X(exp_setup: HybridExpSetup,
                     sensor_y: np.ndarray,
                     # => {exp_idx}
                     sentence_dataset: List[EmobankSample]) -> List[np.ndarray]:
    if exp_setup.SENTENCE_MODEL_DATASET_EMOTION_ID == 0:
        sentence_y = np.array([sample.A for sample in sentence_dataset])
    elif exp_setup.SENTENCE_MODEL_DATASET_EMOTION_ID == 1:
        sentence_y = np.array([sample.V for sample in sentence_dataset])
    else:
        raise RuntimeError('Unknown SENTENCE_MODEL_DATASET_EMOTION_ID')

    sentence_X: List[np.ndarray] = []
    for i in range(sensor_y.shape[0]):
        # calculate random selection weight f(d) where d is normalized distance in [0, 1]
        # e.g. f(x) ~ 1 - d**3

        sample_weights = 1 - ((abs(sentence_y - sensor_y[i]) - _LABEL_VAL_MIN) / (_LABEL_VAL_MAX - _LABEL_VAL_MIN)) ** 3
        sample_weights /= sample_weights.sum()

        sampled_idx = np.random.choice(len(sentence_dataset), 1, p=sample_weights)[0]
        sentence_X.append(sentence_dataset[sampled_idx].text)

    return sentence_X


def _make_tf_dataset(exp_setup: HybridExpSetup,
                     sensor_X: np.ndarray,
                     # => {exp_idx} X {window_idx} X {sensor_idx} X {sensor_vector_dim} X {channel == 1}
                     sentence_X: List[np.ndarray],
                     # => [ {word_idx} X {word_vector_dim}, ... ]
                     sensor_y: np.ndarray,
                     # => {exp_idx}
                     epochs: int,
                     *,
                     do_shuffle: bool = True) -> Tuple[int, tf.data.Dataset]:
    n = sensor_X.shape[0]
    sensor_cnt = sensor_X.shape[2]
    sensor_vector_dim = sensor_X.shape[3]

    def _dataset_generator() -> Iterator[Tuple[Dict[str, np.ndarray], float]]:
        for i in range(n):
            yield (
                {
                    'sensor_input': sensor_X[i],
                    # -> {window_idx} X {sensor_idx} X {sensor_vector_dim} X {channel == 1}
                    'sentence_input': sentence_X[i]
                    # -> {word_idx} X {word_vector_dim}
                },
                sensor_y[i]
            )

    output_shape = (
        {
            'sensor_input': tf.TensorShape([None, sensor_cnt, sensor_vector_dim, 1]),
            'sentence_input': tf.TensorShape([None, exp_setup.WORD_VECTOR_DIM]),
        },
        tf.TensorShape([])
    )

    tf_dataset = tf.data.Dataset.from_generator(
        _dataset_generator,
        output_types=({'sensor_input': tf.float32, 'sentence_input': tf.float32}, tf.float32),
        output_shapes=output_shape
    )
    if do_shuffle:
        tf_dataset = tf_dataset.shuffle(50 * exp_setup.BATCH_SIZE, reshuffle_each_iteration=True)
    tf_dataset = tf_dataset.repeat(epochs)
    tf_dataset = tf_dataset.padded_batch(
        exp_setup.BATCH_SIZE,
        padded_shapes=output_shape
    )

    steps_per_epoch = n // exp_setup.BATCH_SIZE

    return steps_per_epoch, tf_dataset


def _build_model(exp_setup: HybridExpSetup) -> keras.Model:
    # Load sensor model and remove the last layer
    pretrained_sensor_model = keras.models.load_model(h5py.File(exp_setup.SENSOR_MODEL_FILENAME, 'r'))
    pretrained_sensor_model.trainable = False

    sensor_input = keras.layers.Input(shape=pretrained_sensor_model.inputs[0].shape[1:], name='sensor_input')
    transferred_sensor_model = keras.models.Model(
        inputs=pretrained_sensor_model.inputs,
        outputs=pretrained_sensor_model.layers[-2].output,
        name='transferred_sensor_model'
    )
    sensor_output = transferred_sensor_model(sensor_input)

    # Load sentence model and remove the last layer
    pretrained_sentence_model = keras.models.load_model(h5py.File(exp_setup.SENTENCE_MODEL_FILENAME, 'r'))
    pretrained_sentence_model.trainable = False

    sentence_input = keras.layers.Input(shape=pretrained_sentence_model.inputs[0].shape[1:], name='sentence_input')
    transferred_sentence_model = keras.models.Model(
        inputs=pretrained_sentence_model.inputs,
        outputs=pretrained_sentence_model.layers[-2].output,
        name='transferred_sentence_model'
    )
    sentence_output = transferred_sentence_model(sentence_input)

    last_layer = keras.layers.Concatenate(axis=1)([sensor_output, sentence_output])

    for i in range(exp_setup.DENSE_NUM_LAYERS):
        last_layer = keras.layers.Dense(exp_setup.DENSE_UNIT_CNT, activation='relu')(last_layer)
        last_layer = keras.layers.BatchNormalization()(last_layer)

    if exp_setup.DO_REGRESSION:
        # regression approach
        last_layer = keras.layers.Dense(1, activation='linear')(last_layer)
        # => (?batch, 1)
    else:
        # classification approach
        last_layer = keras.layers.Dense(5, activation='softmax')(last_layer)
        # # => (?batch, 5)

    model = keras.models.Model(inputs=[sensor_input, sentence_input], outputs=last_layer, name='hybrid_model')

    model.summary()

    return model


class _TestEvaluationCallback(keras.callbacks.Callback):
    def __init__(self, test_spe, test_tf_dataset, round_predict_for_acc):
        super().__init__()

        self.test_spe = test_spe
        self.test_tf_dataset = test_tf_dataset
        self.round_predict_for_acc = round_predict_for_acc

        ys = []
        for X, y in test_tf_dataset.as_numpy_iterator():
            ys.append(y)

        if ys[0].shape[0] != ys[-1].shape[0]:
            # remainder batch exists
            self.test_spe += 1

        self.y_test = np.concatenate(ys)

        self.train_history = {
            'train_loss': [],
            'train_acc': [],

            'valid_loss': [],
            'valid_acc': [],

            'test_acc': [],
            'test_macro_precision': [],
            'test_macro_recall': [],
            'test_macro_f1': []
        }

        self._model_checkpoint_dict = {
            'valid_loss': None,
            'test_acc': None,
            'test_macro_f1': None,
        }

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        y_pred = self.model.predict(self.test_tf_dataset, steps=self.test_spe)
        if self.round_predict_for_acc:
            y_hat = np.maximum(0, np.minimum(4, np.round(y_pred)))
            acc_name = 'round_acc'
        else:
            y_hat = y_pred.argmax(axis=1)
            acc_name = 'accuracy'

        print('\n')
        print('Test confusion matrix : ')
        cm = sklearn_metrics.confusion_matrix(self.y_test, y_hat)
        print(cm)

        accuracy = sklearn_metrics.accuracy_score(self.y_test, y_hat)

        mac_precision = sklearn_metrics.precision_score(self.y_test, y_hat, average='macro')
        mac_recall = sklearn_metrics.recall_score(self.y_test, y_hat, average='macro')
        mac_f1_score = sklearn_metrics.f1_score(self.y_test, y_hat, average='macro')

        try:
            self.train_history['train_loss'].append(float(logs['loss']))
            self.train_history['valid_loss'].append(float(logs['val_loss']))

            self.train_history['train_acc'].append(float(logs[acc_name]))
            self.train_history['valid_acc'].append(float(logs['val_%s' % acc_name]))
        except KeyError:
            # does not reach to validation step
            return

        self.train_history['test_acc'].append(float(accuracy))
        self.train_history['test_macro_precision'].append(float(mac_precision))
        self.train_history['test_macro_recall'].append(float(mac_recall))
        self.train_history['test_macro_f1'].append(float(mac_f1_score))

        _saved_model = None
        for metric_name, direction in [('valid_loss', -1),
                                       ('test_acc', 1),
                                       ('test_macro_f1', 1)]:
            checkpoint = self._model_checkpoint_dict[metric_name]
            metric_val = self.train_history[metric_name][-1]

            if (checkpoint is not None
                    and checkpoint[0] * direction >= metric_val * direction):
                continue

            if _saved_model is None:
                _saved_model_io = io.BytesIO()
                _saved_model = h5py.File(_saved_model_io, mode='w')
                _saved_model._io = _saved_model_io
                self.model.save(_saved_model)

            self._model_checkpoint_dict[metric_name] = (metric_val, epoch, _saved_model)

        print('\n')
        print(('Test evaluation :\n'
               '  %s %.4f\n'
               '  (macro) prec %.4f / recall %.4f / f1 %.4f') % (
            acc_name, accuracy,
            mac_precision, mac_recall, mac_f1_score))
        print(('Best scores :\n'
               '  val_loss %.4f (@%d) / test_acc %.4f (@%d) / test_macro_f1 %.4f (@%d)\n') % (
            *self._model_checkpoint_dict['valid_loss'][:2],
            *self._model_checkpoint_dict['test_acc'][:2],
            *self._model_checkpoint_dict['test_macro_f1'][:2],
        ))
        print('\n')

    def flush_checkpoints(self, result_dir):
        for metric_name in ['valid_loss', 'test_acc', 'test_macro_f1']:
            if self._model_checkpoint_dict[metric_name] is None:
                continue

            metric_val, epoch, _saved_model = self._model_checkpoint_dict[metric_name]

            filename = os.path.join(result_dir, f'{metric_name}.{epoch:03d}-{metric_val:.4f}.hdf5')
            with open(filename, 'wb') as f:
                _saved_model._io.seek(0)
                shutil.copyfileobj(_saved_model._io, f)


def _save_train_history(result_dir: str, train_history: Dict[str, List[float]]) -> None:
    with open(os.path.join(result_dir, 'history.json'), 'w') as f:
        json.dump(train_history, f, indent=2)

    # draw loss plot
    plt.subplot(2, 1, 1)
    plt.title('Loss')
    plt.plot(train_history['train_loss'], label='train')
    plt.plot(train_history['valid_loss'], label='valid')
    plt.legend()

    # draw accuracy plot
    plt.subplot(2, 1, 2)
    plt.title('Accuracy')
    plt.plot(train_history['train_acc'], label='train')
    plt.plot(train_history['valid_acc'], label='valid')
    plt.plot(train_history['test_acc'], label='test')
    plt.plot(train_history['test_macro_precision'], label='test_m_prec')
    plt.plot(train_history['test_macro_recall'], label='test_m_rec')
    plt.plot(train_history['test_macro_f1'], label='test_m_f1')
    plt.legend()

    plt.tight_layout()

    plt.savefig(os.path.join(result_dir, 'history.pdf'))
    plt.savefig(os.path.join(result_dir, 'history.png'))

    if os.getenv('NO_PLOT', '').lower() != 'true':
        plt.show()


def round_acc(y_true, y_pred):
    y_pred_round = K.minimum(4.0, K.maximum(0.0, K.round(y_pred)))
    return K.mean(K.equal(y_true, y_pred_round), axis=-1)


def main() -> None:
    exp_setup = HybridExpSetup.init_from_env()

    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    # ==== load dataset ====

    with open(exp_setup.SENSOR_MODEL_DATASET_FILENAME, 'rb') as fb:
        sensor_db = msgpack.unpack(fb, use_list=False, object_hook=msgpack_numpy.decode)

    sensor_X, _, sensor_y, _ = load_sensor_dataset(sensor_db, exp_setup.SENSOR_MODEL_DATASET_SENSOR_IDS)

    sentence_dataset = load_emobank_dataset(exp_setup.WORD_VECTOR_DIM, exp_setup.MAX_SENTENCE_LENGTH)

    # ==== build model ====

    model = _build_model(exp_setup)

    if exp_setup.DO_REGRESSION:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            # optimizer=keras.optimizers.SGD(momentum=0.9, nesterov=True),
            loss='mean_squared_error',
            metrics=[round_acc]
        )
    else:
        model.compile(
            optimizer=keras.optimizers.Adam(),
            # optimizer=keras.optimizers.SGD(momentum=0.9, nesterov=True),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy']
        )

    result_dir = os.path.join(_DB_PATH, exp_setup.RESULT_DIR)
    os.makedirs(result_dir, exist_ok=True)

    with open(os.path.join(result_dir, 'setup.json'), 'w') as f:
        json.dump(exp_setup._asdict(), f, indent=2)

    plot_model(model,
               show_shapes=True,
               expand_nested=True,
               to_file=os.path.join(result_dir, 'model.pdf'))

    # ==== preapre train / valid / test dataset ====

    # FIXME: need to preserve train/valid/test split for transfer learning
    sensor_X_train, sensor_X_test, sensor_y_train, sensor_y_test = train_test_split(
        sensor_X, sensor_y, test_size=0.1, stratify=sensor_y)
    sensor_X_train, sensor_X_valid, sensor_y_train, sensor_y_valid = train_test_split(
        sensor_X_train, sensor_y_train, test_size=0.1, stratify=sensor_y_train)
    # => train : valid : test = 8.1 : 0.9 : 1

    print('Train', sensor_X_train.shape, sensor_y_train.shape)
    print('Valid', sensor_X_valid.shape, sensor_y_valid.shape)
    print('Test', sensor_X_test.shape, sensor_y_test.shape)

    train_spe, train_tf_dataset = _make_tf_dataset(
        exp_setup,
        sensor_X_train,
        _make_sentence_X(
            exp_setup,
            sensor_y_train,
            [sample for sample in sentence_dataset if sample.split == SampleSplit.TRAIN]
        ),
        sensor_y_train,
        exp_setup.MAX_EPOCHS
    )
    valid_spe, valid_tf_dataset = _make_tf_dataset(
        exp_setup,
        sensor_X_valid,
        _make_sentence_X(
            exp_setup,
            sensor_y_valid,
            [sample for sample in sentence_dataset if sample.split == SampleSplit.DEV]
        ),
        sensor_y_valid,
        1
    )
    test_spe, test_tf_dataset = _make_tf_dataset(
        exp_setup,
        sensor_X_test,
        _make_sentence_X(
            exp_setup,
            sensor_y_test,
            [sample for sample in sentence_dataset if sample.split == SampleSplit.TEST]
        ),
        sensor_y_test,
        1,
        do_shuffle=False
    )

    # ==== do train ====

    test_evaluation_callback = _TestEvaluationCallback(test_spe, test_tf_dataset, exp_setup.DO_REGRESSION)

    try:
        model.fit(
            train_tf_dataset,
            verbose=1,
            epochs=exp_setup.MAX_EPOCHS,
            steps_per_epoch=train_spe,
            validation_data=valid_tf_dataset,
            validation_steps=valid_spe,
            callbacks=[
                test_evaluation_callback,
                keras.callbacks.ReduceLROnPlateau(
                    patience=10,
                    factor=0.5,
                    min_lr=1e-5,
                    verbose=1
                ),
                keras.callbacks.EarlyStopping(
                    monitor='val_loss',
                    patience=30
                )
            ]
        )
    except KeyboardInterrupt:
        print('The training is stopped by KeyboardInterrupt')

    test_evaluation_callback.flush_checkpoints(result_dir)
    _save_train_history(result_dir, test_evaluation_callback.train_history)


if __name__ == '__main__':
    main()
