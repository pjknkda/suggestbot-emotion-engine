from __future__ import annotations

import dataclasses
import enum
import os
import time
from typing import Any, Callable, Literal, NamedTuple, TypedDict

import numpy as np


class SampleSplit(enum.IntEnum):
    TRAIN = 0
    DEV = 1
    TEST = 2

    @staticmethod
    def from_str(s: str) -> SampleSplit:
        return {
            'train': SampleSplit.TRAIN,
            'dev': SampleSplit.DEV,
            'valid': SampleSplit.DEV,
            'test': SampleSplit.TEST
        }[s]


class EmobankSample(NamedTuple):
    id: str
    split: SampleSplit
    V: float
    A: float
    D: float
    text: np.ndarray


class EmobankExpSetup(NamedTuple):
    RESULT_DIR: str

    FASTTEXT_VECTOR_DIM: int = 300
    MAX_SENTENCE_LENGTH: int = 40

    RNN_UNIT: str = 'GRU'
    RNN_BIDIRECTIONAL: bool = True

    TARGET_EMOTION_ID: int = 0

    RNN_UNIT_CNT: int = 128
    RNN_NUM_LAYERS: int = 2

    DENSE_UNIT_CNT: int = 128
    DENSE_NUM_LAYERS: int = 2

    MAX_EPOCHS: int = 1000
    BATCH_SIZE: int = 128

    @staticmethod
    def converter() -> dict[str, Callable[[Any], Any]]:
        return {
            'FASTTEXT_VECTOR_DIM': int,
            'MAX_SENTENCE_LENGTH': int,

            'RNN_UNIT': lambda v: v.upper(),
            'RNN_BIDIRECTIONAL': lambda v: v.lower() == 'true',

            'TARGET_EMOTION_ID': int,

            'RNN_UNIT_CNT': int,
            'RNN_NUM_LAYERS': int,

            'DENSE_UNIT_CNT': int,
            'DENSE_NUM_LAYERS': int,

            'MAX_EPOCHS': int,
            'BATCH_SIZE': int
        }

    @staticmethod
    def init_from_env() -> EmobankExpSetup:
        env_vals: dict[str, Any] = dict()
        env_vals.setdefault('RESULT_DIR', 'result_%d' % time.time())
        for key in EmobankExpSetup._fields:
            env_val = os.getenv(key)
            if env_val is None:
                continue
            env_vals[key] = EmobankExpSetup.converter().get(key, lambda x: x)(env_val)
        return EmobankExpSetup(**env_vals)


class VoxCelebSample(TypedDict):
    voice_ident: int
    sample_ident: str
    timestamps: np.ndarray
    fft_bins: np.ndarray


class VoxCelebExpSetup(NamedTuple):
    RESULT_DIR: str

    # DATASET_NAME: str = 'db+v1+small_5000.msgpack'
    # IDENT_VECTOR_DIM: list[int] = [64]

    DATASET_NAME: str = 'db+v1+medium_5000.msgpack'
    IDENT_VECTOR_DIM: list[int] = [64]

    # DATASET_NAME: str = 'db+v1+full_5000.msgpack'
    # IDENT_VECTOR_DIM: list[int] = [256]

    MAX_EPOCHS: int = 1000
    BATCH_SIZE: int = 256

    @staticmethod
    def converter() -> dict[str, Callable[[Any], Any]]:
        return {
            'IDENT_VECTOR_DIM': lambda vs: [int(v) for v in vs.split(',')],

            'MAX_EPOCHS': int,
            'BATCH_SIZE': int
        }

    @staticmethod
    def init_from_env() -> VoxCelebExpSetup:
        env_vals: dict[str, Any] = dict()
        env_vals.setdefault('RESULT_DIR', 'result_%d' % time.time())
        for key in VoxCelebExpSetup._fields:
            env_val = os.getenv(key)
            if env_val is None:
                continue
            env_vals[key] = VoxCelebExpSetup.converter().get(key, lambda x: x)(env_val)
        return VoxCelebExpSetup(**env_vals)


@dataclasses.dataclass
class SensorExpParam:
    RESULT_DIR: str

    DO_REGRESSION: bool = True
    USE_CBAM: bool = True
    USE_NORM: bool = True
    CLASS_WEIGHT: bool = True

    SENSOR_GAUSSIAN_NOISE: float | None = None

    SENSOR_CNN_FILTER_CNT: int = 64
    SENSOR_CNN_FILTER_WIDTH: list[int] = dataclasses.field(default_factory=lambda: [3, 3, 3])
    SENSOR_CNN_KEEP_PROB: float | None = None
    SENSOR_CNN_DEPTH: int = 3

    SENSOR_MERGE_CNN_FILTER_CNT: int = 64
    SENSOR_MERGE_CNN_FILTER_WIDTH: list[int] = dataclasses.field(default_factory=lambda: [6, 6, 4, 4, 2, 2])
    SENSOR_MERGE_CNN_DEPTH: int = 6

    RNN_UNIT: Literal['LSTM', 'GRU'] = 'GRU'

    MAIN_RNN_BIDIRECTIONAL: bool = False
    MAIN_RNN_UNIT_CNT: int = 128
    MAIN_RNN_KEEP_PROB: float | None = None
    MAIN_RNN_NUM_LAYERS: int = 2

    MAIN_DENSE_UNIT_CNT: int = 128
    MAIN_DENSE_NUM_LAYERS: int = 1

    EPOCH: int = 1000
    BATCH_SIZE: int = 32

    @staticmethod
    def converter() -> dict[str, Callable[[Any], Any]]:
        return {
            'DO_REGRESSION': lambda v: v.lower() == 'true',
            'USE_CBAM': lambda v: v.lower() == 'true',
            'USE_NORM': lambda v: v.lower() == 'true',
            'CLASS_WEIGHT': lambda v: v.lower() == 'true',

            'SENSOR_GAUSSIAN_NOISE': float,

            'SENSOR_CNN_FILTER_CNT': int,
            'SENSOR_CNN_FILTER_WIDTH': lambda vs: [int(v) for v in vs.split(',')],
            'SENSOR_CNN_KEEP_PROB': float,
            'SENSOR_CNN_DEPTH': int,

            'SENSOR_MERGE_CNN_FILTER_CNT': int,
            'SENSOR_MERGE_CNN_FILTER_WIDTH': lambda vs: [int(v) for v in vs.split(',')],
            'SENSOR_MERGE_CNN_DEPTH': int,

            'RNN_UNIT': lambda v: v.upper(),

            'MAIN_RNN_BIDIRECTIONAL': lambda v: v.lower() == 'true',
            'MAIN_RNN_UNIT_CNT': int,
            'MAIN_RNN_KEEP_PROB': float,
            'MAIN_RNN_NUM_LAYERS': int,

            'MAIN_DENSE_UNIT_CNT': int,
            'MAIN_DENSE_NUM_LAYERS': int,

            'EPOCH': int,
            'BATCH_SIZE': int,
        }

    @staticmethod
    def init_from_env() -> SensorExpParam:
        env_vals: dict[str, Any] = dict()
        env_vals.setdefault('RESULT_DIR', 'result_%d' % time.time())
        for field in dataclasses.fields(SensorExpParam):
            env_val = os.getenv(field.name)
            if env_val is None:
                continue
            env_vals[field.name] = SensorExpParam.converter().get(field.name, lambda x: x)(env_val)
        return SensorExpParam(**env_vals)


class HybridExpSetup(NamedTuple):
    RESULT_DIR: str

    DO_REGRESSION: bool = True

    SENSOR_MODEL_FILENAME: str = (
        'db_v6/'
        'result_noaudio+small'
        '_arousal'
        '_c+nocbam+nonorm+gru+noise5+sdrop8+sfilter64+smfilter64+runit128+rdrop5+rlayer2+dunit128+dlayer1'
        '_366573/'
        'test_macro_f1.097-0.5191.hdf5'
        # 'test_acc.078-0.8462.hdf5'
    )
    # SENSOR_MODEL_FILENAME: str = (
    #     'db_v6/'
    #     'result_noaudio+small'
    #     '_valence'
    #     '_c+nocbam+nonorm+gru+noise5+sdrop8+sfilter64+smfilter64+runit128+rdrop5+rlayer2+dunit128+dlayer1'
    #     '_366290/'
    #     'test_macro_f1.070-0.5594.hdf5'
    #     # 'test_acc.081-0.8462.hdf5'
    # )

    @property
    def SENSOR_MODEL_DATASET_FILENAME(self) -> str:
        if '_noaudio+small' in self.SENSOR_MODEL_FILENAME:
            return (
                'db_v6/'
                'debate2-noaudio_MD5000_WS1000_WI1000_DS5000_VD10.msgpack'
            )
        elif '_noaudio+dense' in self.SENSOR_MODEL_FILENAME:
            return (
                'db_v6/'
                'debate2-noaudio-dense_MD5000_WS1000_WI500_DS5000_VD32.msgpack'
            )
        elif '_audio+small' in self.SENSOR_MODEL_FILENAME:
            return (
                'db_v6/'
                'debate2_MD5000_WS1000_WI1000_DS5000_VD10.msgpack'
            )
        elif '_audio+dense' in self.SENSOR_MODEL_FILENAME:
            return (
                'db_v6/'
                'debate2-dense_MD5000_WS1000_WI500_DS5000_VD32.msgpack'
            )
        else:
            raise RuntimeError('Unknown SENSOR_MODEL_DATASET_FILENAME')

    @property
    def SENSOR_MODEL_DATASET_SENSOR_IDS(self) -> list[int]:
        if '_arousal' in self.SENSOR_MODEL_FILENAME:
            return [0]
        elif '_valence' in self.SENSOR_MODEL_FILENAME:
            return [1]
        else:
            raise RuntimeError('Unknown SENSOR_MODEL_DATASET_SENSOR_IDS')

    SENTENCE_MODEL_FILENAME: str = (
        'db_v6_emobank/'
        'result_fasttext300+emobank_arousal_gru+uni+runit32+rlayer2+dunit128+dlayer2_198030/'
        'test_MAPE.117-8.5133.hdf5'
        # 'test_MAE.102-0.1727.hdf5'
    )
    # SENTENCE_MODEL_FILENAME: str = (
    #     'db_v6_emobank/'
    #     'result_fasttext300+emobank_valence_lstm+uni+runit32+rlayer2+dunit256+dlayer1_198143/'
    #     'test_MAPE.028-10.5115.hdf5'
    #     # 'test_MAE.030-0.1971.hdf5'
    # )

    @property
    def SENTENCE_MODEL_DATASET_EMOTION_ID(self) -> int:
        if '_arousal' in self.SENTENCE_MODEL_FILENAME:
            return 0
        elif '_valence' in self.SENTENCE_MODEL_FILENAME:
            return 1
        else:
            raise RuntimeError('Unknown SENSOR_MODEL_DATASET_SENSOR_IDS')

    WORD_VECTOR_DIM: int = 300  # TODO: fetch from SENTENCE_MODEL
    MAX_SENTENCE_LENGTH: int = 40  # TODO: fetch from SENTENCE_MODEL

    DENSE_UNIT_CNT: int = 128
    DENSE_NUM_LAYERS: int = 2

    MAX_EPOCHS: int = 1000
    BATCH_SIZE: int = 128

    @staticmethod
    def converter() -> dict[str, Callable[[Any], Any]]:
        return {
            'DO_REGRESSION': lambda v: v.lower() == 'true',

            'WORD_VECTOR_DIM': int,
            'MAX_SENTENCE_LENGTH': int,

            'DENSE_UNIT_CNT': int,
            'DENSE_NUM_LAYERS': int,

            'MAX_EPOCHS': int,
            'BATCH_SIZE': int
        }

    @staticmethod
    def init_from_env() -> HybridExpSetup:
        env_vals: dict[str, Any] = dict()
        env_vals.setdefault('RESULT_DIR', 'result_%d' % time.time())
        for key in HybridExpSetup._fields:
            env_val = os.getenv(key)
            if env_val is None:
                continue
            env_vals[key] = HybridExpSetup.converter().get(key, lambda x: x)(env_val)
        return HybridExpSetup(**env_vals)


@dataclasses.dataclass
class IvecExpParam:
    RESULT_DIR: str

    DO_REGRESSION: bool = True
    USE_CBAM: bool = True
    USE_NORM: bool = True
    CLASS_WEIGHT: bool = True

    LR_SCHEDULE_TYPE: int = 0

    SENSOR_GAUSSIAN_NOISE: float | None = None

    SENSOR_CNN_FILTER_CNT: int = 64
    SENSOR_CNN_FILTER_WIDTH: list[int] = dataclasses.field(default_factory=lambda: [3, 3, 3])
    SENSOR_CNN_KEEP_PROB: float | None = None
    SENSOR_CNN_DEPTH: int = 3

    SENSOR_MERGE_CNN_FILTER_CNT: int = 64
    SENSOR_MERGE_CNN_FILTER_WIDTH: list[int] = dataclasses.field(default_factory=lambda: [6, 6, 4, 4, 2, 2])
    SENSOR_MERGE_CNN_DEPTH: int = 6

    RNN_UNIT: Literal['LSTM', 'GRU'] = 'GRU'

    MAIN_RNN_BIDIRECTIONAL: bool = False
    MAIN_RNN_UNIT_CNT: int = 128
    MAIN_RNN_KEEP_PROB: float | None = None
    MAIN_RNN_NUM_LAYERS: int = 2

    MAIN_DENSE_UNIT_CNT: int = 128
    MAIN_DENSE_NUM_LAYERS: int = 1

    # VOXCELEB_MODEL_FILENAME: str = (
    #     'db_v6_voxceleb/'
    #     'result_medium_1616489835/'
    #     'test_macro_f1.124-0.7869.hdf5'
    # )
    VOXCELEB_MODEL_FILENAME: str = (
        'db_v6_voxceleb/'
        'result_small_1616488794/'
        'test_macro_f1.149-0.8879.hdf5'
    )

    EPOCH: int = 1000
    BATCH_SIZE: int = 32

    @staticmethod
    def converter() -> dict[str, Callable[[Any], Any]]:
        return {
            'DO_REGRESSION': lambda v: v.lower() == 'true',
            'USE_CBAM': lambda v: v.lower() == 'true',
            'USE_NORM': lambda v: v.lower() == 'true',
            'CLASS_WEIGHT': lambda v: v.lower() == 'true',

            'LR_SCHEDULE_TYPE': int,

            'SENSOR_GAUSSIAN_NOISE': float,

            'SENSOR_CNN_FILTER_CNT': int,
            'SENSOR_CNN_FILTER_WIDTH': lambda vs: [int(v) for v in vs.split(',')],
            'SENSOR_CNN_KEEP_PROB': float,
            'SENSOR_CNN_DEPTH': int,

            'SENSOR_MERGE_CNN_FILTER_CNT': int,
            'SENSOR_MERGE_CNN_FILTER_WIDTH': lambda vs: [int(v) for v in vs.split(',')],
            'SENSOR_MERGE_CNN_DEPTH': int,

            'RNN_UNIT': lambda v: v.upper(),

            'MAIN_RNN_BIDIRECTIONAL': lambda v: v.lower() == 'true',
            'MAIN_RNN_UNIT_CNT': int,
            'MAIN_RNN_KEEP_PROB': float,
            'MAIN_RNN_NUM_LAYERS': int,

            'MAIN_DENSE_UNIT_CNT': int,
            'MAIN_DENSE_NUM_LAYERS': int,

            'EPOCH': int,
            'BATCH_SIZE': int,
        }

    @staticmethod
    def init_from_env() -> IvecExpParam:
        env_vals: dict[str, Any] = dict()
        env_vals.setdefault('RESULT_DIR', 'result_%d' % time.time())
        for field in dataclasses.fields(IvecExpParam):
            env_val = os.getenv(field.name)
            if env_val is None:
                continue
            env_vals[field.name] = IvecExpParam.converter().get(field.name, lambda x: x)(env_val)
        return IvecExpParam(**env_vals)
