from __future__ import annotations

import os
import queue
import random
import subprocess  # nosec
import sys
import time
from concurrent.futures import Future, ThreadPoolExecutor, as_completed
from typing import NamedTuple

from sklearn.model_selection import ParameterGrid, ParameterSampler

INTERPRETER = sys.executable
if INTERPRETER is None:
    raise RuntimeError('Could not find python interpreter')
else:
    print(f'interpreter: {INTERPRETER}')

_DEBATE_LABELS: dict[str, str] = {
    'arousal': '0',
    # 'valence': '1',
    # 'stress': '2,3'
}

_PARAM_GRID_REQ: dict[str, list[tuple[str, str | None]]] = {
    'DO_REGRESSION': [('c', 'false'), ('r', 'true')],
    'USE_CBAM': [('cbam', 'true'), ('nocbam', 'false')],
    'USE_NORM': [('norm', 'true'), ('nonorm', 'false')],
    'LR_SCHEDULE_TYPE': [('lr0', '0'), ('lr1', '1')],
}

_PARAM_GRID_OPT: dict[str, list[tuple[str, str | None]]] = {
    # 'RNN_UNIT': [('gru', 'gru'), ('lstm', 'lstm')],
    'RNN_UNIT': [('lstm', 'lstm')],
    # 'SENSOR_GAUSSIAN_NOISE': [('noiseN', None), ('noise3', '1e-3'), ('noise5', '1e-5')],
    'SENSOR_GAUSSIAN_NOISE': [('noiseN', None)],
    # 'SENSOR_CNN_KEEP_PROB': [('sdrop0', None), ('sdrop5', '0.5'), ('sdrop8', '0.8')],
    'SENSOR_CNN_KEEP_PROB': [('sdrop0', None)],
    'SENSOR_CNN_FILTER_CNT': [('sfilter16', '16'), ('sfilter32', '32'), ('sfilter64', '64'), ('sfilter128', '128')],
    'SENSOR_MERGE_CNN_FILTER_CNT': [
        ('smfilter16', '16'), ('smfilter32', '32'), ('smfilter64', '64'), ('smfilter128', '128')],
    'MAIN_RNN_UNIT_CNT': [('runit32', '32'), ('runit64', '64'), ('runit128', '128')],
    # 'MAIN_RNN_KEEP_PROB': [('rdrop0', None), ('rdrop5', '0.5'), ('rdrop8', '0.8')],
    'MAIN_RNN_KEEP_PROB': [('rdrop0', None), ('rdrop5', '0.5')],
    # 'MAIN_RNN_NUM_LAYERS': [('rlayer1', '1'), ('rlayer2', '2'), ('rlayer3', '3')],
    'MAIN_RNN_NUM_LAYERS': [('rlayer1', '1'), ('rlayer2', '2'), ('rlayer3', '3'), ('rlayer4', '4')],
    'MAIN_DENSE_UNIT_CNT': [('dunit32', '32'), ('dunit64', '64'), ('dunit128', '128')],
    # 'MAIN_DENSE_NUM_LAYERS': [('dlayer1', '1'), ('dlayer2', '2')],
    'MAIN_DENSE_NUM_LAYERS': [('dlayer1', '1'), ('dlayer2', '2'), ('dlayer3', '3')],
}


class DBInfo(NamedTuple):
    dbfile: str
    result_dir_format: str
    labels: dict[str, str]


_DB_INFO: list[DBInfo] = [
    # DBInfo(
    #     dbfile='debate2_MD5000_WS1000_WI1000_DS5000_VD10.msgpack.lz4',
    #     result_dir_format='result_audio+small_{label}_{option}_{ts}',
    #     labels=_DEBATE_LABELS
    # ),
    DBInfo(
        dbfile='debate2-dense_MD5000_WS1000_WI500_DS5000_VD32.msgpack.lz4',
        result_dir_format='result_audio+dense_{label}_{option}_{ts}',
        labels=_DEBATE_LABELS
    ),
]


GPU_IDS = os.getenv('GPU_IDS', '0,1,2,3,4,5,6,7').split(',')
N_ITER = int(os.getenv('N_ITER', '150'))


def _do_run(
    gpu_queue: queue.Queue[str],
    exp_param: dict[str, str],
    dbfile: str,
    result_dir: str,
    label_ids: str,
) -> dict[str, str]:
    assert INTERPRETER is not None  # nosec

    gpu_id = gpu_queue.get()
    try:
        subprocess.run(  # nosec
            [INTERPRETER, '-m', 'app.3_classifier_ivec'],
            env={
                **os.environ,
                **exp_param,
                'CUDA_VISIBLE_DEVICES': gpu_id,
                'DB_FILENAME': dbfile,
                'RESULT_DIR': result_dir,
                'LABEL_IDS': label_ids,
                'NO_PLOT': 'true'
            }
        )
    finally:
        gpu_queue.task_done()
        gpu_queue.put(gpu_id)

    return exp_param


with ThreadPoolExecutor(len(GPU_IDS)) as executor:
    gpu_queue: queue.Queue[str] = queue.Queue()
    for gpu_id in GPU_IDS:
        gpu_queue.put(gpu_id)

    futs: list[Future] = []

    for param_sample_opt in ParameterSampler(_PARAM_GRID_OPT, n_iter=N_ITER):
        option_opt_str = '+'.join(param_sample_opt[key][0] for key in _PARAM_GRID_OPT.keys())

        for param_sample_req in ParameterGrid(_PARAM_GRID_REQ):
            option_req_str = '+'.join(param_sample_req[key][0] for key in _PARAM_GRID_REQ.keys())

            exp_param: dict[str, str] = {
                **{
                    key: val[1]
                    for key, val in param_sample_req.items()
                    if val[1] is not None
                },
                **{
                    key: val[1]
                    for key, val in param_sample_opt.items()
                    if val[1] is not None
                }
            }

            option_str = '%s+%s' % (option_req_str, option_opt_str)

            for db_info in _DB_INFO:
                for label_name, label_ids in db_info.labels.items():
                    futs.append(
                        executor.submit(
                            _do_run,
                            gpu_queue,
                            exp_param,
                            db_info.dbfile,
                            db_info.result_dir_format.format(
                                label=label_name,
                                option=option_str,
                                ts=int(time.monotonic() + random.randint(0, 1000))  # nosec
                            ),
                            label_ids
                        )
                    )

    try:
        for idx, fut in enumerate(as_completed(futs)):
            print('Done! [%d / %d]' % (idx + 1, len(futs)))
    except KeyboardInterrupt:
        print('Cancelled!')
        for fut in futs:
            if not fut.done():
                fut.cancel()
        executor.shutdown(False)
