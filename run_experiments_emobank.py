from __future__ import annotations

import os
import queue
import random
import subprocess  # nosec
import time
from concurrent.futures import Future, ThreadPoolExecutor, as_completed

from sklearn.model_selection import ParameterSampler

POSSIBLE_INTERPRETERS: list[str] = [
    './venv/Scripts/python.exe',
    './venv/bin/python'
]

INTERPRETER: str | None = None
for pypath in POSSIBLE_INTERPRETERS:
    if os.path.exists(pypath):
        INTERPRETER = pypath
        break

if INTERPRETER is None:
    raise RuntimeError('Could not find python interpreter')
else:
    print(f'interpreter: {INTERPRETER}')

_EMOTION_IDS: dict[str, str] = {
    'arousal': '0',
    'valence': '1',
}


_PARAM_GRID: dict[str, list[tuple[str, str | None]]] = {
    'RNN_UNIT': [('gru', 'gru'), ('lstm', 'lstm')],
    'RNN_BIDIRECTIONAL': [('bi', 'true'), ('uni', 'false')],
    'RNN_UNIT_CNT': [('runit32', '32'), ('runit64', '64'), ('runit128', '128'), ('runit128', '256')],
    'RNN_NUM_LAYERS': [('rlayer1', '1'), ('rlayer2', '2'), ('rlayer3', '3')],
    'DENSE_UNIT_CNT': [('dunit32', '32'), ('dunit64', '64'), ('dunit128', '128'), ('dunit256', '256')],
    'DENSE_NUM_LAYERS': [('dlayer1', '1'), ('dlayer2', '2'), ('dlayer3', '3')]
}


RESULT_DIR_FORMAT = 'result_emobank_{label}_{option}_{ts}'


GPU_IDS = os.getenv('GPU_IDS', '0,1,2,3,4,5,6,7').split(',')
N_ITER = int(os.getenv('N_ITER', '1000'))


def _do_run(
    gpu_queue: queue.Queue[str],
    option_str: str,
    exp_param: dict[str, str],
    result_dir: str,
    emotion_id: str,
) -> dict[str, str]:
    assert INTERPRETER is not None  # nosec

    gpu_id = gpu_queue.get()
    try:
        subprocess.run(  # nosec
            [INTERPRETER, '-m', 'app.emobank.1_make_model'],
            env={
                **os.environ,
                **exp_param,
                'CUDA_VISIBLE_DEVICES': gpu_id,
                'RESULT_DIR': result_dir,
                'TARGET_EMOTION_ID': emotion_id,
                'NO_PLOT': 'true'
            }
        )
    finally:
        gpu_queue.task_done()
        gpu_queue.put(gpu_id)

    return exp_param


with ThreadPoolExecutor(len(GPU_IDS)) as executor:
    gpu_queue: queue.Queue[str] = queue.Queue()
    for gpu_id in GPU_IDS:
        gpu_queue.put(gpu_id)

    futs: list[Future] = []

    for param_sample in ParameterSampler(_PARAM_GRID, n_iter=N_ITER):
        option_str = '+'.join(param_sample[key][0] for key in _PARAM_GRID.keys())

        exp_param: dict[str, str] = {
            key: val[1]
            for key, val in param_sample.items()
            if val[1] is not None
        }

        for label_name, emotion_id in _EMOTION_IDS.items():
            futs.append(
                executor.submit(
                    _do_run,
                    gpu_queue,
                    option_str,
                    exp_param,
                    RESULT_DIR_FORMAT.format(
                        label=label_name,
                        option=option_str,
                        ts=int(time.monotonic() + random.randint(0, 1000))  # nosec
                    ),
                    emotion_id
                )
            )

    try:
        for idx, fut in enumerate(as_completed(futs)):
            print('Done! [%d / %d]' % (idx + 1, len(futs)))
    except KeyboardInterrupt:
        print('Cancelled!')
        for fut in futs:
            if not fut.done():
                fut.cancel()
        executor.shutdown(False)
